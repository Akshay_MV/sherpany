({
    doInit: function(component, event, helper) {
        var getDefault = component.get('c.getDefault');
        getDefault.setParams({
            OppId: component.get('v.recordId')
        });
        getDefault.setCallback(this, function(response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                component.set('v.Opportunity', response.getReturnValue());
                // component.set('v.OpportunityClone',response.getReturnValue());
                helper.doInitClone(component, event, helper);
                // component.set('v.Spinner',false);
            } else {
                helper.NotificationToast(component, event, helper, 'Something went wrong!', 'error', 'Please contact your System Administrator.');
                component.set('v.Spinner', false);
            }
        });
        $A.enqueueAction(getDefault);
    },

    doInitClone: function(component, event, helper) {
        var getDefault = component.get('c.getDefault');
        getDefault.setParams({
            OppId: component.get('v.recordId')
        });
        getDefault.setCallback(this, function(response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                // component.set('v.Opportunity',response.getReturnValue());
                component.set('v.OpportunityClone', response.getReturnValue());
                component.set('v.Spinner', false);
            } else {
                helper.NotificationToast(component, event, helper, 'Something went wrong!', 'error', 'Please contact your System Administrator.');
                component.set('v.Spinner', false);
            }
        });
        $A.enqueueAction(getDefault);
    },

    createNew: function(component, event, helper) {
        component.set('v.Spinner', true);
        var insertOpp = component.get('c.createNewOpp');
        insertOpp.setParams({
            Opportunity: component.get('v.Opportunity')
        });
        insertOpp.setCallback(this, function(response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                helper.NotificationToast(component, event, helper, 'Success!', 'Success', 'New Opportunity Created');
                component.set('v.returenedId', response.getReturnValue().Id);
                // component.set('v.Spinner',false);
                helper.updateOld(component, event, helper);
                // window.open('/'+respone.getReturnValue().Id,'_top');
            } else {
                helper.NotificationToast(component, event, helper, 'Error!', 'Error', 'Selected User is Inactive');
                component.set('v.Spinner', false);
            }
        });
        $A.enqueueAction(insertOpp);
    },

    updateOld: function(component, event, helper) {
        var updateOpp = component.get('c.updateOldOpp');
        updateOpp.setParams({
            Opportunity: component.get('v.OpportunityClone')
        });
        updateOpp.setCallback(this, function(response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                helper.NotificationToast(component, event, helper, 'Success!', 'Success', 'Old Opportunity updated');
                component.set('v.Spinner', false);
                window.open('/' + component.get('v.returenedId'), '_top');
            } else {
                helper.NotificationToast(component, event, helper, 'Success!', 'Success', 'Opportunity Update Failed. Please contact System administrator.');
            }
        });
        $A.enqueueAction(updateOpp);
    },

    getPickListValues: function(component, event, helper, objectName, FieldName, AttributName) {
        var status = component.get("c.getPickListValues");
        status.setParams({
            'ObjName': objectName,
            'FieldName': FieldName
        });
        status.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            component.set("v." + AttributName, allValues);
        });
        $A.enqueueAction(status);
    },

    NotificationToast: function(component, event, helper, toastTitle, toastType, Msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: toastTitle,
            type: toastType,
            message: Msg
        });
        toastEvent.fire();
    }
})