({
	doInit : function(component, event, helper){

		var getDefaults =  component.get('c.getDefault');
		getDefaults.setParams({
			year        : component.get('v.selectedYear'),
			month       : component.get('v.selectedMonth'),
            ls          : component.get('v.selectedSource'),
            userNames   : component.get('v.selectedCheckBoxes')
		});
		getDefaults.setCallback(this, function(response){
			var state = response.getState();
			if(state == 'SUCCESS'){
				// console.log('Got this from APEX');
				// console.log(response.getReturnValue());
                component.set('v.OppFieldHistory',response.getReturnValue());
                component.set("v.isModalOpen", false);
                component.set("v.Spinner", false);
			}else{
                alert('Fail');
                component.set("v.isModalOpen", false);
                component.set("v.Spinner", false);
			}
		});
		$A.enqueueAction(getDefaults);
	},

	getPickListValues : function(component, event, helper, objectName, FieldName, AttributName){
        var status = component.get("c.getPickListValues");
        status.setParams({
            'ObjName' : objectName,
            'FieldName' : FieldName
        });
        status.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            component.set("v."+AttributName, allValues);
        });
        $A.enqueueAction(status);
    },

    getUserNameData : function(component, event, helper){
        var action = component.get('c.UserName');
        action.setParams({
            Year : component.get('v.selectedYear')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                component.set('v.userNames',response.getReturnValue());
                // console.log(response.getReturnValue());
                // alert('SUCCESS');
                component.set("v.Spinner", false);
            }else{
                alert('Something went wrong!');
                component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    
    chartLoad : function(component, event, helper)
    {
        var action = component.get("c.getOpportunityJSON");
        action.setParams({
            year        : component.get('v.selectedYear'),
			month       : component.get('v.selectedMonth'),
            userNames   : component.get('v.selectedCheckBoxes')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            //alert(state);
            if (state === "SUCCESS") {
                var dataObj= response.getReturnValue();
                // jsonData = dataObj;
                console.log('===='+dataObj);
                console.log(dataObj);
                component.set("v.data",dataObj);
                
                helper.Linechart(component,event,helper);
                
            }else{
                alert('Fail');
            }
        });
        $A.enqueueAction(action);
    },
    
    Linechart : function(component,event,helper) {
        var jsonData = component.get("v.data");
        var dataObj = JSON.parse(jsonData);

        var xAxis = [];
                for(var i = 0 ; i < dataObj.length ; i++){
                    xAxis.push(dataObj[i].name);
                    console.log('Array ==>');
                    console.log(dataObj[i]);
                }
                console.log(xAxis);

                

                component.set('v.xAxisCategories',xAxis);
                
        
        new Highcharts.Chart({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                renderTo: component.find("linechart").getElement(),
                type: 'line'
            },
            title: {
                text: component.get("v.chartTitle")+' (Line Chart)'
            },
            subtitle: {
                text: component.get("v.chartSubTitle")
            },
            xAxis: {
                // for(var i = 0 ; i < dataObj.length ; i++){
                    // categories:dataObj.y,
                // }
                categories: component.get("v.xAxisCategories"),
                crosshair: true
            },
            yAxis: {
                min: 0,
                title:
                {
                    text: component.get("v.yAxisParameter")
                    // text: dataObj[0].y
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name:'Year ( '+component.get('v.selectedYear')+' )',
                data:dataObj
            }]
 
        });
        component.set("v.Spinner", false);
    },
    
})