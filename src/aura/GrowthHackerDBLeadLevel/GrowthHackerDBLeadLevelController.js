({
	doInit : function(component, event, helper){
	    var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        var selectedYear    = today[0]+today[1]+today[2]+today[3];
        var sq              = today[5]+today[6];
        if(sq == 1 || sq == 2 || sq == 3 ) sq = 1;
        if(sq == 4 || sq == 5 || sq == 6 ) sq = 2;
        if(sq == 7 || sq == 8 || sq == 9 ) sq = 3;
        if(sq == 10 || sq == 11 || sq == 12 ) sq = 4;
        
	    component.set("v.selectedYear", selectedYear);
	    component.set("v.selectedQuarter", sq);
	   // component.set('v.DashboardType', 'Country');
		helper.doInit(component, event, helper, true);
		
		helper.getPickListValues(component, event, helper, 'Lead', 'LeadSource', 'LeadSource');
		helper.getPickListValues(component, event, helper, 'Lead', 'lead_level__c', 'LeadLevel');
// 		helper.getPickListValues(component, event, helper, 'Lead', 'CurrencyIsoCode', 'CurrencyIsoCode');
	},

	SelectLeadLevel: function(component, event, helper){
		var selectedLevel = event.getSource().get("v.label");
		var SelectedLevelList = component.get('v.SelectedLeadLevel');
		
		if(selectedLevel == 'Lead'){
			component.get('v.isLeadSelected')		== true ? component.set('v.isLeadSelected',false)		: component.set('v.isLeadSelected',true);
		}else if(selectedLevel == 'MQL'){
			component.get('v.isMQLSelected')		== true ? component.set('v.isMQLSelected',false)		: component.set('v.isMQLSelected',true);
		}else if(selectedLevel == 'C-Lead'){
			component.get('v.isC_LeadSelected')		== true ? component.set('v.isC_LeadSelected',false)		: component.set('v.isC_LeadSelected',true);
		}else{
			component.get('v.isNotDefinedSelected') == true ? component.set('v.isNotDefinedSelected',false)	: component.set('v.isNotDefinedSelected',true);
		}
        // if(!SelectedLevelList.includes(selectedLevel)){
        //     SelectedLevelList.push(selectedLevel);
        // }else{
        //     SelectedLevelList.splice(SelectedLevelList.indexOf(selectedLevel) ,1);
        // }
	},
	
	changeDashboardType : function(component, event, helper){
	    helper.doInit(component, event, helper);
	},
	
	updateData : function(component, event, helper){
	    helper.doInit(component, event, helper);
	},
	
	doneWaiting: function(component, event, helper) {
        component.set("v.Spinner", false);
    },
    
    waiting: function(component, event, helper) {
        component.set("v.Spinner", true);
	},
	
	// Open Model
	openModel: function(component, event, helper) {
		component.set("v.isOpen", true);
	},

	closeModel: function(component, event, helper) {
		component.set("v.isOpen", false);
	},

	likenClose: function(component, event, helper) {
		component.set("v.isOpen", false);
	},
})