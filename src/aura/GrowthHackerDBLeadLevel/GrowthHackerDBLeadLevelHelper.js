({
	doInit : function(component, event, helper){
	    component.set("v.Spinner", true);
		var leadDataAction  = component.get('c.LeadLevelData');
		var selectedYear    = component.get("v.selectedYear");
        var selectedQuarter = component.get("v.value");
        component.set('v.selectedQuarter',component.get("v.value"));
        var sq;
        if(selectedQuarter == 'Quarter 1') sq = 1;
        if(selectedQuarter == 'Quarter 2') sq = 2;
        if(selectedQuarter == 'Quarter 3') sq = 3;
        if(selectedQuarter == 'Quarter 4') sq = 4;
        component.set('v.selectedQuarterPickList',selectedQuarter);
        
		leadDataAction.setParams({
            'year' : selectedYear,
            'Quarter' : sq,
            'ls' : component.get('v.selectedSource'),
            'DataType' : component.get('v.DBType')
        });
		leadDataAction.setCallback(this, function(response) {
		    var State = response.getState();
            if(State == 'SUCCESS'){
                component.set('v.LeadData',response.getReturnValue());
                console.log(response.getReturnValue());
                helper.getYearData(component, event, helper, selectedYear);
            }else{
                alert('Something went wrong!');
                component.set("v.Spinner", false);
            }
		});
		$A.enqueueAction(leadDataAction);
	},
	
	getYearData : function(component, event, helper, selectedYear){
	   // var selectedYear    = component.get("v.selectedYear");
	    var action          = component.get('c.LeadLevelGetYearData');
	    
	    action.setParams({
	        'year' : selectedYear,
	        'ls' : component.get('v.selectedSource'),
	        'DataType' : component.get('v.DBType')
	    });
	    action.setCallback(this, function(response){
	        var state = response.getState();
	        if(state == 'SUCCESS'){
	            component.set('v.yearData',response.getReturnValue());
	            component.set("v.Spinner", false);
	           // console.log('response.getReturnValue()');
	           // console.log(response.getReturnValue());
	           // alert('SUCCESS');
	        }else{
	            alert('FAIL');
	            component.set("v.Spinner", false);
	        }
	    });
	    $A.enqueueAction(action);
	},
	
	updateData : function(component, event, helper){
        var selectedYear = component.get("v.selectedYear");
        var selectedQuarter = component.get("v.selectedQuarter");
        
        var updateDBTable = component.get('c.quarterDataChange');
        quarterDataChange.setParams({
            'year' : selectedYear,
            'quarter' : selectedQuarter
        });
        quarterDataChange.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                alert('Success');
                component.set("v.Spinner", false);
            }else{
                alert('Fail');
                component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(quarterDataChange);
    },
    
    SelectLeadLevel: function(component, event, helper){
        // var selectedLevel = event.getSource().get("v.value");
        // var SelectedLevelList = component.get('v.SelectedLeadLevel');

        // if(!SelectedLevelList.includes(selectedLevel)){
        //     SelectedLevelList.push(selectedLevel);
        // }else{
        //     var index = SelectedLevelList.indexOf(selectedLevel);
        //     alert(index);
        //     SelectedLevelList.pop(index);
        // }
        // alert(SelectedLevelList);
	},
	
	getPickListValues : function(component, event, helper, objectName, FieldName, AttributName){
        var status = component.get("c.getPickListValues");
        status.setParams({
            'ObjName' : objectName,
            'FieldName' : FieldName
        });
        status.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            component.set("v."+AttributName, allValues);
        });
        $A.enqueueAction(status);
    }
})