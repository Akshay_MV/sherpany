({
    NotificationToast : function(component, event, helper, toastTitle, toastType, Msg ) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: toastTitle,
            type: toastType,
            message: Msg
        });
        toastEvent.fire();
    }
})