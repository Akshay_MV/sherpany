({
	doInit : function(component, event, helper){
	    var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        var selectedYear    = today[0]+today[1]+today[2]+today[3];
        var sq              = today[5]+today[6];
        if(sq == 1 || sq == 2 || sq == 3 ) sq = 1;
        if(sq == 4 || sq == 5 || sq == 6 ) sq = 2;
        if(sq == 7 || sq == 8 || sq == 9 ) sq = 3;
        if(sq == 10 || sq == 11 || sq == 12 ) sq = 4;
        
	    component.set("v.selectedYear", selectedYear);
	    component.set("v.selectedQuarter", sq);
		helper.doInit(component, event, helper, true);
		
		helper.getPickListValues(component, event, helper, 'Lead', 'LeadSource', 'LeadSource');
// 		helper.getPickListValues(component, event, helper, 'Lead', 'CurrencyIsoCode', 'CurrencyIsoCode');
	},
	
	updateData : function(component, event, helper){
	    helper.doInit(component, event, helper);
	},
	
	doneWaiting: function(component, event, helper) {
        component.set("v.Spinner", false);
    },
    
    waiting: function(component, event, helper) {
        component.set("v.Spinner", true);
    },
})