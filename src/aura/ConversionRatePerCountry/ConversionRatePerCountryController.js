({
	doInit : function(component, event, helper){
		helper.doInit(component, event, helper);
		helper.getPickListValues(component, event, helper, 'Opportunity', 'StageName', 'OppStageList');
		helper.getUserNameData(component, event, helper);
	},
	
	getData : function(component, event, helper){
        component.set("v.Spinner", true);
	    helper.getData(component, event, helper);
	},
	
	doneWaiting: function(component, event, helper) {
        //component.set("v.Spinner", false);
    },
    
    waiting: function(component, event, helper) {
        //component.set("v.Spinner", true);
    },

   openModel: function(component, event, helper) {
      component.set("v.isModalOpen", true);
      component.set("v.selectedCheckBoxes",'');
   },
  
   closeModel: function(component, event, helper) {
      component.set("v.isModalOpen", false);
   },
  
   submitDetails: function(component, event, helper) {
      alert(component.get("v.selectedCheckBoxes"));
      component.set("v.isModalOpen", false);
   },
   
    selectTnC : function(component, event, helper){
        // alert('checked');
            var capturedCheckboxName = event.getSource().get("v.value");
            var selectedCheckBoxes =  component.get("v.selectedCheckBoxes");
        
            if(selectedCheckBoxes.indexOf(capturedCheckboxName) > -1){            
                selectedCheckBoxes.splice(selectedCheckBoxes.indexOf(capturedCheckboxName), 1);           
            }else{
                selectedCheckBoxes.push(capturedCheckboxName);
            }
            console.log(selectedCheckBoxes);
            component.set("v.selectedCheckBoxes", selectedCheckBoxes);
    },
})