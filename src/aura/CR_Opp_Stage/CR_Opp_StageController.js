({
	doInit : function(component, event, helper){
		component.set("v.Spinner", true);
        var date = new Date(), y = date.getFullYear(), m = date.getMonth();
        var firstDay = new Date(y, m, 1);
        var lastDay = new Date(y, m + 1, 0);
        
	    component.set("v.selectedStartDate",$A.localizationService.formatDate(firstDay, "yyyy-MM-dd") );
	    component.set("v.selectedEndDate",$A.localizationService.formatDate(lastDay, "yyyy-MM-dd"));
        
		helper.doInit(component, event, helper);
	},

	updateData : function (component, event, helper){
		component.set("v.Spinner", true);
        component.set("v.isOpen", false);

		helper.doInit(component, event, helper);
	},
    
})