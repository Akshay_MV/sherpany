({
	doInit : function(component, event, helper){
		component.set("v.Spinner", true);
		var dateObj = new Date();
		var month = dateObj.getUTCMonth() + 1; //months from 1-12
		var day = dateObj.getUTCDate();
		var year = dateObj.getUTCFullYear();
		component.set('v.selectedYear',year);
		component.set('v.selectedMonth',month);

		helper.doInit(component, event, helper);
		// helper.getPickListValues(component, event, helper, 'Lead', 'LeadSource', 'LeadSource');
	},

	updateData : function (component, event, helper){
		component.set("v.Spinner", true);
		helper.doInit(component, event, helper);
	}
})