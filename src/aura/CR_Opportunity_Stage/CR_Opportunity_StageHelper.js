({
	doInit : function(component, event, helper){

		var getDefaults =  component.get('c.getDefault');
		getDefaults.setParams({
			year	: component.get('v.selectedYear'),
			month	: component.get('v.selectedMonth'),
			// ls		: component.get('v.selectedSource')
		});
		getDefaults.setCallback(this, function(response){
			var state = response.getState();
			if(state == 'SUCCESS'){
				console.log('Got this from APEX');
				console.log(response.getReturnValue());
				component.set('v.OppFieldHistory',response.getReturnValue());
				component.set("v.Spinner", false);
			}else{
				alert('Fail');
				component.set("v.Spinner", false);
			}
		});
		$A.enqueueAction(getDefaults);
	},

	getPickListValues : function(component, event, helper, objectName, FieldName, AttributName){
        // var status = component.get("c.getPickListValues");
        // status.setParams({
        //     'ObjName' : objectName,
        //     'FieldName' : FieldName
        // });
        // status.setCallback(this, function(response) {
        //     var allValues = response.getReturnValue();
        //     component.set("v."+AttributName, allValues);
        // });
        // $A.enqueueAction(status);
    }
})