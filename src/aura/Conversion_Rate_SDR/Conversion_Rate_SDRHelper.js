({
	doInit : function(component, event, helper) {
        var action = component.get('c.getDefault');
        action.setParams({
            year        : component.get('v.selectedYear'),
			month       : component.get('v.selectedMonth'),
            ls          : component.get('v.selectedSource'),
            userNames   : component.get('v.selectedCheckBoxes')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state = 'SUCCESSS'){
                // alert('Success');
                console.log(response.getReturnValue());
                component.set('v.SDRData',response.getReturnValue());
                component.set("v.Spinner", false);
            }else{
                alert('Fail');
            }
        });
        $A.enqueueAction(action);
	},
	
	getUserNameData : function(component, event, helper){
        var action = component.get('c.UserName');
        action.setParams({
            Year : component.get('v.selectedYear')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                component.set('v.userNames',response.getReturnValue());
                // console.log(response.getReturnValue());
                // alert('SUCCESS');
                component.set("v.Spinner", false);
            }else{
                alert('Something went wrong!');
                component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);
    },
	
	getPickListValues : function(component, event, helper, objectName, FieldName, AttributName){
        var status = component.get("c.getPickListValues");
        status.setParams({
            'ObjName' : objectName,
            'FieldName' : FieldName
        });
        status.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            component.set("v."+AttributName, allValues);
        });
        $A.enqueueAction(status);
    },
})