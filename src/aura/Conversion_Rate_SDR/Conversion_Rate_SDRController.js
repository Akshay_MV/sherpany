({
	doInit : function(component, event, helper) {
	    component.set("v.Spinner", true);
		var dateObj = new Date();
		var month = dateObj.getUTCMonth() + 1; //months from 1-12
		var day = dateObj.getUTCDate();
		var year = dateObj.getUTCFullYear();
		component.set('v.selectedYear',year);
		component.set('v.selectedMonth',month);
		
		helper.doInit(component, event, helper);
		helper.getPickListValues(component, event, helper, 'Lead', 'LeadSource', 'LeadSource');
		helper.getUserNameData(component, event, helper);
	},
	
	updateData : function(component, event, helper){
		component.set("v.Spinner", true);
		helper.doInit(component, event, helper);
// 		if(component.get("v.graph")){
// 			helper.chartLoad(component,event,helper);
// 		}
	},
	
	selectTnC : function(component, event, helper){
        // alert('checked');
            var capturedCheckboxName = event.getSource().get("v.value");
            var selectedCheckBoxes =  component.get("v.selectedCheckBoxes");
        
            if(selectedCheckBoxes.indexOf(capturedCheckboxName) > -1){            
                selectedCheckBoxes.splice(selectedCheckBoxes.indexOf(capturedCheckboxName), 1);           
            }else{
                selectedCheckBoxes.push(capturedCheckboxName);
            }
            console.log(selectedCheckBoxes);
			component.set("v.selectedCheckBoxes", selectedCheckBoxes);
    },
	
	searchUser : function(component, event, helper){
		component.set("v.Spinner", true);
		helper.doInit(component, event, helper);
		helper.getPickListValues(component, event, helper, 'Lead', 'LeadSource', 'LeadSource');
		helper.getUserNameData(component, event, helper);
		component.set("v.isModalOpen", false);
// 		if(component.get('v.graph')){ 
// 			helper.chartLoad(component,event,helper);
// 		}
	},
	
	openModel: function(component, event, helper) {
		component.set("v.isModalOpen", true);
		component.set("v.selectedCheckBoxes",'');
	},
	
	closeModel: function(component, event, helper) {
		component.set("v.isModalOpen", false);
	},
})