({
     doInit: function(component, event, helper) {
        // helper.onchangedata(component,event,helper);
        var action = component.get('c.getDefault');
        var rptName = component.get("v.rptName");
        console.log('rptName'+rptName);
        action.setParams({
            'rptName': rptName
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                component.set('v.options', response.getReturnValue());
                var result = response.getReturnValue();
                console.log(response.getReturnValue());
                var selectedIds = [];
                var selectedNames =[];
                for (var i = 0; i < result.length; i++) {
                    if (result[i].isSelected) {
                        selectedIds.push(result[i].UserId);
                    }
                    if(result[i].ischecked){
                        selectedNames.push(result[i].UserName);
                    }
                }
                component.set("v.selectedUser", selectedIds);
                component.set("v.selectedUserName",selectedNames);
                component.set("v.Spinner",false);
            } else {
                alert('Fail');
            }
        });
        $A.enqueueAction(action);
    },
    saveSetUp: function(component, event, helper, selectedId, selectedName) {
        var action = component.get("c.saveSetup");
        var rptName = component.get("v.rptName");
        action.setParams({
            lstUserId: selectedId,
            lstUserName: selectedName,
            'rptName': rptName
        });
        action.setCallback(this, function(e) {
            if (e.getState() == 'SUCCESS') {
                var result = e.getReturnValue();
                helper.NotificationToast(component, event, helper,'SUCCESS!','success','SetUp is Saved Successfully');
                component.set("v.Spinner",false);
            }
        });
        $A.enqueueAction(action);
    },
    selectUser: function(component, event, helper) {
        var capturedCheckboxName = event.getSource().get("v.value");
        var selectedCheckBoxes = component.get("v.selectedUserName");
        var selectedCheckBoxes1 = component.get("v.selectedUser");
        var rpt= component.get("v.rptName");
        if(rpt == 'Conversion Rate SDR'){
            if (selectedCheckBoxes.indexOf(capturedCheckboxName) > -1) {
                selectedCheckBoxes.splice(selectedCheckBoxes.indexOf(
                    capturedCheckboxName), 1);
            } else {
                selectedCheckBoxes.push(capturedCheckboxName);
            }
        }else{
            if (selectedCheckBoxes1.indexOf(capturedCheckboxName) > -1) {
                selectedCheckBoxes1.splice(selectedCheckBoxes1.indexOf(
                    capturedCheckboxName), 1);
            } else {
                selectedCheckBoxes1.push(capturedCheckboxName);
            }
        }

        component.set("v.selectedUser", selectedCheckBoxes1);
        
        component.set("v.selectedUserName", selectedCheckBoxes);
        console.log('selectedCheckBoxes'+selectedCheckBoxes);
    },
    
    NotificationToast : function(component, event, helper, toastTitle, toastType, Msg ) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: toastTitle,
            type: toastType,
            message: Msg
        });
        toastEvent.fire();
    }
})