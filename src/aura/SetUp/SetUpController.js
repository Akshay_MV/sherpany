({
    doInit: function(component, event, helper) {
        component.set("v.Spinner",true);
        var action = component.get('c.getDefault');
        var rptName = component.get("v.rptName");
        console.log('rptName'+rptName);
        action.setParams({
            'rptName': rptName
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                component.set('v.options', response.getReturnValue());
                var result = response.getReturnValue();
                console.log(response.getReturnValue());
                var selectedIds = [];
                var selectedNames = [];
                for (var i = 0; i < result.length; i++) {
                    if (result[i].isSelected) {
                        selectedIds.push(result[i].UserId);
                    }
                    if(result[i].ischecked){
                        selectedNames.push(result[i].UserName);
                    }
                }
                component.set("v.selectedUser", selectedIds);
                component.set("v.selectedUserName",selectedNames);
                component.set("v.Spinner",false);
            } else {
                alert('Fail');
            }
        });
        $A.enqueueAction(action);
    },

    getSelectedUser: function(component, event, helper) { 
        helper.selectUser(component, event, helper);
    },
    onchangedata :function(component,event,helper){
        component.set("v.Spinner",true);
        helper.doInit(component,event,helper);
    },
    SaveUser: function(component, event, helper) {
        component.set("v.Spinner",true);
        var selectedId = component.get("v.selectedUser");
        var selectedName = component.get("v.selectedUserName");
        if (selectedId.length > 0 || selectedName.length >0) {
            
            helper.saveSetUp(component,event,helper, selectedId, selectedName);
        } else {
            component.set("v.Spinner",false);
            helper.NotificationToast(component, event, helper,'Alert!','info','Please Select The User!');
        }
    },

})