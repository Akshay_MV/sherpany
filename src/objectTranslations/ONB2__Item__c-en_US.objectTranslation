<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <help><!-- shows minimum users / rooms / shareholders / ... --></help>
        <label><!-- Minimum Quantity --></label>
        <name>Minimum_Quantity__c</name>
    </fields>
    <fields>
        <help><!-- Defines whether or not this Item is considered during Invoice Run. --></help>
        <label><!-- Active --></label>
        <name>ONB2__Active__c</name>
    </fields>
    <fields>
        <help><!-- Additional description for items. --></help>
        <label><!-- Additional Description --></label>
        <name>ONB2__AdditionalDescription__c</name>
    </fields>
    <fields>
        <help><!-- Additional title for items. --></help>
        <label><!-- Additional Title --></label>
        <name>ONB2__AdditionalTitle__c</name>
    </fields>
    <fields>
        <help><!-- If set, transactions with an individual price are aggregated into one Transaction. --></help>
        <label><!-- Aggregate Indiv. Priced Transactions --></label>
        <name>ONB2__AggregateIndividualPriced__c</name>
    </fields>
    <fields>
        <help><!-- Defines the time period in months when the item will be considered in the invoice run, e.g every 12 months. --></help>
        <label><!-- Billing Period --></label>
        <name>ONB2__BillingPeriod__c</name>
    </fields>
    <fields>
        <help><!-- Defines the billing practice for recurring items. Possible values: Empty or &apos;Advance&apos;: This means that items are billed as soon as possible. &apos;Arrears&apos;: This means that items are billed as late as possible. --></help>
        <label><!-- Billing Practice --></label>
        <name>ONB2__BillingPractice__c</name>
        <picklistValues>
            <masterLabel>Advance</masterLabel>
            <translation>Invoicing in advance</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Arrears</masterLabel>
            <translation>Invoicing in arrears</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Determines the method by which quantities are calculated. --></help>
        <label><!-- Billing Type --></label>
        <name>ONB2__BillingType__c</name>
        <picklistValues>
            <masterLabel>One-Time</masterLabel>
            <translation><!-- One-Time --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Recurring</masterLabel>
            <translation><!-- Recurring --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Transactional</masterLabel>
            <translation><!-- Transactional --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Defines the billing unit. This unit is used for the price calculation of the billing period. --></help>
        <label><!-- Billing Unit --></label>
        <name>ONB2__BillingUnit__c</name>
        <picklistValues>
            <masterLabel>Day</masterLabel>
            <translation><!-- Day --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Month</masterLabel>
            <translation><!-- Month --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Allows to define a Charge model. --></help>
        <label><!-- Charge Model --></label>
        <name>ONB2__ChargeModel__c</name>
        <picklistValues>
            <masterLabel>Mark Down</masterLabel>
            <translation><!-- Mark Down --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Mark Up</masterLabel>
            <translation><!-- Mark Up --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The commission in percent for this line item used as a factor for the pos. total. --></help>
        <label><!-- Commission --></label>
        <name>ONB2__Commission__c</name>
    </fields>
    <fields>
        <help><!-- The correction value can be used to take contract values in account, which have been invoiced elsewhere. --></help>
        <label><!-- Contract Value Correction --></label>
        <name>ONB2__ContractValueCorrection__c</name>
    </fields>
    <fields>
        <help><!-- The Contract Value, which has already been invoiced. Will be calculated during the invoicing process. --></help>
        <label><!-- Contract Value Invoiced --></label>
        <name>ONB2__ContractValueInvoiced__c</name>
    </fields>
    <fields>
        <help><!-- The Remaining Contract Value is the difference between the Contract Value and the already invoiced values. --></help>
        <label><!-- Contract Value Remaining --></label>
        <name>ONB2__ContractValueRemaining__c</name>
    </fields>
    <fields>
        <help><!-- The Contract Value reflects the value of this item over the contract or item period. --></help>
        <label><!-- Contract Value --></label>
        <name>ONB2__ContractValue__c</name>
    </fields>
    <fields>
        <help><!-- The Reason why this item was deactivated or ended. --></help>
        <label><!-- Deactivation Reason --></label>
        <name>ONB2__DeactivationReason__c</name>
        <picklistValues>
            <masterLabel>Billed</masterLabel>
            <translation><!-- Billed --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Merged</masterLabel>
            <translation><!-- Merged --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The number of decimal places for the quantity as displayed on the invoice. --></help>
        <label><!-- Decimal Places for Quantity --></label>
        <name>ONB2__DecimalPlacesForQuantity__c</name>
    </fields>
    <fields>
        <help><!-- The number of decimal places for the unit price as displayed on the invoice. --></help>
        <label><!-- Decimal Places for Unit Price --></label>
        <name>ONB2__DecimalPlacesForUnitPrice__c</name>
    </fields>
    <fields>
        <help><!-- A description for this Item that may be printed at the InvoiceLineItem. --></help>
        <label><!-- Description --></label>
        <name>ONB2__Description__c</name>
    </fields>
    <fields>
        <help><!-- With the discount a percentage is deducted of the normal price of an item. --></help>
        <label><!-- Discount --></label>
        <name>ONB2__Discount__c</name>
    </fields>
    <fields>
        <help><!-- Defines wether a subtotal will be displayed after this. --></help>
        <label><!-- Display Subtotal After This Item --></label>
        <name>ONB2__DisplaySubtotalAfter__c</name>
    </fields>
    <fields>
        <help><!-- The last day on which this item is active. --></help>
        <label><!-- End Date --></label>
        <name>ONB2__EndDate__c</name>
    </fields>
    <fields>
        <help><!-- The monthly expected revenue. This is only valid for transactional items and will be analyzed for MRR/MRUR reporting. --></help>
        <label><!-- Expected Revenue --></label>
        <name>ONB2__ExpectedRevenue__c</name>
    </fields>
    <fields>
        <help><!-- Defines whether this item is included in the global monthly minimum base fee. --></help>
        <label><!-- Include In Monthly Minimum --></label>
        <name>ONB2__GlobalMonthlyMinimum__c</name>
    </fields>
    <fields>
        <help><!-- If checked, the price tier quantity is determined independently of the item criterion. --></help>
        <label><!-- Ignore Criterion For Price Tier Quantity --></label>
        <name>ONB2__IgnoreCriterionForPriceTierQuantity__c</name>
    </fields>
    <fields>
        <help><!-- If checked the criterion field of transactions/sObjects will be ignored and no grouping will take place. --></help>
        <label><!-- Ignore Item Criterion --></label>
        <name>ONB2__IgnoreCriterion__c</name>
    </fields>
    <fields>
        <help><!-- The type of the invoice line item. If empty it defaults to product. --></help>
        <label><!-- Invoice Line Item Type --></label>
        <name>ONB2__InvoiceLineItemType__c</name>
        <picklistValues>
            <masterLabel>Information</masterLabel>
            <translation><!-- Information --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Defines a lead time in months for this item. Therefore items will be considered ahead of their start time. Recurring items only. --></help>
        <label><!-- Lead Time --></label>
        <name>ONB2__LeadTime__c</name>
    </fields>
    <fields>
        <help><!-- Holds the next booking detail date for this item. --></help>
        <label><!-- Next Booking Detail Start --></label>
        <name>ONB2__NextBookingDetail__c</name>
    </fields>
    <fields>
        <help><!-- Holds the date of the next billing date for that item. --></help>
        <label><!-- Next Service Period Start --></label>
        <name>ONB2__NextInvoice__c</name>
    </fields>
    <fields>
        <help><!-- The API name of an invoice line item field by which the line items are to be ordered. --></help>
        <label><!-- Order By --></label>
        <name>ONB2__OrderBy__c</name>
    </fields>
    <fields>
        <help><!-- The previously recorded end date for this item. This is used to detect and record subscription metric changes. DO NOT EDIT! --></help>
        <label><!-- Previous End Date --></label>
        <name>ONB2__PreviousEndDate__c</name>
    </fields>
    <fields>
        <help><!-- Holds the amount of the previous end amount. This is used to detect and record subscription metric changes. DO NOT EDIT! --></help>
        <label><!-- Previous End Metric --></label>
        <name>ONB2__PreviousEndMetric__c</name>
    </fields>
    <fields>
        <help><!-- Holds the amount of the previous start amount. This is used to detect and record subscription metric changes. DO NOT EDIT! --></help>
        <label><!-- Previous Start Metric --></label>
        <name>ONB2__PreviousStartMetric__c</name>
    </fields>
    <fields>
        <help><!-- The price type of this item. Will be ignored, if there are price tiers. --></help>
        <label><!-- Price Type --></label>
        <name>ONB2__PriceType__c</name>
        <picklistValues>
            <masterLabel>Default</masterLabel>
            <translation><!-- Default --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Flat</masterLabel>
            <translation><!-- Flat --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The price of this item. Will be ignored, if there are price tiers. --></help>
        <label><!-- Price --></label>
        <name>ONB2__Price__c</name>
    </fields>
    <fields>
        <help><!-- The product group of this subscription item. --></help>
        <label><!-- Product Group --></label>
        <name>ONB2__ProductGroup__c</name>
    </fields>
    <fields>
        <help><!-- The quantity used to bill recurring and one-time items. Has no effect on items that are billed on a transactional basis. --></help>
        <label><!-- Quantity --></label>
        <name>ONB2__Quantity__c</name>
    </fields>
    <fields>
        <help><!-- Contains the aggregated quantities of the invoice line items per period. --></help>
        <label><!-- Timed Quota Period --></label>
        <name>ONB2__QuotaQuantityPeriod__c</name>
    </fields>
    <fields>
        <help><!-- If the quantity over all line items within a year from the start date of the item is greater than the timed quota, the price is taken from the price tier configuration instead of the item price field. --></help>
        <label><!-- Timed Quota --></label>
        <name>ONB2__QuotaQuantity__c</name>
    </fields>
    <fields>
        <help><!-- If checked, the display order of the invoice line items created from this item is reversed. --></help>
        <label><!-- Reverse --></label>
        <name>ONB2__Reverse__c</name>
    </fields>
    <fields>
        <help><!-- The position of this item in the table of all items. --></help>
        <label><!-- Sequence --></label>
        <name>ONB2__Sequence__c</name>
    </fields>
    <fields>
        <help><!-- Holds the id of the source child. The field is set during subscription building. --></help>
        <label><!-- Source Child Id --></label>
        <name>ONB2__SourceChildId__c</name>
    </fields>
    <fields>
        <help><!-- Holds the id of the source parent. The field is set during subscription building. The field is used to channel subscription updates in case of items with duplicate order numbers. --></help>
        <label><!-- Source Parent Id --></label>
        <name>ONB2__SourceParentId__c</name>
    </fields>
    <fields>
        <help><!-- The first day from which this item is active --></help>
        <label><!-- Start Date --></label>
        <name>ONB2__StartDate__c</name>
    </fields>
    <fields>
        <help><!-- The Subscription to which this record is an Item. --></help>
        <label><!-- Subscription --></label>
        <name>ONB2__Subscription__c</name>
        <relationshipLabel><!-- Items --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Valid for recurring items: the next invoice date will be syncronized with the chosen start date. --></help>
        <label><!-- Sync With --></label>
        <name>ONB2__SyncWith__c</name>
        <picklistValues>
            <masterLabel>Start of next fiscal year</masterLabel>
            <translation><!-- Start of next fiscal year --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Start of next year</masterLabel>
            <translation><!-- Start of next year --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The title or name of a product. --></help>
        <label><!-- Title --></label>
        <name>ONB2__Title__c</name>
    </fields>
    <fields>
        <help><!-- Define additional fields, which should be aggregated for transactions of this item. Format: {&quot;FIELDNAME1&quot;:&quot;FUNCTION&quot;,&quot;FIELDNAME2&quot;:&quot;FUNCTION&quot;}. Valid functions are: SUM, MIN, MAX --></help>
        <label><!-- Transaction Aggregation Fields --></label>
        <name>ONB2__TransactionAggregationFields__c</name>
    </fields>
    <fields>
        <help><!-- Define a source field for the commission tier price, which should be used during the transaction building. --></help>
        <label><!-- Transaction Commission Tier Price Field --></label>
        <name>ONB2__TransactionCommissionTierPriceField__c</name>
    </fields>
    <fields>
        <help><!-- Define a source field for the price, which should be used during the transaction building. --></help>
        <label><!-- Transaction Price Field --></label>
        <name>ONB2__TransactionPriceField__c</name>
    </fields>
    <fields>
        <help><!-- Define a source field for the price tier quantity, which should be used during the transaction building. --></help>
        <label><!-- Transaction Price Tier Quantity Field --></label>
        <name>ONB2__TransactionPriceTierQuantityField__c</name>
    </fields>
    <fields>
        <help><!-- Define a source field for the quantity, which should be used during the transaction building. --></help>
        <label><!-- Transaction Quantity Field --></label>
        <name>ONB2__TransactionQuantityField__c</name>
    </fields>
    <fields>
        <help><!-- Transactions of this type will be billed with this item. --></help>
        <label><!-- Transaction Type --></label>
        <name>ONB2__TransactionType__c</name>
        <picklistValues>
            <masterLabel>Default</masterLabel>
            <translation><!-- Default --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The unit associated with this Item. --></help>
        <label><!-- Unit --></label>
        <name>ONB2__Unit__c</name>
        <picklistValues>
            <masterLabel> </masterLabel>
            <translation><!--   --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Quantity Updated --></label>
        <name>Quantity_Updated__c</name>
    </fields>
    <fields>
        <help><!-- Datetime from last user quantity update from backend --></help>
        <label><!-- Quantity last update --></label>
        <name>Quantity_last_update__c</name>
    </fields>
    <fields>
        <label><!-- Slug --></label>
        <name>Slug__c</name>
    </fields>
    <validationRules>
        <errorMessage><!-- The item start date may not be after its end date --></errorMessage>
        <name>ONB2__ItemDatesOrderedCorrectly</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The item end date may not be after the subscription end date --></errorMessage>
        <name>ONB2__ItemEndDateNotAfterSubEndDate</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The item end date may not be before the subscription start date --></errorMessage>
        <name>ONB2__ItemEndDateNotBeforeSubStartDate</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The item start date may not be after the subscription end date --></errorMessage>
        <name>ONB2__ItemStartDateNotAfterSubEndDate</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The item start date may not be before the subscription start date --></errorMessage>
        <name>ONB2__ItemStartDateNotBeforeSubStartDate</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Please set a Quantity for recurring items --></errorMessage>
        <name>ONB2__RecurringQuantityRequired</name>
    </validationRules>
    <webLinks>
        <label><!-- ChangeProductGroup --></label>
        <name>ONB2__ChangeProductGroup</name>
    </webLinks>
    <webLinks>
        <label><!-- ChangeUnit --></label>
        <name>ONB2__ChangeUnit</name>
    </webLinks>
    <webLinks>
        <label><!-- NewItemFromProduct --></label>
        <name>ONB2__NewItemFromProduct</name>
    </webLinks>
</CustomObjectTranslation>
