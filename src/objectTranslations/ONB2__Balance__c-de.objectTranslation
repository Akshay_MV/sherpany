<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>false</plural>
        <value><!-- Saldo --></value>
    </caseValues>
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>true</plural>
        <value><!-- Saldi --></value>
    </caseValues>
    <caseValues>
        <caseType>Accusative</caseType>
        <plural>false</plural>
        <value><!-- Saldo --></value>
    </caseValues>
    <caseValues>
        <caseType>Accusative</caseType>
        <plural>true</plural>
        <value><!-- Saldi --></value>
    </caseValues>
    <caseValues>
        <caseType>Genitive</caseType>
        <plural>false</plural>
        <value><!-- Saldo --></value>
    </caseValues>
    <caseValues>
        <caseType>Genitive</caseType>
        <plural>true</plural>
        <value><!-- Saldi --></value>
    </caseValues>
    <caseValues>
        <caseType>Dative</caseType>
        <plural>false</plural>
        <value><!-- Saldo --></value>
    </caseValues>
    <caseValues>
        <caseType>Dative</caseType>
        <plural>true</plural>
        <value><!-- Saldi --></value>
    </caseValues>
    <fieldSets>
        <label><!-- Currency --></label>
        <name>Currency</name>
    </fieldSets>
    <fields>
        <help>Der Account zu dem dieser Saldo zugeordnet ist.</help>
        <label><!-- Account --></label>
        <name>ONB2__Account__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help>Positive Beträge stellen Forderungen dar. Negative Beträge stellen Guthaben dar.</help>
        <label><!-- Amount --></label>
        <name>ONB2__Amount__c</name>
    </fields>
    <fields>
        <help>Zeigt die interne Bankkonto-ID des Zahlungsdienstleisters an.</help>
        <label><!-- Bank Account Id --></label>
        <name>ONB2__BankAccountId__c</name>
    </fields>
    <fields>
        <help>Die Umrechnungsrate für den originalen Betrag des Zahlungseintrags.</help>
        <label><!-- Conversion Rate --></label>
        <name>ONB2__ConversionRate__c</name>
    </fields>
    <fields>
        <help>Das Datum, zu dem der Saldo angefallen ist.</help>
        <label><!-- Date --></label>
        <name>ONB2__Date__c</name>
    </fields>
    <fields>
        <help>Entwurfssalden werden von JustOn ignoriert und müssen per &apos;Fix Balances&apos; registriert werden.</help>
        <label><!-- Draft --></label>
        <name>ONB2__Draft__c</name>
    </fields>
    <fields>
        <help>Die referenzierte Mahnung für Mahngebühren.</help>
        <label><!-- Dunning --></label>
        <name>ONB2__Dunning__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help>Zeigt an, ob die verknüpfte Rechnung finalisiert wurde.</help>
        <label><!-- Invoice Is Finalized --></label>
        <name>ONB2__InvoiceIsFinalized__c</name>
    </fields>
    <fields>
        <help>Die Rechnung, der dieser Saldo zugeordnet ist. Eine Rechnung wird als bezahlt/gutgeschrieben angesehen, wenn die Summe ihrer Saldi Null ergibt.</help>
        <label><!-- Invoice --></label>
        <name>ONB2__Invoice__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help>Wird automatisch gesetzt, wenn der Saldo beim Finalisieren einer Rechnung aus PP-Feldern erzeugt wird.</help>
        <label><!-- IsPrepaid --></label>
        <name>ONB2__IsPrepaid__c</name>
    </fields>
    <fields>
        <help>Der Grund für die Sperrung dieses Saldos.</help>
        <label><!-- Locked Reason --></label>
        <name>ONB2__LockedReason__c</name>
        <picklistValues>
            <masterLabel>Refund via payment provider</masterLabel>
            <translation>Erstattung über einen Zahlungsdienstleister</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SEPA Export</masterLabel>
            <translation><!-- SEPA Export --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Gesperrte Salden sind von bestimmten Prozessen wie der Zuordnung zu Rechnungen während des Rechnungslaufs ausgeschlossen.</help>
        <label><!-- Locked --></label>
        <name>ONB2__Locked__c</name>
    </fields>
    <fields>
        <help>Der Saldo soll nicht automatisch einer Rechnung zugewiesen werden.</help>
        <label><!-- No Auto Assignment --></label>
        <name>ONB2__NoAutoAssignment__c</name>
    </fields>
    <fields>
        <help>Der originale Betrag des Zahlungseintrags vor der Umrechnung.</help>
        <label><!-- Original Amount --></label>
        <name>ONB2__OriginalAmount__c</name>
    </fields>
    <fields>
        <help>Die originale Währung des Zahlungseintrags vor der Umrechnung.</help>
        <label><!-- Original Currency Code --></label>
        <name>ONB2__OriginalCurrencyCode__c</name>
    </fields>
    <fields>
        <help>Der zugehörige Zahlungseintrag.</help>
        <label><!-- Payment Entry --></label>
        <name>ONB2__PaymentEntry__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help>Das Zahlmittel, welches verwendet wurde, um diesen Saldo einzuziehen.</help>
        <label><!-- Payment Instrument --></label>
        <lookupFilter>
            <errorMessage><!-- Only payment instruments of the same account can be linked to balances! --></errorMessage>
        </lookupFilter>
        <name>ONB2__PaymentInstrument__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help>Die Zahlart (z.B. Kreditkarte, Lastschrift...)</help>
        <label><!-- Payment Method --></label>
        <name>ONB2__PaymentMethod__c</name>
        <picklistValues>
            <masterLabel>Bank Transfer</masterLabel>
            <translation>Überweisung</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Cash</masterLabel>
            <translation>Barzahlung</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Credit Card</masterLabel>
            <translation>Kreditkarte</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Direct Debit</masterLabel>
            <translation>Lastschrift</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Der Zahlungsdienstleister (z.B. Paymill, Paypal, Wirecard...)</help>
        <label><!-- Payment Provider --></label>
        <name>ONB2__PaymentProvider__c</name>
        <picklistValues>
            <masterLabel>Paymill</masterLabel>
            <translation><!-- Paymill --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Paypal</masterLabel>
            <translation><!-- Paypal --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Wirecard</masterLabel>
            <translation><!-- Wirecard --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Gebühren, die bei der Zahlung über einen Zahlungsdienstleister angefallen sind.</help>
        <label><!-- Provider Fee --></label>
        <name>ONB2__ProviderFee__c</name>
    </fields>
    <fields>
        <help>Die Referenz enthält weitere zahlungsrelevante Informationen (Kundennummer, Transaktionsnummer, etc)</help>
        <label><!-- Reference --></label>
        <name>ONB2__Reference__c</name>
    </fields>
    <fields>
        <help>Zeigt den verbundenen Saldo eines zusammengehörigen Saldo-Paares, z.B. den Ausgangssaldo nach einer Saldoteilung.</help>
        <label><!-- Related Balance --></label>
        <name>ONB2__RelatedBalance__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Related Invoice Type --></label>
        <name>ONB2__RelatedInvoiceType__c</name>
    </fields>
    <fields>
        <help>Die verbundene Rechnung für den Rechnungsausgleich.</help>
        <label><!-- Related Invoice --></label>
        <name>ONB2__RelatedInvoice__c</name>
        <relationshipLabel><!-- Balances (Related Invoice) --></relationshipLabel>
    </fields>
    <fields>
        <help>Enthält eine Liste von SEPA-Feldern, die nicht gültig sind und einen Export des Saldos als SEPA-XML verhindern.</help>
        <label><!-- Sepa Validation Error --></label>
        <name>ONB2__SepaValidationError__c</name>
    </fields>
    <fields>
        <help>Ein verlinkter Fakturierungsplan schränkt die Zuweisung des Saldos auf Rechnungen des selben Fakturierungsplanes ein.</help>
        <label><!-- Subscription --></label>
        <lookupFilter>
            <errorMessage><!-- Subscriptions must link to the same account. --></errorMessage>
            <informationalMessage><!-- Subscriptions must link to the same account. --></informationalMessage>
        </lookupFilter>
        <name>ONB2__Subscription__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help>Die Transaktionsnummer des Zahlungsdienstleisters.</help>
        <label><!-- Transaction No. --></label>
        <name>ONB2__TransactionNo__c</name>
    </fields>
    <fields>
        <help>Der Typ des Saldos</help>
        <label><!-- Type --></label>
        <name>ONB2__Type__c</name>
        <picklistValues>
            <masterLabel>Clearing</masterLabel>
            <translation>Auszifferung</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Payment</masterLabel>
            <translation>Zahlung</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Payout</masterLabel>
            <translation>Auszahlung</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Prepayment</masterLabel>
            <translation>Vorkasse</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Refund</masterLabel>
            <translation>Rückerstattung</translation>
        </picklistValues>
    </fields>
    <gender><!-- Masculine --></gender>
    <nameFieldLabel><!-- # --></nameFieldLabel>
    <validationRules>
        <errorMessage><!-- A balance of type Payment or Prepayment needs to have an amount lower than zero. --></errorMessage>
        <name>ONB2__CheckPayment</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A balance of type Refund or Payout needs to have an amount greater than zero. --></errorMessage>
        <name>ONB2__CheckRefund</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- It is not allowed to clear the Locked Checkbox or to change the Locked Reason. --></errorMessage>
        <name>ONB2__RestrictLockedChange</name>
    </validationRules>
    <webLinks>
        <label><!-- ExportBalances --></label>
        <name>ONB2__ExportBalances</name>
    </webLinks>
    <webLinks>
        <label><!-- Refund --></label>
        <name>ONB2__Refund</name>
    </webLinks>
    <webLinks>
        <label><!-- UnregisterPayment --></label>
        <name>ONB2__UnregisterPayment</name>
    </webLinks>
</CustomObjectTranslation>
