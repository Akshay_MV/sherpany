<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldSets>
        <label><!-- Currency --></label>
        <name>Currency</name>
    </fieldSets>
    <fields>
        <help><!-- The account which received or provided this balance. --></help>
        <label><!-- Account --></label>
        <name>ONB2__Account__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Positive amounts are considered a debt. Negative amounts are considered a credit. --></help>
        <label><!-- Amount --></label>
        <name>ONB2__Amount__c</name>
    </fields>
    <fields>
        <help><!-- Shows the internal bank account id of the payment provider. --></help>
        <label><!-- Bank Account Id --></label>
        <name>ONB2__BankAccountId__c</name>
    </fields>
    <fields>
        <help><!-- The currency conversion rate for the original amount of the payment entry. --></help>
        <label><!-- Conversion Rate --></label>
        <name>ONB2__ConversionRate__c</name>
    </fields>
    <fields>
        <help><!-- The effective date of the balance. --></help>
        <label><!-- Date --></label>
        <name>ONB2__Date__c</name>
    </fields>
    <fields>
        <help><!-- Draft balances are ignored in all business processes. Use the &quot;Fix Balances&quot; API to register them. This field should always be false and only set during a process. --></help>
        <label><!-- Draft --></label>
        <name>ONB2__Draft__c</name>
    </fields>
    <fields>
        <help><!-- The related dunning in case of a dunning fee balance. --></help>
        <label><!-- Dunning --></label>
        <name>ONB2__Dunning__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Signals wheter the linked invoice has been finalized. --></help>
        <label><!-- Invoice Is Finalized --></label>
        <name>ONB2__InvoiceIsFinalized__c</name>
    </fields>
    <fields>
        <help><!-- The invoice, where this balance has been assigned to. An invoice is considered paid/settled, when the sum of all balances is zero. --></help>
        <label><!-- Invoice --></label>
        <name>ONB2__Invoice__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Is set automatically if the balance was creating from PP-Fields during finalize --></help>
        <label><!-- IsPrepaid --></label>
        <name>ONB2__IsPrepaid__c</name>
    </fields>
    <fields>
        <help><!-- The reason why this balance has been locked. --></help>
        <label><!-- Locked Reason --></label>
        <name>ONB2__LockedReason__c</name>
        <picklistValues>
            <masterLabel>Refund via payment provider</masterLabel>
            <translation><!-- Refund via payment provider --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SEPA Export</masterLabel>
            <translation><!-- SEPA Export --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Locked balances are excluded from certain business processes like assignment to invoices during the invoice run. --></help>
        <label><!-- Locked --></label>
        <name>ONB2__Locked__c</name>
    </fields>
    <fields>
        <help><!-- This balance should not be assigned automatically. --></help>
        <label><!-- No Auto Assignment --></label>
        <name>ONB2__NoAutoAssignment__c</name>
    </fields>
    <fields>
        <help><!-- The original amount of the payment entry before the currency conversion. --></help>
        <label><!-- Original Amount --></label>
        <name>ONB2__OriginalAmount__c</name>
    </fields>
    <fields>
        <help><!-- The original currency code of the payment entry before the currency conversion. --></help>
        <label><!-- Original Currency Code --></label>
        <name>ONB2__OriginalCurrencyCode__c</name>
    </fields>
    <fields>
        <help><!-- The Payment Entry which was used to create this Balance. --></help>
        <label><!-- Payment Entry --></label>
        <name>ONB2__PaymentEntry__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- The payment instrument which has been used to capture this balance. --></help>
        <label><!-- Payment Instrument --></label>
        <lookupFilter>
            <errorMessage><!-- Only payment instruments of the same account can be linked to balances! --></errorMessage>
        </lookupFilter>
        <name>ONB2__PaymentInstrument__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- The payment method, e.g. Credit Card, Direct Debit, etc. --></help>
        <label><!-- Payment Method --></label>
        <name>ONB2__PaymentMethod__c</name>
        <picklistValues>
            <masterLabel>Bank Transfer</masterLabel>
            <translation><!-- Bank Transfer --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Cash</masterLabel>
            <translation><!-- Cash --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Credit Card</masterLabel>
            <translation><!-- Credit Card --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Direct Debit</masterLabel>
            <translation><!-- Direct Debit --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The payment provider, e.g.Paymill, Paypal, Wirecard, etc. --></help>
        <label><!-- Payment Provider --></label>
        <name>ONB2__PaymentProvider__c</name>
        <picklistValues>
            <masterLabel>Paymill</masterLabel>
            <translation><!-- Paymill --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Paypal</masterLabel>
            <translation><!-- Paypal --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Wirecard</masterLabel>
            <translation><!-- Wirecard --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Charges incurred when paying through a payment service provider. --></help>
        <label><!-- Provider Fee --></label>
        <name>ONB2__ProviderFee__c</name>
    </fields>
    <fields>
        <help><!-- The reference may hold further information related to the payment (e.g. Customer No, Transaction No). --></help>
        <label><!-- Reference --></label>
        <name>ONB2__Reference__c</name>
    </fields>
    <fields>
        <help><!-- Shows the related balance of a matching balance pair, like the original balance after a balance split. --></help>
        <label><!-- Related Balance --></label>
        <name>ONB2__RelatedBalance__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Related Invoice Type --></label>
        <name>ONB2__RelatedInvoiceType__c</name>
    </fields>
    <fields>
        <help><!-- The related invoice for the settlement process. --></help>
        <label><!-- Related Invoice --></label>
        <name>ONB2__RelatedInvoice__c</name>
        <relationshipLabel><!-- Balances (Related Invoice) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Contains a list of SEPA-related fields that are not valid and prevent the balance from being exported as SEPA XML. --></help>
        <label><!-- Sepa Validation Error --></label>
        <name>ONB2__SepaValidationError__c</name>
    </fields>
    <fields>
        <help><!-- If a subscription is set, the balance will only be assigned to invoices of the same subscription. --></help>
        <label><!-- Subscription --></label>
        <lookupFilter>
            <errorMessage><!-- Subscriptions must link to the same account. --></errorMessage>
            <informationalMessage><!-- Subscriptions must link to the same account. --></informationalMessage>
        </lookupFilter>
        <name>ONB2__Subscription__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- The transaction number from the payment provider. --></help>
        <label><!-- Transaction No. --></label>
        <name>ONB2__TransactionNo__c</name>
    </fields>
    <fields>
        <help><!-- The type of the balance. --></help>
        <label><!-- Type --></label>
        <name>ONB2__Type__c</name>
        <picklistValues>
            <masterLabel>Clearing</masterLabel>
            <translation><!-- Clearing --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Payment</masterLabel>
            <translation><!-- Payment --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Payout</masterLabel>
            <translation><!-- Payout --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Prepayment</masterLabel>
            <translation><!-- Prepayment --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Refund</masterLabel>
            <translation><!-- Refund --></translation>
        </picklistValues>
    </fields>
    <validationRules>
        <errorMessage><!-- A balance of type Payment or Prepayment needs to have an amount lower than zero. --></errorMessage>
        <name>ONB2__CheckPayment</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A balance of type Refund or Payout needs to have an amount greater than zero. --></errorMessage>
        <name>ONB2__CheckRefund</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- It is not allowed to clear the Locked Checkbox or to change the Locked Reason. --></errorMessage>
        <name>ONB2__RestrictLockedChange</name>
    </validationRules>
    <webLinks>
        <label><!-- ExportBalances --></label>
        <name>ONB2__ExportBalances</name>
    </webLinks>
    <webLinks>
        <label><!-- Refund --></label>
        <name>ONB2__Refund</name>
    </webLinks>
    <webLinks>
        <label><!-- UnregisterPayment --></label>
        <name>ONB2__UnregisterPayment</name>
    </webLinks>
</CustomObjectTranslation>
