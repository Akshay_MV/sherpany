<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>false</plural>
        <value>Effort</value>
    </caseValues>
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>true</plural>
        <value>Efforts</value>
    </caseValues>
    <fields>
        <label><!-- Billable --></label>
        <name>Billable__c</name>
    </fields>
    <fields>
        <help><!-- Select the quarter where the effort has to be billed. Calculated date can be found in ON_Date. --></help>
        <label><!-- Billing Quarter --></label>
        <name>Billing_Quarter__c</name>
        <picklistValues>
            <masterLabel>Q1</masterLabel>
            <translation><!-- Q1 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Q2</masterLabel>
            <translation><!-- Q2 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Q3</masterLabel>
            <translation><!-- Q3 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Q4</masterLabel>
            <translation><!-- Q4 --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Contact --></label>
        <name>Contact__c</name>
        <relationshipLabel><!-- Efforts --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Shows the date of this effort --></help>
        <label><!-- Effort Date --></label>
        <name>EffortDate__c</name>
    </fields>
    <fields>
        <help><!-- The quantity that is used for the created transaction. --></help>
        <label><!-- Hours/Expense --></label>
        <name>Hours_Expense__c</name>
    </fields>
    <fields>
        <help><!-- If selected, the effort will be billed immediately. Overrides the billing quarter selection. --></help>
        <label><!-- Immediate Billing --></label>
        <name>ImmediateBilling__c</name>
    </fields>
    <fields>
        <help><!-- If true, the effort is billed --></help>
        <label><!-- Is billed --></label>
        <name>Is_billed__c</name>
    </fields>
    <fields>
        <help><!-- Calculated date for the billing of this effort --></help>
        <label><!-- OFF_Date --></label>
        <name>OFF_Date__c</name>
    </fields>
    <fields>
        <help><!-- Defines the SF Account ID for which the transaction is created --></help>
        <label><!-- ON_Account --></label>
        <name>ON_Account__c</name>
    </fields>
    <fields>
        <label><!-- ON_AddToCsv --></label>
        <name>ON_AddToCsv__c</name>
    </fields>
    <fields>
        <label><!-- ON_AddToTransactionTable --></label>
        <name>ON_AddToTransactionTable__c</name>
    </fields>
    <fields>
        <label><!-- ON_Date --></label>
        <name>ON_Date__c</name>
    </fields>
    <fields>
        <help><!-- If the default description should not be used, the alternative title will be set on the invoice line item --></help>
        <label><!-- Alternative Description --></label>
        <name>ON_Description__c</name>
    </fields>
    <fields>
        <help><!-- Relation to the invoice where this effort was billed. --></help>
        <label><!-- ON_Invoice --></label>
        <name>ON_Invoice__c</name>
        <relationshipLabel><!-- Efforts --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Holds conversion errors that occur during transaction building. --></help>
        <label><!-- ON_LastError --></label>
        <name>ON_LastError__c</name>
    </fields>
    <fields>
        <help><!-- Defines the item for which the transaction record is created. --></help>
        <label><!-- ON_OrderNo --></label>
        <name>ON_OrderNo__c</name>
    </fields>
    <fields>
        <help><!-- The unit price that will be used for the transaction. --></help>
        <label><!-- ON_Price --></label>
        <name>ON_Price__c</name>
    </fields>
    <fields>
        <help><!-- The quantity that is used for the transaction. --></help>
        <label><!-- ON_Quantity --></label>
        <name>ON_Quantity__c</name>
    </fields>
    <fields>
        <help><!-- Successfully converted objects are linked to this subscription. --></help>
        <label><!-- ON_Subscription --></label>
        <name>ON_Subscription__c</name>
        <relationshipLabel><!-- Efforts --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- If the default title should not be used, the alternative title will be set on the invoice line item --></help>
        <label><!-- Alternative Title --></label>
        <name>ON_Title__c</name>
    </fields>
    <fields>
        <help><!-- Defines the type of transaction that should be created form the object. --></help>
        <label><!-- ON_Type --></label>
        <name>ON_Type__c</name>
    </fields>
    <fields>
        <label><!-- Opportunity --></label>
        <name>Opportunity__c</name>
        <relationshipLabel><!-- Efforts --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Owner --></label>
        <name>Owner__c</name>
        <relationshipLabel><!-- Efforts --></relationshipLabel>
    </fields>
    <gender>Neuter</gender>
    <layouts>
        <layout>Effort Layout</layout>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
        <sections>
            <label><!-- JustOn Billing --></label>
            <section>JustOn Billing</section>
        </sections>
    </layouts>
    <validationRules>
        <errorMessage><!-- Dear Moneymaker, please fill out the field Immediate Billing or Billing Quarter. 
Immediate Billing: If its a Setup Effort (example: onboarding)
Billing Quarter: If the effort should be attached to the next invoice run (example: sending of a USB Stick) --></errorMessage>
        <name>FillOutImmediate_BillingORBilling_Quarte</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Owner must be set --></errorMessage>
        <name>Owner_must_be_set</name>
    </validationRules>
</CustomObjectTranslation>
