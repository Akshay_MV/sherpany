<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <help><!-- Primary account number (PAN) for credit card payments. / Buyer id for paypal payments. --></help>
        <label><!-- Account No. --></label>
        <name>ONB2__AccountNo__c</name>
    </fields>
    <fields>
        <help><!-- The received amount for invoice payments. / The paid amount for refunds. --></help>
        <label><!-- Amount --></label>
        <name>ONB2__Amount__c</name>
    </fields>
    <fields>
        <help><!-- The Message provided by the payment provider in case of an error. --></help>
        <label><!-- Callout Message --></label>
        <name>ONB2__CalloutMessage__c</name>
    </fields>
    <fields>
        <help><!-- The ISO currency code for the amount. If it is empty, the currency of the invoice is assumed. --></help>
        <label><!-- Currency --></label>
        <name>ONB2__Currency__c</name>
    </fields>
    <fields>
        <help><!-- Shows the due date of this payment. --></help>
        <label><!-- Date --></label>
        <name>ONB2__Date__c</name>
    </fields>
    <fields>
        <help><!-- The date on which the payment was exported. --></help>
        <label><!-- Export Date --></label>
        <name>ONB2__ExportDate__c</name>
    </fields>
    <fields>
        <help><!-- The file to which the payment was exported. --></help>
        <label><!-- Export File --></label>
        <name>ONB2__ExportFile__c</name>
    </fields>
    <fields>
        <help><!-- The financial Institution performing the transfer, e.g. VISA, MASTER, Paypal, etc. --></help>
        <label><!-- Financial Institution --></label>
        <name>ONB2__FinancialInstitution__c</name>
    </fields>
    <fields>
        <help><!-- The invoice the payment relates to. --></help>
        <label><!-- Invoice --></label>
        <name>ONB2__Invoice__c</name>
        <relationshipLabel><!-- Payments --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- The date when the payment has been received. --></help>
        <label><!-- Payment Date --></label>
        <name>ONB2__PaymentDate__c</name>
    </fields>
    <fields>
        <help><!-- Payment fees as transmitted by the payment provider. --></help>
        <label><!-- Payment Fees --></label>
        <name>ONB2__PaymentFees__c</name>
    </fields>
    <fields>
        <help><!-- The payment method, e.g. Bank Transfer, Cash, Credit Card, Direct Debit, Paypal, etc. --></help>
        <label><!-- Payment Method --></label>
        <name>ONB2__PaymentMethod__c</name>
        <picklistValues>
            <masterLabel>Bank Transfer</masterLabel>
            <translation><!-- Bank Transfer --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Cash</masterLabel>
            <translation><!-- Cash --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Credit</masterLabel>
            <translation><!-- Credit --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Credit Card</masterLabel>
            <translation><!-- Credit Card --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Direct Debit</masterLabel>
            <translation><!-- Direct Debit --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Manual</masterLabel>
            <translation><!-- Manual --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Paypal</masterLabel>
            <translation><!-- Paypal --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The third party payment provider used for the payment, e.g. Paypal, Wirecard. --></help>
        <label><!-- Payment Provider --></label>
        <name>ONB2__PaymentProvider__c</name>
        <picklistValues>
            <masterLabel>Paypal</masterLabel>
            <translation><!-- Paypal --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Wirecard</masterLabel>
            <translation><!-- Wirecard --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Shows the rate of this installment in relation to the grand total of the invoice. --></help>
        <label><!-- Rate --></label>
        <name>ONB2__Rate__c</name>
    </fields>
    <fields>
        <label><!-- Reference --></label>
        <name>ONB2__Reference__c</name>
    </fields>
    <fields>
        <help><!-- The line item related to this payment, e.g. the settlement line item corresponding to this payment. --></help>
        <label><!-- Related Line Item --></label>
        <name>ONB2__RelatedLineItem__c</name>
        <relationshipLabel><!-- Payments --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- The original payment for a refund. --></help>
        <label><!-- Related Payment --></label>
        <name>ONB2__RelatedPayment__c</name>
        <relationshipLabel><!-- Related Payments --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Determines the position of this payment item. --></help>
        <label><!-- Sequence --></label>
        <name>ONB2__Sequence__c</name>
    </fields>
    <fields>
        <help><!-- The payment status as one of Authorized, Canceled, Invalid, Received, Refunded, Pending; returned by the payment provider. --></help>
        <label><!-- Status --></label>
        <name>ONB2__Status__c</name>
        <picklistValues>
            <masterLabel>Authorized</masterLabel>
            <translation><!-- Authorized --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Canceled</masterLabel>
            <translation><!-- Canceled --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Invalid</masterLabel>
            <translation><!-- Invalid --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>New</masterLabel>
            <translation><!-- New --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pending</masterLabel>
            <translation><!-- Pending --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Received</masterLabel>
            <translation><!-- Received --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Refunded</masterLabel>
            <translation><!-- Refunded --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Specifies a name for payments of the type Installment. --></help>
        <label><!-- Title --></label>
        <name>ONB2__Title__c</name>
    </fields>
    <fields>
        <help><!-- The unique number returned by the payment provider for identifying the transaction. May be used for refunds. --></help>
        <label><!-- Transaction No. --></label>
        <name>ONB2__TransactionNo__c</name>
    </fields>
    <fields>
        <help><!-- The main payment type, e.g. &apos;Payment&apos; for inbound and &apos;Refund&apos; for outbound payments. --></help>
        <label><!-- Type --></label>
        <name>ONB2__Type__c</name>
        <picklistValues>
            <masterLabel>Payment</masterLabel>
            <translation><!-- Payment --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Refund</masterLabel>
            <translation><!-- Refund --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Return Debit Note</masterLabel>
            <translation><!-- Return Debit Note --></translation>
        </picklistValues>
    </fields>
    <validationRules>
        <errorMessage><!-- deprecated --></errorMessage>
        <name>ONB2__CreditMemoNotPayment</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- deprecated --></errorMessage>
        <name>ONB2__InvoiceNotRefund</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- deprecated --></errorMessage>
        <name>ONB2__RestrictStatusChangeForPayment</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- deprecated --></errorMessage>
        <name>ONB2__RestrictStatusChangeForRefund</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- deprecated --></errorMessage>
        <name>ONB2__StatusOnPaymentCreate</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- deprecated --></errorMessage>
        <name>ONB2__StatusOnRefundCreate</name>
    </validationRules>
    <webLinks>
        <label><!-- New --></label>
        <name>ONB2__New</name>
    </webLinks>
    <webLinks>
        <label><!-- Refund --></label>
        <name>ONB2__Refund</name>
    </webLinks>
</CustomObjectTranslation>
