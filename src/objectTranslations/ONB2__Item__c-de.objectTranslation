<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>false</plural>
        <value><!-- Posten --></value>
    </caseValues>
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>true</plural>
        <value><!-- Posten --></value>
    </caseValues>
    <caseValues>
        <caseType>Accusative</caseType>
        <plural>false</plural>
        <value><!-- Posten --></value>
    </caseValues>
    <caseValues>
        <caseType>Accusative</caseType>
        <plural>true</plural>
        <value><!-- Posten --></value>
    </caseValues>
    <caseValues>
        <caseType>Genitive</caseType>
        <plural>false</plural>
        <value><!-- Posten --></value>
    </caseValues>
    <caseValues>
        <caseType>Genitive</caseType>
        <plural>true</plural>
        <value><!-- Posten --></value>
    </caseValues>
    <caseValues>
        <caseType>Dative</caseType>
        <plural>false</plural>
        <value><!-- Posten --></value>
    </caseValues>
    <caseValues>
        <caseType>Dative</caseType>
        <plural>true</plural>
        <value><!-- Posten --></value>
    </caseValues>
    <fields>
        <help><!-- shows minimum users / rooms / shareholders / ... --></help>
        <label><!-- Minimum Quantity --></label>
        <name>Minimum_Quantity__c</name>
    </fields>
    <fields>
        <help>Bestimmt, ob dieser Posten in den Rechnungslauf einbezogen wird.</help>
        <label><!-- Active --></label>
        <name>ONB2__Active__c</name>
    </fields>
    <fields>
        <help>Gibt eine zusätzliche Beschreibung für den Posten an.</help>
        <label><!-- Additional Description --></label>
        <name>ONB2__AdditionalDescription__c</name>
    </fields>
    <fields>
        <help>Gibt einen zusätzlichen Titel für den Posten an.</help>
        <label><!-- Additional Title --></label>
        <name>ONB2__AdditionalTitle__c</name>
    </fields>
    <fields>
        <help>Bestimmt, ob Transaktionen mit individuellen Preisen in einer Transaktion aggregiert werden sollen.</help>
        <label><!-- Aggregate Indiv. Priced Transactions --></label>
        <name>ONB2__AggregateIndividualPriced__c</name>
    </fields>
    <fields>
        <help>Gibt den Zyklus (in Monaten) an, nach dem der Posten in den Rechnungslauf einbezogen wird.</help>
        <label><!-- Billing Period --></label>
        <name>ONB2__BillingPeriod__c</name>
    </fields>
    <fields>
        <help>Bestimmt die Rechnungsstellung für wiederkehrende Posten. Mögliche Werte: Leer oder &apos;Advance&apos;: Posten werden zum frühestmöglichen Zeitpunkt fakturiert. &quot;Arrears&quot;: Posten werden so spät wie möglich fakturiert.</help>
        <label><!-- Billing Practice --></label>
        <name>ONB2__BillingPractice__c</name>
        <picklistValues>
            <masterLabel>Advance</masterLabel>
            <translation>Rechnung im Voraus</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Arrears</masterLabel>
            <translation>Rechnung rückwirkend</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Bestimmt die Methode zur Ermittlung der Menge.</help>
        <label><!-- Billing Type --></label>
        <name>ONB2__BillingType__c</name>
        <picklistValues>
            <masterLabel>One-Time</masterLabel>
            <translation>Einmalig</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Recurring</masterLabel>
            <translation>Wiederkehrend</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Transactional</masterLabel>
            <translation>Transaktionsbasiert</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Legt die Zeiteinheit (Tag oder Monat) für den Abrechnungszeitraum fest, die für die Preisberechnung verwendet wird.</help>
        <label><!-- Billing Unit --></label>
        <name>ONB2__BillingUnit__c</name>
        <picklistValues>
            <masterLabel>Day</masterLabel>
            <translation>Tag</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Month</masterLabel>
            <translation>Monat</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Kann eine Provisionsberechnungsart für spezielle Preisbildungsmodelle festlegen (Mark Up/Mark Down).</help>
        <label><!-- Charge Model --></label>
        <name>ONB2__ChargeModel__c</name>
        <picklistValues>
            <masterLabel>Mark Down</masterLabel>
            <translation>Preissenkung</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Mark Up</masterLabel>
            <translation>Preisaufschlag</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Gibt eine Provision an zur Berechnung der Zwischensumme auf Basis des Einzelpreises und des angegebenen Prozentsatzes.</help>
        <label><!-- Commission --></label>
        <name>ONB2__Commission__c</name>
    </fields>
    <fields>
        <help>Der Korrekturwert kann dazu verwendet werden, den Auftragswert um anderweitig abgerechnete Were zu korrigieren.</help>
        <label><!-- Contract Value Correction --></label>
        <name>ONB2__ContractValueCorrection__c</name>
    </fields>
    <fields>
        <help>Der bereits fakturierte Auftragswert. Wird im Rahmen der Rechnungslegung automatisch ermittelt.</help>
        <label><!-- Contract Value Invoiced --></label>
        <name>ONB2__ContractValueInvoiced__c</name>
    </fields>
    <fields>
        <help>Der Restwert ergibt sich aus der Differenz des Auftragswertes und den bereits in Rechnung gestellten Werten.</help>
        <label><!-- Contract Value Remaining --></label>
        <name>ONB2__ContractValueRemaining__c</name>
    </fields>
    <fields>
        <help>Der Auftragswert gibt den Wert des Postens über die Vertragslaufzeit an.</help>
        <label><!-- Contract Value --></label>
        <name>ONB2__ContractValue__c</name>
    </fields>
    <fields>
        <help>Der Grund warum dieser Posten deaktiviert wurde.</help>
        <label><!-- Deactivation Reason --></label>
        <name>ONB2__DeactivationReason__c</name>
        <picklistValues>
            <masterLabel>Billed</masterLabel>
            <translation>Abgerechnet</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Merged</masterLabel>
            <translation>Fusioniert</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Die Anzahl der Dezimalstellen für die Darstellung der Menge auf der Rechnung.</help>
        <label><!-- Decimal Places for Quantity --></label>
        <name>ONB2__DecimalPlacesForQuantity__c</name>
    </fields>
    <fields>
        <help>Die Anzahl der Dezimalstellen für die Darstellung des Einzelpreises auf der Rechnung.</help>
        <label><!-- Decimal Places for Unit Price --></label>
        <name>ONB2__DecimalPlacesForUnitPrice__c</name>
    </fields>
    <fields>
        <help>Eine lange Beschreibung für den Posten, die mit dem Rechnungsposten gedruckt werden kann.</help>
        <label><!-- Description --></label>
        <name>ONB2__Description__c</name>
    </fields>
    <fields>
        <help>Der Rabatt gibt den Prozentsatz an, der von Standardpreis abgezogen wird.</help>
        <label><!-- Discount --></label>
        <name>ONB2__Discount__c</name>
    </fields>
    <fields>
        <help>Legt fest, ob nach diesem Posten eine Zwischensumme angezeigt werden soll.</help>
        <label><!-- Display Subtotal After This Item --></label>
        <name>ONB2__DisplaySubtotalAfter__c</name>
    </fields>
    <fields>
        <help>Das Datum, bis zu dem der Posten aktiv ist.</help>
        <label><!-- End Date --></label>
        <name>ONB2__EndDate__c</name>
    </fields>
    <fields>
        <help>Der erwartete Umsatz pro Monat. Dieser Wert ist nur für transaktionale Posten gültig und wird beim MRR-Reporting ausgewertet.</help>
        <label><!-- Expected Revenue --></label>
        <name>ONB2__ExpectedRevenue__c</name>
    </fields>
    <fields>
        <help>Bestimmt, ob dieser Posten auf den monatlichen Minimalbetrag angerechnet wird.</help>
        <label><!-- Include In Monthly Minimum --></label>
        <name>ONB2__GlobalMonthlyMinimum__c</name>
    </fields>
    <fields>
        <help>Wenn diese Option markiert ist, wird der Staffelpreis unabhängig vom Aggregationskriterium betimmt.</help>
        <label><!-- Ignore Criterion For Price Tier Quantity --></label>
        <name>ONB2__IgnoreCriterionForPriceTierQuantity__c</name>
    </fields>
    <fields>
        <help>Wenn diese Option markiert ist, wird das Aggregationskriterium für Transaktionen (oder andere Objekte) ignoriert, sodass die Rechnungsposten nicht gruppiert werden.</help>
        <label><!-- Ignore Item Criterion --></label>
        <name>ONB2__IgnoreCriterion__c</name>
    </fields>
    <fields>
        <help>Gibt den Typ des Rechnungspostens an. Bei leerem Feld wird standardmäßig Produkt benutzt.</help>
        <label><!-- Invoice Line Item Type --></label>
        <name>ONB2__InvoiceLineItemType__c</name>
        <picklistValues>
            <masterLabel>Information</masterLabel>
            <translation><!-- Information --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Definiert eine Vorlaufzeit in Monaten für diesen wiederkehrenden Posten. D.h. der Posten wird schon vor der eigentlichen Startzeit abgerechnet.</help>
        <label><!-- Lead Time --></label>
        <name>ONB2__LeadTime__c</name>
    </fields>
    <fields>
        <help>Enthält das Datum für den nächsten Buchungsvorschlag des Postens.</help>
        <label><!-- Next Booking Detail Start --></label>
        <name>ONB2__NextBookingDetail__c</name>
    </fields>
    <fields>
        <help>Enthält das nächste Rechnungsdatum des Postens.</help>
        <label><!-- Next Service Period Start --></label>
        <name>ONB2__NextInvoice__c</name>
    </fields>
    <fields>
        <help>Der API-Name eines Rechnungspostenfeldes, nach dessen Werten die Posten sortiert werden sollen.</help>
        <label><!-- Order By --></label>
        <name>ONB2__OrderBy__c</name>
    </fields>
    <fields>
        <help>Speichert das vorherige Enddatum. Der Wert wird verwendet, um Änderungen feststellen zu können. NICHT BEARBEITEN!</help>
        <label><!-- Previous End Date --></label>
        <name>ONB2__PreviousEndDate__c</name>
    </fields>
    <fields>
        <help>Speichert den vorherigen Endbetrag. Der Wert wird verwendet, um Änderungen feststellen zu können. NICHT BEARBEITEN!</help>
        <label><!-- Previous End Metric --></label>
        <name>ONB2__PreviousEndMetric__c</name>
    </fields>
    <fields>
        <help>Speichert den vorherigen Startbetrag. Der Wert wird verwendet, um Änderungen feststellen zu können. NICHT BEARBEITEN!</help>
        <label><!-- Previous Start Metric --></label>
        <name>ONB2__PreviousStartMetric__c</name>
    </fields>
    <fields>
        <help>Der Preistyp des Postens. Wird bei existierenden Preisstaffeln ignoriert.</help>
        <label><!-- Price Type --></label>
        <name>ONB2__PriceType__c</name>
        <picklistValues>
            <masterLabel>Default</masterLabel>
            <translation>Standard</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Flat</masterLabel>
            <translation>Flatrate</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Der Preis des Postens. Wird bei existierenden Preisstaffeln ignoriert.</help>
        <label><!-- Price --></label>
        <name>ONB2__Price__c</name>
    </fields>
    <fields>
        <help>Kann die Produktgruppe des Postens angeben.</help>
        <label><!-- Product Group --></label>
        <name>ONB2__ProductGroup__c</name>
    </fields>
    <fields>
        <help>Gibt die Menge für einmalig oder wiederkehrend abzurechnende Posten an. Betrifft nicht Verbrauchsdaten/transaktionale Posten.</help>
        <label><!-- Quantity --></label>
        <name>ONB2__Quantity__c</name>
    </fields>
    <fields>
        <help>Enthält die aggregierten Mengen der Rechnungspositionen pro Periode.</help>
        <label><!-- Timed Quota Period --></label>
        <name>ONB2__QuotaQuantityPeriod__c</name>
    </fields>
    <fields>
        <help>Wenn die Menge über alle Rechnungsposten innerhalb eines Jahres ab dem Startdatum des Postens größer als das Kontingent ist, wird der Preis aus der Preisstufenkonfiguration anstelle des Preisfeldes übernommen.</help>
        <label><!-- Timed Quota --></label>
        <name>ONB2__QuotaQuantity__c</name>
    </fields>
    <fields>
        <help>Wenn diese Option markiert ist, wird die Anzeigereihenfolge der aus diesem Posten erzeugten Rechnungsposten umgekehrt.</help>
        <label><!-- Reverse --></label>
        <name>ONB2__Reverse__c</name>
    </fields>
    <fields>
        <help>Bestimmt die Position des Postens in der Rechnungspostentabelle.</help>
        <label><!-- Sequence --></label>
        <name>ONB2__Sequence__c</name>
    </fields>
    <fields>
        <help>Enthält die Id des Quell-Kindobjektes. Das Feld wird beim Bauen des Fakturierungsplans gefüllt.</help>
        <label><!-- Source Child Id --></label>
        <name>ONB2__SourceChildId__c</name>
    </fields>
    <fields>
        <help>Enthält die Id des Quell-Elternobjektes. Das Feld wird beim Bauen des Fakturierungsplans gefüllt. Das Feld wird beim Aktualisieren von Fakturierungsplänen genutzt, wenn Posten mit doppelter Order-No. anliegen.</help>
        <label><!-- Source Parent Id --></label>
        <name>ONB2__SourceParentId__c</name>
    </fields>
    <fields>
        <help>Das Startdatum, ab dem der Posten aktiv ist.</help>
        <label><!-- Start Date --></label>
        <name>ONB2__StartDate__c</name>
    </fields>
    <fields>
        <help>Der Fakturierungsplan, mit dem dieser Posten verknüpft ist.</help>
        <label><!-- Subscription --></label>
        <name>ONB2__Subscription__c</name>
        <relationshipLabel><!-- Items --></relationshipLabel>
    </fields>
    <fields>
        <help>Gültig für wiederkehrende Zahlungen: das Datum der nächsten Rechnung wird mit dem gewählten Startdatum synchronisiert.</help>
        <label><!-- Sync With --></label>
        <name>ONB2__SyncWith__c</name>
        <picklistValues>
            <masterLabel>Start of next fiscal year</masterLabel>
            <translation>Beginn des nächsten Fiskaljahres</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Start of next year</masterLabel>
            <translation>Beginn des nächsten Jahres</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Gibt den Titel oder Namen eines Produktes an.</help>
        <label><!-- Title --></label>
        <name>ONB2__Title__c</name>
    </fields>
    <fields>
        <help>Zusätzliche Felder der Transaktion, die aggregiert werden sollen. Format: {&quot;FIELDNAME1&quot;:&quot;FUNCTION&quot;,&quot;FIELDNAME2&quot;:&quot;FUNCTION&quot;}. Gültige Funktionen sind: SUM, MIN, MAX</help>
        <label><!-- Transaction Aggregation Fields --></label>
        <name>ONB2__TransactionAggregationFields__c</name>
    </fields>
    <fields>
        <help>Gibt das Quellfeld für den Provisionsstaffelpreis beim Erzeugen der Transaktion an.</help>
        <label><!-- Transaction Commission Tier Price Field --></label>
        <name>ONB2__TransactionCommissionTierPriceField__c</name>
    </fields>
    <fields>
        <help>Definiert ein Quellfeld für den Preis beim Erzeugen von Transaktionen.</help>
        <label><!-- Transaction Price Field --></label>
        <name>ONB2__TransactionPriceField__c</name>
    </fields>
    <fields>
        <help>Definiert ein Quellfeld für die Staffelpreis-Menge beim Erzeugen von Transaktionen.</help>
        <label><!-- Transaction Price Tier Quantity Field --></label>
        <name>ONB2__TransactionPriceTierQuantityField__c</name>
    </fields>
    <fields>
        <help>Definiert ein Quellfeld für die Menge beim Erzeugen von Transaktionen.</help>
        <label><!-- Transaction Quantity Field --></label>
        <name>ONB2__TransactionQuantityField__c</name>
    </fields>
    <fields>
        <help>Gibt die Art der Transaktionen an, die mit diesem Posten berechnet werden sollen.</help>
        <label><!-- Transaction Type --></label>
        <name>ONB2__TransactionType__c</name>
        <picklistValues>
            <masterLabel>Default</masterLabel>
            <translation>Standard</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Gibt die Einheit an, die dem Posten zugewiesen ist.</help>
        <label><!-- Unit --></label>
        <name>ONB2__Unit__c</name>
        <picklistValues>
            <masterLabel> </masterLabel>
            <translation><!--   --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Quantity Updated --></label>
        <name>Quantity_Updated__c</name>
    </fields>
    <fields>
        <help><!-- Datetime from last user quantity update from backend --></help>
        <label><!-- Quantity last update --></label>
        <name>Quantity_last_update__c</name>
    </fields>
    <fields>
        <label><!-- Slug --></label>
        <name>Slug__c</name>
    </fields>
    <gender><!-- Masculine --></gender>
    <nameFieldLabel><!-- Order No. --></nameFieldLabel>
    <validationRules>
        <errorMessage><!-- The item start date may not be after its end date --></errorMessage>
        <name>ONB2__ItemDatesOrderedCorrectly</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The item end date may not be after the subscription end date --></errorMessage>
        <name>ONB2__ItemEndDateNotAfterSubEndDate</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The item end date may not be before the subscription start date --></errorMessage>
        <name>ONB2__ItemEndDateNotBeforeSubStartDate</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The item start date may not be after the subscription end date --></errorMessage>
        <name>ONB2__ItemStartDateNotAfterSubEndDate</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The item start date may not be before the subscription start date --></errorMessage>
        <name>ONB2__ItemStartDateNotBeforeSubStartDate</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Please set a Quantity for recurring items --></errorMessage>
        <name>ONB2__RecurringQuantityRequired</name>
    </validationRules>
    <webLinks>
        <label><!-- ChangeProductGroup --></label>
        <name>ONB2__ChangeProductGroup</name>
    </webLinks>
    <webLinks>
        <label><!-- ChangeUnit --></label>
        <name>ONB2__ChangeUnit</name>
    </webLinks>
    <webLinks>
        <label><!-- NewItemFromProduct --></label>
        <name>ONB2__NewItemFromProduct</name>
    </webLinks>
</CustomObjectTranslation>
