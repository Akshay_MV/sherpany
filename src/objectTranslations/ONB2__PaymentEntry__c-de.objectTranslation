<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>false</plural>
        <value><!-- Zahlungseintrag --></value>
    </caseValues>
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>true</plural>
        <value><!-- Zahlungseinträge --></value>
    </caseValues>
    <caseValues>
        <caseType>Accusative</caseType>
        <plural>false</plural>
        <value><!-- Zahlungseintrag --></value>
    </caseValues>
    <caseValues>
        <caseType>Accusative</caseType>
        <plural>true</plural>
        <value><!-- Zahlungseinträge --></value>
    </caseValues>
    <caseValues>
        <caseType>Genitive</caseType>
        <plural>false</plural>
        <value><!-- Zahlungseintrag --></value>
    </caseValues>
    <caseValues>
        <caseType>Genitive</caseType>
        <plural>true</plural>
        <value><!-- Zahlungseinträge --></value>
    </caseValues>
    <caseValues>
        <caseType>Dative</caseType>
        <plural>false</plural>
        <value><!-- Zahlungseintrag --></value>
    </caseValues>
    <caseValues>
        <caseType>Dative</caseType>
        <plural>true</plural>
        <value><!-- Zahlungseinträge --></value>
    </caseValues>
    <fields>
        <help>Zeigt die interne Bankkonto-ID des Zahlungsdienstleisters an.</help>
        <label><!-- Bank Account Id --></label>
        <name>ONB2__BankAccountId__c</name>
    </fields>
    <fields>
        <label><!-- Booking Date --></label>
        <name>ONB2__BookingDate__c</name>
    </fields>
    <fields>
        <label><!-- Credit --></label>
        <name>ONB2__Credit__c</name>
    </fields>
    <fields>
        <help>Name des Zahlers oder Zahlungsempfängers</help>
        <label><!-- Customer Name --></label>
        <name>ONB2__CustomerName__c</name>
    </fields>
    <fields>
        <label><!-- Debit --></label>
        <name>ONB2__Debit__c</name>
    </fields>
    <fields>
        <help>Zeigt eine vom Zahlungsdienstleister übermittelte Transaktion-ID an.</help>
        <label><!-- External Transaction Id --></label>
        <name>ONB2__ExternalTransactionId__c</name>
    </fields>
    <fields>
        <help><!-- The payment entry amount in another currency --></help>
        <label><!-- Foreign Amount --></label>
        <name>ONB2__ForeignAmount__c</name>
    </fields>
    <fields>
        <help><!-- The conversion rate for converting the payment amount to the foreign amount. (Foreign Amount = Rate × Payment Amount) --></help>
        <label><!-- Foreign Conversion Rate --></label>
        <name>ONB2__ForeignConversionRate__c</name>
    </fields>
    <fields>
        <help><!-- The currency code of the foreign amount. --></help>
        <label><!-- Foreign Currency Code --></label>
        <name>ONB2__ForeignCurrencyCode__c</name>
    </fields>
    <fields>
        <label><!-- Info --></label>
        <name>ONB2__Info__c</name>
    </fields>
    <fields>
        <help>Zeigt die Rechnung an, mit der der Zahlungseintrag verknüpft ist.</help>
        <label><!-- Invoice --></label>
        <name>ONB2__Invoice__c</name>
        <relationshipLabel><!-- Payment Entries --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Is set if the corresponding bank transaction has been deleted by the payment provider. --></help>
        <label><!-- IsDeleted --></label>
        <name>ONB2__IsDeleted__c</name>
    </fields>
    <fields>
        <help>Zeigt den letzten Fehler an, der beim Verarbeiten des Zahlungseintrags aufgetreten ist.</help>
        <label><!-- Last Error --></label>
        <name>ONB2__LastError__c</name>
    </fields>
    <fields>
        <help>Zeigt das Ergebnis des letzten Zuordnungsprozesses an. Enthält eine Liste zugeordneter Rechnungsnummern.</help>
        <label><!-- Last Matching Result --></label>
        <name>ONB2__LastMatchingResult__c</name>
    </fields>
    <fields>
        <label><!-- Payment Amount --></label>
        <name>ONB2__PaymentAmount__c</name>
    </fields>
    <fields>
        <help>Zeigt die Zahlungsart an, z.B. Kreditkarte, Lastschrift usw.</help>
        <label><!-- Payment Method --></label>
        <name>ONB2__PaymentMethod__c</name>
        <picklistValues>
            <masterLabel>Credit Card</masterLabel>
            <translation>Kreditkarte</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Direct Debit</masterLabel>
            <translation>Lastschrift</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Zeigt den externen Zahlungsdienstleister an, der für diesen Zahlungseintrag verwendet wird.</help>
        <label><!-- Payment Provider --></label>
        <name>ONB2__PaymentProvider__c</name>
        <picklistValues>
            <masterLabel>Paymill</masterLabel>
            <translation><!-- Paymill --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Zeigt spezielle Daten des externen Zahlungsdienstleisters an.</help>
        <label><!-- Provider Specific Data --></label>
        <name>ONB2__ProviderSpecificData__c</name>
    </fields>
    <fields>
        <label><!-- Reference --></label>
        <name>ONB2__Reference__c</name>
    </fields>
    <fields>
        <help>Zeigt den Status des Zuordnungsprozesses: Neu = keine Übereinstimmung (Standard), Zugeordnet = Übereinstimmungen gefunden, Konvertiert = Zahlungen wurden erzeugt.</help>
        <label><!-- Status --></label>
        <name>ONB2__Status__c</name>
        <picklistValues>
            <masterLabel>Canceled</masterLabel>
            <translation><!-- Canceled --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Converted</masterLabel>
            <translation>Konvertiert</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Matched</masterLabel>
            <translation>Zugeordnet</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>New</masterLabel>
            <translation>Neu</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Transferred</masterLabel>
            <translation>Übertragen</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Zeigt den Fakturierungsplan, mit dem der Zahlungseintrag verknüpft ist.</help>
        <label><!-- Subscription --></label>
        <name>ONB2__Subscription__c</name>
        <relationshipLabel><!-- Payment Entries --></relationshipLabel>
    </fields>
    <fields>
        <help>Zeigt eine vom Zahlungsdienstleister übermittelte eindeutige Nummer zur Identifikation der Transaktion.</help>
        <label><!-- Transaction No. --></label>
        <name>ONB2__TransactionNo__c</name>
    </fields>
    <gender><!-- Masculine --></gender>
    <nameFieldLabel><!-- # --></nameFieldLabel>
    <webLinks>
        <label><!-- Assign --></label>
        <name>ONB2__Assign</name>
    </webLinks>
    <webLinks>
        <label><!-- Delete --></label>
        <name>ONB2__Delete</name>
    </webLinks>
    <webLinks>
        <label><!-- Force_Figo_Authorization --></label>
        <name>ONB2__Force_Figo_Authorization</name>
    </webLinks>
    <webLinks>
        <label><!-- ImportCsvFile --></label>
        <name>ONB2__ImportCsvFile</name>
    </webLinks>
    <webLinks>
        <label><!-- Match --></label>
        <name>ONB2__Match</name>
    </webLinks>
    <webLinks>
        <label><!-- MatchInBatch --></label>
        <name>ONB2__MatchInBatch</name>
    </webLinks>
    <webLinks>
        <label><!-- RetrieveFigoTransactions --></label>
        <name>ONB2__RetrieveFigoTransactions</name>
    </webLinks>
    <webLinks>
        <label><!-- Transfer --></label>
        <name>ONB2__Transfer</name>
    </webLinks>
    <webLinks>
        <label><!-- Paid --></label>
        <name>Paid</name>
    </webLinks>
</CustomObjectTranslation>
