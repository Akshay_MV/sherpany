<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value><!-- Balance --></value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value><!-- Balance --></value>
    </caseValues>
    <fieldSets>
        <label><!-- Currency --></label>
        <name>Currency</name>
    </fieldSets>
    <fields>
        <help>Le compte concerné par ce solde</help>
        <label><!-- Account --></label>
        <name>ONB2__Account__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help>Montants positifs sont considérés comme des créances. Montants négatifs sont considérés comme des crédits.</help>
        <label><!-- Amount --></label>
        <name>ONB2__Amount__c</name>
    </fields>
    <fields>
        <help><!-- Shows the internal bank account id of the payment provider. --></help>
        <label><!-- Bank Account Id --></label>
        <name>ONB2__BankAccountId__c</name>
    </fields>
    <fields>
        <help><!-- The currency conversion rate for the original amount of the payment entry. --></help>
        <label><!-- Conversion Rate --></label>
        <name>ONB2__ConversionRate__c</name>
    </fields>
    <fields>
        <help>La date effective du solde.</help>
        <label><!-- Date --></label>
        <name>ONB2__Date__c</name>
    </fields>
    <fields>
        <help><!-- Draft balances are ignored in all business processes. Use the &quot;Fix Balances&quot; API to register them. This field should always be false and only set during a process. --></help>
        <label><!-- Draft --></label>
        <name>ONB2__Draft__c</name>
    </fields>
    <fields>
        <help>La relance en cas de frais de relance.</help>
        <label><!-- Dunning --></label>
        <name>ONB2__Dunning__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Signals wheter the linked invoice has been finalized. --></help>
        <label><!-- Invoice Is Finalized --></label>
        <name>ONB2__InvoiceIsFinalized__c</name>
    </fields>
    <fields>
        <help>La facture concernée par ce solde. Une facture est considérée comme réglée quand la somme des soldes est zéro.</help>
        <label><!-- Invoice --></label>
        <name>ONB2__Invoice__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Is set automatically if the balance was creating from PP-Fields during finalize --></help>
        <label><!-- IsPrepaid --></label>
        <name>ONB2__IsPrepaid__c</name>
    </fields>
    <fields>
        <help><!-- The reason why this balance has been locked. --></help>
        <label><!-- Locked Reason --></label>
        <name>ONB2__LockedReason__c</name>
        <picklistValues>
            <masterLabel>Refund via payment provider</masterLabel>
            <translation><!-- Refund via payment provider --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SEPA Export</masterLabel>
            <translation><!-- SEPA Export --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Locked balances are excluded from certain business processes like assignment to invoices during the invoice run. --></help>
        <label><!-- Locked --></label>
        <name>ONB2__Locked__c</name>
    </fields>
    <fields>
        <help>Ce solde ne doit pas être rapproché automatiquement.</help>
        <label><!-- No Auto Assignment --></label>
        <name>ONB2__NoAutoAssignment__c</name>
    </fields>
    <fields>
        <help><!-- The original amount of the payment entry before the currency conversion. --></help>
        <label><!-- Original Amount --></label>
        <name>ONB2__OriginalAmount__c</name>
    </fields>
    <fields>
        <help><!-- The original currency code of the payment entry before the currency conversion. --></help>
        <label><!-- Original Currency Code --></label>
        <name>ONB2__OriginalCurrencyCode__c</name>
    </fields>
    <fields>
        <help><!-- The Payment Entry which was used to create this Balance. --></help>
        <label><!-- Payment Entry --></label>
        <name>ONB2__PaymentEntry__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- The payment instrument which has been used to capture this balance. --></help>
        <label><!-- Payment Instrument --></label>
        <lookupFilter>
            <errorMessage><!-- Only payment instruments of the same account can be linked to balances! --></errorMessage>
        </lookupFilter>
        <name>ONB2__PaymentInstrument__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help>La méthode de paiement, ex. carte de crédit, prélèvement automatique, etc.</help>
        <label><!-- Payment Method --></label>
        <name>ONB2__PaymentMethod__c</name>
        <picklistValues>
            <masterLabel>Bank Transfer</masterLabel>
            <translation>Virement bancaire</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Cash</masterLabel>
            <translation>Comptant (en numéraire)</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Credit Card</masterLabel>
            <translation>Carte de crédit</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Direct Debit</masterLabel>
            <translation>Prélèvement automatique</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Le prestataire de paiement, tel Paymill, Paypal, Wirecard, etc.</help>
        <label><!-- Payment Provider --></label>
        <name>ONB2__PaymentProvider__c</name>
        <picklistValues>
            <masterLabel>Paymill</masterLabel>
            <translation><!-- Paymill --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Paypal</masterLabel>
            <translation><!-- Paypal --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Wirecard</masterLabel>
            <translation><!-- Wirecard --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Charges incurred when paying through a payment service provider. --></help>
        <label><!-- Provider Fee --></label>
        <name>ONB2__ProviderFee__c</name>
    </fields>
    <fields>
        <help>La référence peut comporter d&apos;autres informations concernant le paiement (ex. numéro client, numéro de transaction, etc.).</help>
        <label><!-- Reference --></label>
        <name>ONB2__Reference__c</name>
    </fields>
    <fields>
        <help>Solde afférent à un poste rapproché.</help>
        <label><!-- Related Balance --></label>
        <name>ONB2__RelatedBalance__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Related Invoice Type --></label>
        <name>ONB2__RelatedInvoiceType__c</name>
    </fields>
    <fields>
        <help>La facture afférente pour le processus de règlement.</help>
        <label><!-- Related Invoice --></label>
        <name>ONB2__RelatedInvoice__c</name>
        <relationshipLabel><!-- Balances (Related Invoice) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Contains a list of SEPA-related fields that are not valid and prevent the balance from being exported as SEPA XML. --></help>
        <label><!-- Sepa Validation Error --></label>
        <name>ONB2__SepaValidationError__c</name>
    </fields>
    <fields>
        <help>Si une souscription est en place, le solde va uniquement être assigné à des factures issues de la même souscription.</help>
        <label><!-- Subscription --></label>
        <lookupFilter>
            <errorMessage><!-- Subscriptions must link to the same account. --></errorMessage>
            <informationalMessage><!-- Subscriptions must link to the same account. --></informationalMessage>
        </lookupFilter>
        <name>ONB2__Subscription__c</name>
        <relationshipLabel><!-- Balances --></relationshipLabel>
    </fields>
    <fields>
        <help>Le numéro de transaction du prestataire de paiement.</help>
        <label><!-- Transaction No. --></label>
        <name>ONB2__TransactionNo__c</name>
    </fields>
    <fields>
        <help>Le type du solde.</help>
        <label><!-- Type --></label>
        <name>ONB2__Type__c</name>
        <picklistValues>
            <masterLabel>Clearing</masterLabel>
            <translation>Rapprochement</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Payment</masterLabel>
            <translation>Paiement</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Payout</masterLabel>
            <translation>Versement (Payout)</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Prepayment</masterLabel>
            <translation>Acompte</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Refund</masterLabel>
            <translation>Remboursement</translation>
        </picklistValues>
    </fields>
    <gender><!-- Masculine --></gender>
    <nameFieldLabel><!-- # --></nameFieldLabel>
    <startsWith><!-- Consonant --></startsWith>
    <validationRules>
        <errorMessage><!-- A balance of type Payment or Prepayment needs to have an amount lower than zero. --></errorMessage>
        <name>ONB2__CheckPayment</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A balance of type Refund or Payout needs to have an amount greater than zero. --></errorMessage>
        <name>ONB2__CheckRefund</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- It is not allowed to clear the Locked Checkbox or to change the Locked Reason. --></errorMessage>
        <name>ONB2__RestrictLockedChange</name>
    </validationRules>
    <webLinks>
        <label><!-- ExportBalances --></label>
        <name>ONB2__ExportBalances</name>
    </webLinks>
    <webLinks>
        <label><!-- Refund --></label>
        <name>ONB2__Refund</name>
    </webLinks>
    <webLinks>
        <label><!-- UnregisterPayment --></label>
        <name>ONB2__UnregisterPayment</name>
    </webLinks>
</CustomObjectTranslation>
