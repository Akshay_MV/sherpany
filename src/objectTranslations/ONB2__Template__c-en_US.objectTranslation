<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <help><!-- Enables data creation for QR codes in a particular format. Must be used together with a corresponding liquid template. --></help>
        <label><!-- Add QR Code --></label>
        <name>ONB2__AddQRCode__c</name>
        <picklistValues>
            <masterLabel>Swiss QR Code</masterLabel>
            <translation><!-- Swiss QR Code --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Defines the which fields of a Balance are displayed in columns. --></help>
        <label><!-- Balance Columns --></label>
        <name>ONB2__BalanceColumns__c</name>
    </fields>
    <fields>
        <help><!-- Defines which Balance types are shown on the PDF together with their title text. Encoded in JSON. --></help>
        <label><!-- Balance Types --></label>
        <name>ONB2__BalanceTypes__c</name>
    </fields>
    <fields>
        <help><!-- This block will only be printed on the invoice (per default at the bottom of the top right header) when the billing address is different from the recipient address. --></help>
        <label><!-- Billing Address --></label>
        <name>ONB2__BillingAddress__c</name>
    </fields>
    <fields>
        <help><!-- Specifies the name of an invoice line item field, e.g., ProductGroup__c. Each time the field value changes, a category header row is generated in the invoice table. --></help>
        <label><!-- Category Criterion --></label>
        <name>ONB2__CategoryCriterion__c</name>
    </fields>
    <fields>
        <help><!-- Defines the display of the category header, which is rendered as category header above the invoice line items. Can contain placeholders, like &quot;Group: [ProductGroup]&quot;. --></help>
        <label><!-- Category Label --></label>
        <name>ONB2__CategoryLabel__c</name>
    </fields>
    <fields>
        <help><!-- Name of the associated counter for this template. &apos;Default&apos; will be used, if empty. --></help>
        <label><!-- Counter --></label>
        <name>ONB2__Counter__c</name>
    </fields>
    <fields>
        <help><!-- Custom CSS to define and overrule the default layout and styling of an invoice pdf. --></help>
        <label><!-- Custom CSS --></label>
        <name>ONB2__CustomCSS__c</name>
    </fields>
    <fields>
        <help><!-- The format for all Date fields printed on the invoice. --></help>
        <label><!-- Date Format --></label>
        <name>ONB2__DateFormat__c</name>
        <picklistValues>
            <masterLabel>MM/dd/yyyy</masterLabel>
            <translation><!-- MM/dd/yyyy --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>dd. MMMM yyyy</masterLabel>
            <translation><!-- dd. MMMM yyyy --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>dd.MM.yyyy</masterLabel>
            <translation><!-- dd.MM.yyyy --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>dd/MM/yyyy</masterLabel>
            <translation><!-- dd/MM/yyyy --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>yyyy-MM-dd</masterLabel>
            <translation><!-- yyyy-MM-dd --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>yyyy.MM.dd</masterLabel>
            <translation><!-- yyyy.MM.dd --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>yyyy/MM/dd</masterLabel>
            <translation><!-- yyyy/MM/dd --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Configuration for custom decimal places in JSON notation. --></help>
        <label><!-- Decimal Places --></label>
        <name>ONB2__DecimalPlaces__c</name>
    </fields>
    <fields>
        <help><!-- Decimal Separator for all Number fields like Currency or Percent. --></help>
        <label><!-- Decimal Separator --></label>
        <name>ONB2__DecimalSeparator__c</name>
        <picklistValues>
            <masterLabel>,</masterLabel>
            <translation><!-- , --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>.</masterLabel>
            <translation><!-- . --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The description is used for deposit line items at proforrma invoices. --></help>
        <label><!-- Deposit Description --></label>
        <name>ONB2__DepositDescription__c</name>
    </fields>
    <fields>
        <help><!-- Defines the display of the grand total label in case of final invoices with deposit payments. Can contain placeholders. If empty, defaults to standard invoice behavior. --></help>
        <label><!-- Deposit Grand Total Label --></label>
        <name>ONB2__DepositGrandTotalLabel__c</name>
    </fields>
    <fields>
        <help><!-- The title is used for deposit line items at proforrma invoices. --></help>
        <label><!-- Deposit Title --></label>
        <name>ONB2__DepositTitle__c</name>
    </fields>
    <fields>
        <help><!-- You can use the following placeholders inside your text:  
[DiscountAmount]  
[DiscountRate]  
[DiscountPaymentDueDate] 
[PaymentDueDate]  
[PaymentDue]   
These placeholders will be replaced with the corresponding values. --></help>
        <label><!-- Discount Message --></label>
        <name>ONB2__DiscountMessage__c</name>
    </fields>
    <fields>
        <help><!-- Will be printed to the PDF. eg. Invoice, Credit Memo, Reminder, Dunning --></help>
        <label><!-- Display Type --></label>
        <name>ONB2__DisplayType__c</name>
    </fields>
    <fields>
        <help><!-- The description of line items of type Dunning. --></help>
        <label><!-- Dunning Description --></label>
        <name>ONB2__DunningDescription__c</name>
    </fields>
    <fields>
        <help><!-- The description of line items of type Dunning Fee. --></help>
        <label><!-- Dunning Fee Description --></label>
        <name>ONB2__DunningFeeDescription__c</name>
    </fields>
    <fields>
        <help><!-- The title of line items of type Dunning Fee. --></help>
        <label><!-- Dunning Fee Title --></label>
        <name>ONB2__DunningFeeTitle__c</name>
    </fields>
    <fields>
        <help><!-- The title of line items of type Dunning. --></help>
        <label><!-- Dunning Title --></label>
        <name>ONB2__DunningTitle__c</name>
    </fields>
    <fields>
        <help><!-- Defines whether the invoice pdf is attached to any outgoing email. --></help>
        <label><!-- Attach Files To Outgoing Email --></label>
        <name>ONB2__EmailAttachPDF__c</name>
    </fields>
    <fields>
        <help><!-- The plain text body of the email. It will be ignored, if a html body is defined. --></help>
        <label><!-- Email Body --></label>
        <name>ONB2__EmailBody__c</name>
    </fields>
    <fields>
        <help><!-- Defines which file types will be attached to emails (comma separated list of file extensions). If empty (default), all files files are attached except for transaction table HTMLs. --></help>
        <label><!-- Email File Types --></label>
        <name>ONB2__EmailFileTypes__c</name>
    </fields>
    <fields>
        <help><!-- Define a rich html body text for emails. The plain email body will be ignored, if the rich body has some content. --></help>
        <label><!-- Email HTML Body --></label>
        <name>ONB2__EmailHtmlBody__c</name>
    </fields>
    <fields>
        <help><!-- Define an email sending address. It needs to be added as Organization-Wide Email Address as well. --></help>
        <label><!-- Email Sender --></label>
        <name>ONB2__EmailSender__c</name>
    </fields>
    <fields>
        <help><!-- Default e-mail subject. --></help>
        <label><!-- Email Subject --></label>
        <name>ONB2__EmailSubject__c</name>
    </fields>
    <fields>
        <help><!-- A long textarea for a footer. You may use HTML and CSS for formatting. --></help>
        <label><!-- Footer --></label>
        <name>ONB2__Footer__c</name>
    </fields>
    <fields>
        <help><!-- Group Separator for all Number fields like Currency. --></help>
        <label><!-- Grouping Separator --></label>
        <name>ONB2__GroupingSeparator__c</name>
        <picklistValues>
            <masterLabel>&apos;</masterLabel>
            <translation><!-- &apos; --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>,</masterLabel>
            <translation><!-- , --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>.</masterLabel>
            <translation><!-- . --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SPACE</masterLabel>
            <translation><!-- SPACE --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The text which contains informations about your company. --></help>
        <label><!-- Header --></label>
        <name>ONB2__Header__c</name>
    </fields>
    <fields>
        <help><!-- Left part of the information block above the text. --></help>
        <label><!-- Info Left --></label>
        <name>ONB2__InfoLeft__c</name>
    </fields>
    <fields>
        <help><!-- Right part of the information block above the text. --></help>
        <label><!-- Info Right --></label>
        <name>ONB2__InfoRight__c</name>
    </fields>
    <fields>
        <help><!-- Defines which installment fields are to be displayed as columns in the installment table. Defaults to &quot;Title__c;Date__c;Amount__c&quot;. --></help>
        <label><!-- Installment Columns --></label>
        <name>ONB2__InstallmentColumns__c</name>
    </fields>
    <fields>
        <help><!-- Specifies the text to be printed above the installment table. --></help>
        <label><!-- Installments Text --></label>
        <name>ONB2__InstallmentsText__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated --></label>
        <name>ONB2__InvoiceType__c</name>
    </fields>
    <fields>
        <help><!-- The language used for generating an invoice pdf for rendering labels and content; also used for QPAY and Paypal. --></help>
        <label><!-- Language --></label>
        <name>ONB2__Language__c</name>
        <picklistValues>
            <masterLabel>de</masterLabel>
            <translation><!-- de --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>en</masterLabel>
            <translation><!-- en --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>es</masterLabel>
            <translation><!-- es --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>fr</masterLabel>
            <translation><!-- fr --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>it</masterLabel>
            <translation><!-- it --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Controls whether the currency sign (e.g. $ or €) is displayed ahead or after the value. --></help>
        <label><!-- Leading Currency --></label>
        <name>ONB2__LeadingCurrency__c</name>
    </fields>
    <fields>
        <help><!-- The date format for [LastMonth], [CurrentMonth] and [NextMonth]. --></help>
        <label><!-- Month Format --></label>
        <name>ONB2__MonthFormat__c</name>
        <picklistValues>
            <masterLabel>MM/yyyy</masterLabel>
            <translation><!-- MM/yyyy --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>yyyy/MM</masterLabel>
            <translation><!-- yyyy/MM --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- If checked, discount information is only displayed for the entire invoice amount, not for subtotals. --></help>
        <label><!-- No Subtotal Discount --></label>
        <name>ONB2__NoSubtotalDiscount__c</name>
    </fields>
    <fields>
        <help><!-- If checked, tax information is only displayed for the entire invoice amount, not for subtotals. --></help>
        <label><!-- No Subtotal Tax --></label>
        <name>ONB2__NoSubtotalTax__c</name>
    </fields>
    <fields>
        <help><!-- The text displayed above the outstanding invoices table. --></help>
        <label><!-- Outstanding Invoices Text --></label>
        <name>ONB2__OutstandingInvoicesText__c</name>
    </fields>
    <fields>
        <help><!-- Append a table to the invoice which lists outstanding invoices, i.e. open invoices for the corresponding account. --></help>
        <label><!-- Outstanding Invoices --></label>
        <name>ONB2__OutstandingInvoices__c</name>
    </fields>
    <fields>
        <help><!-- Label override configuration in JSON notation. --></help>
        <label><!-- Override Labels --></label>
        <name>ONB2__OverrideLabels__c</name>
    </fields>
    <fields>
        <help><!-- This block defines the page header of the invoice/dunning. --></help>
        <label><!-- Page Header --></label>
        <name>ONB2__PageHeader__c</name>
    </fields>
    <fields>
        <help><!-- The default number of days in which the payment due. --></help>
        <label><!-- Default Payment Due --></label>
        <name>ONB2__PaymentDue__c</name>
    </fields>
    <fields>
        <help><!-- Payment info for bank transfer. --></help>
        <label><!-- Payment Info Bank Transfer --></label>
        <name>ONB2__PaymentInfoBankTransfer__c</name>
    </fields>
    <fields>
        <help><!-- Info for credit card payments. --></help>
        <label><!-- Payment Info Credit Card --></label>
        <name>ONB2__PaymentInfoCC__c</name>
    </fields>
    <fields>
        <help><!-- Info for direct debit payments. --></help>
        <label><!-- Payment Info Direct Debit --></label>
        <name>ONB2__PaymentInfoDD__c</name>
    </fields>
    <fields>
        <help><!-- Info for payments via Paypal. --></help>
        <label><!-- Payment Info PayPal --></label>
        <name>ONB2__PaymentInfoPaypal__c</name>
    </fields>
    <fields>
        <help><!-- The reference, which will be used for SEPA payments. May contain placeholders. --></help>
        <label><!-- Payment Reference --></label>
        <name>ONB2__PaymentReference__c</name>
    </fields>
    <fields>
        <help><!-- The Name of the invoice PDF. Can contain paceholders. The default is &quot;[InvoiceDate]_[InvoiceNo]_[AccountName].pdf&quot; --></help>
        <label><!-- PDF Name --></label>
        <name>ONB2__PdfName__c</name>
    </fields>
    <fields>
        <help><!-- This field contains a JSON configuration for additional options for the PDF renderer v2. --></help>
        <label><!-- PDF Options --></label>
        <name>ONB2__PdfOptions__c</name>
    </fields>
    <fields>
        <help><!-- The version of the PDF renderer. If empty, defaults to v1. For v2 liquid template files are required. --></help>
        <label><!-- PDF Renderer --></label>
        <name>ONB2__PdfRenderer__c</name>
        <picklistValues>
            <masterLabel>v1</masterLabel>
            <translation><!-- v1 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>v2</masterLabel>
            <translation><!-- v2 --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Address information of the invoice recipient. --></help>
        <label><!-- Recipient Address --></label>
        <name>ONB2__RecipientAddress__c</name>
    </fields>
    <fields>
        <help><!-- Specifies an optional reverse charge text to be printed below text 3. This is only visible when the invoice field ShowReverseCharge__c is &apos;EU&apos;. --></help>
        <label><!-- Reverse Charge Text EU --></label>
        <name>ONB2__ReverseChargeTextEU__c</name>
    </fields>
    <fields>
        <help><!-- Specifies an optional reverse charge text to be printed below text 3. This is only visible when the invoice field ShowReverseCharge__c is &apos;Non-EU&apos;. --></help>
        <label><!-- Tax Information Non-EU --></label>
        <name>ONB2__ReverseChargeTextNonEU__c</name>
    </fields>
    <fields>
        <help><!-- Specifies an optional reverse charge text to be printed below text 3. This is only visible when the invoice field ShowReverseCharge__c is true. --></help>
        <label><!-- Reverse Charge Text --></label>
        <name>ONB2__ReverseChargeText__c</name>
    </fields>
    <fields>
        <help><!-- The address of the sender is located above the recipient address. --></help>
        <label><!-- Sender Address --></label>
        <name>ONB2__SenderAddress__c</name>
    </fields>
    <fields>
        <help><!-- If an order discount is used, defines the display of the order discount label. Can contain placeholders, like &quot;Discount [ProductGroup] ([OrderDiscount])&quot;. If empty, defaults to the custom label SubTotalAEDiscount. --></help>
        <label><!-- Order Discount Label --></label>
        <name>ONB2__SubTotalDiscountLabel__c</name>
    </fields>
    <fields>
        <help><!-- If an order discount is used, defines the display of the discounted subtotal (net) label. Can contain placeholders, like &quot;Subtotal discounted [ProductGroup]&quot;. If empty, defaults to the custom label SubTotalNetDiscounted. --></help>
        <label><!-- Subtotal Discounted (net) Label --></label>
        <name>ONB2__SubTotalNetDiscountedLabel__c</name>
    </fields>
    <fields>
        <help><!-- The name of a field at the Invoice Line Item. If the field value changes, a subtotal row will be displayed at the invoice table. Example: TaxRate__c or ProductGroup__c. --></help>
        <label><!-- Subtotal Criterion --></label>
        <name>ONB2__SubtotalCriteria__c</name>
    </fields>
    <fields>
        <help><!-- Defines the display of the subtotal (gross) label. Can contain placeholders, like &quot;Subtotal [ProductGroup] (gross)&quot;. If empty, defaults to the custom label SubTotalGross. --></help>
        <label><!-- Subtotal (gross) Label --></label>
        <name>ONB2__SubtotalGrossLabel__c</name>
    </fields>
    <fields>
        <help><!-- Defines the display of the subtotal (net) label. Can contain placeholders, like &quot;Subtotal [ProductGroup]&quot;. If empty, defaults to the custom label SubTotalNet. --></help>
        <label><!-- Subtotal (net) Label --></label>
        <name>ONB2__SubtotalNetLabel__c</name>
    </fields>
    <fields>
        <help><!-- If an order discount is used, defines the display of the subtotal (net) label  without the discount. Can contain placeholders, like &quot;Subtotal [ProductGroup]&quot;. If empty, defaults to the custom label SubTotalNetNoDiscount. --></help>
        <label><!-- Subtotal Without Discount (net) Label --></label>
        <name>ONB2__SubtotalNetNoDiscountLabel__c</name>
    </fields>
    <fields>
        <help><!-- Determines the columns and their sequence of the invoice detail table, separated by semi-colon. When blank, defaults to: &quot;PosNo; Title__c+Description__c; UnitPriceNet__c; Quantity__c+Unit__c; TaxRate__c; PosTotalNet__c &quot; --></help>
        <label><!-- Table Columns --></label>
        <name>ONB2__TableColumns__c</name>
    </fields>
    <fields>
        <help><!-- You can insert &quot;[TaxRate]&quot; as a place holder within your text.
This place holder will be replaced by the respective values, e.g. &quot;7%&quot;. --></help>
        <label><!-- Tax Label --></label>
        <name>ONB2__TaxLabel__c</name>
    </fields>
    <fields>
        <help><!-- Defines which tax detail fields are to be displayed as columns in the tax table. Defaults to &quot;Name;AppliedTaxRule__c;Rate__c;Amount__c&quot;. If there is no tax detail, the invoice line item values are used. --></help>
        <label><!-- Tax Table Columns --></label>
        <name>ONB2__TaxTableColumns__c</name>
    </fields>
    <fields>
        <help><!-- Specifies the text to be printed above the tax table. --></help>
        <label><!-- Tax Table Text --></label>
        <name>ONB2__TaxTableText__c</name>
    </fields>
    <fields>
        <help><!-- Controls whether to append a table that lists detailed tax information to the current invoice. --></help>
        <label><!-- Tax Table --></label>
        <name>ONB2__TaxTable__c</name>
    </fields>
    <fields>
        <help><!-- The introduction for the invoice is displayed above the invoice items. The following place holders can be used in the text:  
[DiscountAmount]  
[DiscountRate]  
[DiscountPaymentDueDate] 
[PaymentDueDate]  
[PaymentDue] --></help>
        <label><!-- Text 1 --></label>
        <name>ONB2__Text1__c</name>
    </fields>
    <fields>
        <help><!-- The introduction for the invoice is displayed above the invoice items. The following place holders can be used in the text:  
[DiscountAmount]  
[DiscountRate]  
[DiscountPaymentDueDate] 
[PaymentDueDate]  
[PaymentDue] --></help>
        <label><!-- Text 2 --></label>
        <name>ONB2__Text2__c</name>
    </fields>
    <fields>
        <help><!-- A third text block that can be printed on the invoice above or below the detail table. --></help>
        <label><!-- Text 3 --></label>
        <name>ONB2__Text3__c</name>
    </fields>
    <fields>
        <help><!-- The format which will be used to display time values. --></help>
        <label><!-- Time Format --></label>
        <name>ONB2__TimeFormat__c</name>
        <picklistValues>
            <masterLabel>HH:mm</masterLabel>
            <translation><!-- HH:mm --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>HH:mm:ss</masterLabel>
            <translation><!-- HH:mm:ss --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>hh:mm a</masterLabel>
            <translation><!-- hh:mm a --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>hh:mm:ss a</masterLabel>
            <translation><!-- hh:mm:ss a --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Configure transaction records to append to the invoice as Csv. --></help>
        <label><!-- Transaction Csv --></label>
        <name>ONB2__TransactionCsv__c</name>
    </fields>
    <fields>
        <help><!-- Configure transaction records to append to the invoice. --></help>
        <label><!-- Transaction Records --></label>
        <name>ONB2__TransactionRecords__c</name>
    </fields>
    <fields>
        <help><!-- Allows the display of most Unicode Characters with the Font &apos;Arial Unicode MS&apos;. Downside: there is no bold and italic text formatting possible. --></help>
        <label><!-- Use Unicode Font --></label>
        <name>ONB2__UseUnicodeFont__c</name>
    </fields>
</CustomObjectTranslation>
