<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>GDPR</fullName>
        <active>false</active>
        <formula>ISBLANK(Opportunity__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Referral Request Follow up</fullName>
        <actions>
            <name>Referral_Follow_up</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Contact.AccountName</field>
            <operation>contains</operation>
            <value>OST - Ostschweizerische Fachhochschule</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.CreatedById</field>
            <operation>contains</operation>
            <value>Jana Bernhard</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LastModifiedById</field>
            <operation>contains</operation>
            <value>Jana Bernhard</value>
        </criteriaItems>
        <description>Referral Request Follow up OST.ch - for unique import</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Referral_Follow_up</fullName>
        <assignedTo>samuel.kamm@sherpany.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Please follow up regarding getting a referral from the contact connected to this task.</description>
        <dueDateOffset>150</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Referral Follow up</subject>
    </tasks>
</Workflow>
