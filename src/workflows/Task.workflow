<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>task_due_html</fullName>
        <description>task due html</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Task_Due_HTML</template>
    </alerts>
</Workflow>
