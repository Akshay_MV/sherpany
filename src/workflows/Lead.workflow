<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Lead_Test</fullName>
        <description>Lead Test</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/OnBoarding_Done</template>
    </alerts>
    <alerts>
        <fullName>New_Inbound_inform_Marketing</fullName>
        <ccEmails>marketing@sherpany.com</ccEmails>
        <description>New Inbound inform Marketing</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Process_Builder_Emails/Marketing_Inbound</template>
    </alerts>
    <alerts>
        <fullName>New_LinkedIn_Lead_Gen_for_Yannick_Streicher_Romandie</fullName>
        <ccEmails>alexander.bachelor@sherpany.com</ccEmails>
        <description>New LinkedIn Lead Gen for Yannick Streicher/Romandie</description>
        <protected>false</protected>
        <recipients>
            <recipient>yannick.streicher@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Inbound_special_forYannick_Streicher</template>
    </alerts>
    <alerts>
        <fullName>New_LinkedIn_Lead_Gen_inform_Marketing</fullName>
        <description>New LinkedIn Lead Gen inform Marketing</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Builder_Emails/LinkedIn_Lead_Gen_Notification</template>
    </alerts>
    <alerts>
        <fullName>New_Website_Inbound</fullName>
        <description>New Website Inbound</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Builder_Emails/Website_Inbound_Notification</template>
    </alerts>
    <alerts>
        <fullName>Resumption_Date_Email_Alert</fullName>
        <description>Resumption Date Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Builder_Emails/Notification_Lead_Resumption_at</template>
    </alerts>
    <alerts>
        <fullName>Resumption_at_date_is_met</fullName>
        <description>Resumption at: date is met!</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Resumption_at_date_is_met_lead</template>
    </alerts>
    <alerts>
        <fullName>inbound_mail_after_level</fullName>
        <description>Inbound mail after Level is set by GH&apos;s</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Builder_Emails/inbound_mail_after_level</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Lead_Status</fullName>
        <field>Status</field>
        <literalValue>Qualified</literalValue>
        <name>Update Lead Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_send_NL</fullName>
        <field>Don_t_Send_NL__c</field>
        <literalValue>1</literalValue>
        <name>Update send NL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Lead_Outbound</fullName>
        <apiVersion>46.0</apiVersion>
        <description>Trigger from Lead Message</description>
        <endpointUrl>https://hooks.zapier.com/hooks/catch/5386784/o2g34mk/</endpointUrl>
        <fields>Company</fields>
        <fields>Country</fields>
        <fields>Email</fields>
        <fields>FirstName</fields>
        <fields>Full_Name__c</fields>
        <fields>Id</fields>
        <fields>Language__c</fields>
        <fields>LastName</fields>
        <fields>Phone</fields>
        <fields>SDR_could_not_reach_EMAIL_WILL_BE_SENT__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>luca.haag@sherpany.com</integrationUser>
        <name>Lead Outbound</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Demo Request</fullName>
        <actions>
            <name>Update_send_NL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Campagne (Inbound)</value>
        </criteriaItems>
        <description>If a new inbound comes through the landing, the newsletter should not be sent</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Resumption At send Notification</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Resumption_at__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Lead.Resumption_at__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Lead Status MQL</fullName>
        <actions>
            <name>Update_Lead_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.lead_level__c</field>
            <operation>equals</operation>
            <value>MQL</value>
        </criteriaItems>
        <description>This flow updates the field &quot;lead status&quot; to MQL when the &quot;lead level&quot; is MQL</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead field %22DR_could_not_reach_EMAIL_WILL_BE_SENT%5F%5Fc%22</fullName>
        <actions>
            <name>Lead_Outbound</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Workflow used to trigger the outbound message in Zapier.</description>
        <formula>SDR_could_not_reach_EMAIL_WILL_BE_SENT__c  = TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
