<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Final_Handover_Call</fullName>
        <description>Final Handover Call</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>marcel.ulrich@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Final_Handover_Call</template>
    </alerts>
    <outboundMessages>
        <fullName>WootricSurveyCSATservice</fullName>
        <apiVersion>48.0</apiVersion>
        <description>Triggers CSAT Service survey in wootric</description>
        <endpointUrl>https://app.wootric.com/api/v1/triggers/salesforce/169a6dd2c36b05f545ee13ea60b28b4fd5297f59/email_survey</endpointUrl>
        <fields>Additional_contact_II__c</fields>
        <fields>Additional_contact__c</fields>
        <fields>Contact__c</fields>
        <fields>CopyMail__c</fields>
        <fields>ISO_639_1_Language_Code_copiedfromOpp__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>alexander.bachelor@sherpany.com</integrationUser>
        <name>WootricSurveyCSATservice</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>SendWootricService</fullName>
        <actions>
            <name>WootricSurveyCSATservice</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>E Closed Won</value>
        </criteriaItems>
        <description>Send wootric when customer intercation is saved and opp stage is E Closed Won</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
