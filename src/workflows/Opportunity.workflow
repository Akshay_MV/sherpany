<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_Close_Date_Changed</fullName>
        <ccEmails>jana.bernhard@sherpany.com</ccEmails>
        <description>Alert: Close Date Changed</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Alert_Close_Date_Changed</template>
    </alerts>
    <alerts>
        <fullName>CSM_Attention_index</fullName>
        <ccEmails>janek.dekock@sherpany.com</ccEmails>
        <ccEmails>csm@sherpany.com</ccEmails>
        <description>CSM: Attention index</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Process_Builder_Emails/CSM_Attention_Index2</template>
    </alerts>
    <alerts>
        <fullName>CSM_confirmed_Accounting</fullName>
        <description>CSM confirmed Accounting</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>alisha.buck@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nadine.sauter@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CSM_determined_AccountingPlus</template>
    </alerts>
    <alerts>
        <fullName>CSM_confirmed_CSM</fullName>
        <description>CSM confirmed CSM</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>CSM_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CSM_determined_CSM</template>
    </alerts>
    <alerts>
        <fullName>CSM_confirmed_Sales</fullName>
        <description>CSM confirmed Sales</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>andrea.ratzenberger@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CSM_determined_Sales</template>
    </alerts>
    <alerts>
        <fullName>Closed_Won_CSM</fullName>
        <description>Closed Won CSM</description>
        <protected>false</protected>
        <recipients>
            <recipient>CSM</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>andrea.ratzenberger@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Closed_Won_CSM</template>
    </alerts>
    <alerts>
        <fullName>Closed_Won_Sales</fullName>
        <description>Closed Won Sales</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Responsible_Sales__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Closed_Won_Sales</template>
    </alerts>
    <alerts>
        <fullName>Contract_scanned_CSM</fullName>
        <description>Contract scanned CSM</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>CSM_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_scanned_CSMhtml</template>
    </alerts>
    <alerts>
        <fullName>Contract_scanned_Sales</fullName>
        <description>Contract scanned Sales</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Responsible_Sales__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Text_Contract_scanned_Sales</template>
    </alerts>
    <alerts>
        <fullName>Contract_scanned_SalesPlus</fullName>
        <description>Contract scanned Sales+</description>
        <protected>false</protected>
        <recipients>
            <field>Responsible_Sales__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_scanned_CSMhtml</template>
    </alerts>
    <alerts>
        <fullName>Contract_scanned_Sales_html</fullName>
        <description>Contract scanned Sales html</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Responsible_Sales__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_scanned_Saleshtml</template>
    </alerts>
    <alerts>
        <fullName>Contract_signed_and_uploaded_Accounting</fullName>
        <description>Contract signed and uploaded Accounting</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kevin.ratheiser@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Contract_signed_and_uploaded_Accounting</template>
    </alerts>
    <alerts>
        <fullName>Mail_Alert_Request_Handover_CSM</fullName>
        <ccEmails>csm@sherpany.com</ccEmails>
        <description>Mail Alert Request Handover CSM</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Request_Handover_CSM</template>
    </alerts>
    <alerts>
        <fullName>Mail_Alert_request_handover_Sales</fullName>
        <description>Mail Alert request handover Sales</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>andrea.ratzenberger@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Request_Handover_Sales</template>
    </alerts>
    <alerts>
        <fullName>Mail_sent_when_terminated</fullName>
        <description>Mail sent when terminated</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>alisha.buck@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Mail_to_accounting_Terminated</template>
    </alerts>
    <alerts>
        <fullName>Mail_to_CSM_In_termination</fullName>
        <description>Mail_to_CSM_In_termination</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Mail_to_CSM_In_termination</template>
    </alerts>
    <alerts>
        <fullName>OnBoarding_Done</fullName>
        <description>OnBoarding Done</description>
        <protected>false</protected>
        <recipients>
            <recipient>kevin.ratheiser@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/OnBoarding_Done</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Revival</fullName>
        <description>Opportunity Revival</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Builder_Emails/Opportunity_revival</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Terminated</fullName>
        <description>Opportunity Terminated</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexander.bachelor@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Builder_Emails/Opportunity_Terminated</template>
    </alerts>
    <alerts>
        <fullName>Resumption_at_date_is_met</fullName>
        <description>Resumption at: date is met!</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Resumption_at_date_is_met</template>
    </alerts>
    <alerts>
        <fullName>Send_mail_no_Region_found</fullName>
        <description>Send mail no Region found</description>
        <protected>false</protected>
        <recipients>
            <recipient>antje.lausch@sherpany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Builder_Emails/NoRegionFound</template>
    </alerts>
    <alerts>
        <fullName>Tit_for_Tat_CSM</fullName>
        <description>Tit for Tat CSM</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Tit_for_Tat_CSM</template>
    </alerts>
    <alerts>
        <fullName>Tit_for_Tat_Marketing</fullName>
        <description>Tit for Tat Marketing</description>
        <protected>false</protected>
        <recipients>
            <field>Marketing_Team_Mail_Address__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Tit_for_Tat_Marketing</template>
    </alerts>
    <fieldUpdates>
        <fullName>Closed_Duration_Population</fullName>
        <field>Closed_Duration__c</field>
        <formula>TODAY() - Datevalue(CreatedDate)</formula>
        <name>Closed Duration Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Message_to_Slack_Closed_Won</fullName>
        <apiVersion>48.0</apiVersion>
        <endpointUrl>https://hooks.zapier.com/hooks/catch/5386784/omlimdf/</endpointUrl>
        <fields>CSM_Owner__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>Number_of_Users__c</fields>
        <fields>OwnerId</fields>
        <fields>Responsible_Sales__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>jana.bernhard@sherpany.com</integrationUser>
        <name>Message to Slack - Closed Won</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>WootricSurveyCSATSales</fullName>
        <apiVersion>48.0</apiVersion>
        <description>Triggers CSAT Sales survey in wootric</description>
        <endpointUrl>https://app.wootric.com/api/v1/triggers/salesforce/99fc4bae527f8dd7a6982656a04cb889ed86af75/email_survey</endpointUrl>
        <fields>Contact_for_Survey_Feedback__c</fields>
        <fields>ISO_639_1_Language_Code__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>alexander.bachelor@sherpany.com</integrationUser>
        <name>WootricSurveyCSATSales</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Check up on Critical Customer</fullName>
        <actions>
            <name>Check_Freshdesk_Tickets2</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Check_H_cki_Sheet_for_Critical_Data2</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>E Closed Won,Closed Won (Pro Bono)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Product__c</field>
            <operation>equals</operation>
            <value>Boardroom</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Critical__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Check up on Customer when they become critical</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Check up on Customer 3 months before Renewal period starts</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>E Closed Won,Closed Won (Pro Bono)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Product__c</field>
            <operation>equals</operation>
            <value>Boardroom</value>
        </criteriaItems>
        <description>Check up on Customer 3 months before Renewal period starts</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Freshdesk_Tickets</name>
                <type>Task</type>
            </actions>
            <actions>
                <name>Check_H_cki_Sheet_for_Critical_Data</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Opportunity.Subscription_Renewal_Date__c</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Close Date Populate</fullName>
        <actions>
            <name>Closed_Duration_Population</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(StageName),ISPICKVAL(StageName,&apos;Closed Dead&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Feedback Call</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>E Closed Won,Closed Won (Pro Bono)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Onboarding_done__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Automatically creates a task for the CSM to hold the feedback call with a customer 3 months after onboarding is complete.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Feedback_Call</name>
                <type>Task</type>
            </actions>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Resumption At send Notification</fullName>
        <actions>
            <name>Resumption_at_date_is_met</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Resumption_at__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Opportunity.Resumption_at__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SendWootricSales</fullName>
        <actions>
            <name>WootricSurveyCSATSales</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Send wootric when stage change to Closed Won OR Closed Dead</description>
        <formula>AND(
ISPICKVAL(PRIORVALUE(StageName),&apos;B Discovery&apos;)  ||  
ISPICKVAL(PRIORVALUE(StageName),&apos;C POV&apos;)  || 
ISPICKVAL(PRIORVALUE(StageName),&apos;D Closing&apos;) ||
ISPICKVAL(PRIORVALUE(StageName),&apos;Contract registered in SF&apos;)
,
AND(
  ISPICKVAL(StageName, &quot;Closed Dead&quot;) || ISPICKVAL(StageName, &quot;E Closed Won&quot;)
  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Slack Message Closed Won</fullName>
        <actions>
            <name>Message_to_Slack_Closed_Won</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>E Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CSM_Owner__c</field>
            <operation>contains</operation>
            <value>Menzi,Kamm,Ventura,Piwek,Bachelor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Responsible_Sales__c</field>
            <operation>notContain</operation>
            <value>Menzi,Kamm,Ventura,Piwek</value>
        </criteriaItems>
        <description>Sends message to Sherpany Slack Channel when Opp is Closed Won</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Check_Freshdesk_Tickets</fullName>
        <assignedToType>owner</assignedToType>
        <description>This Opportunity&apos;s Renewal period is coming up in 3 months. Please check the support tickets logged in Freshdesk to check on your customer&apos;s satisfaction levels and pain points.</description>
        <dueDateOffset>-80</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Subscription_Renewal_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Check Freshdesk Tickets</subject>
    </tasks>
    <tasks>
        <fullName>Check_Freshdesk_Tickets2</fullName>
        <assignedToType>owner</assignedToType>
        <description>This Opportunity has become critical - please check out its Freshdesk tickets to gain a better understanding on how to support this customer more effectively.</description>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Critical: Check Freshdesk Tickets</subject>
    </tasks>
    <tasks>
        <fullName>Check_H_cki_Sheet_for_Critical_Data</fullName>
        <assignedToType>owner</assignedToType>
        <description>This Opportunity&apos;s Subscription renewal period is coming up in 3 months. Please check if the rooms associated with this opp. have any critical data entries on the Häcki Sheet.

https://docs.google.com/spreadsheets/d/16hHPvYA9zQJ_3Pn5ZI9bwQL3lKDQ6dU</description>
        <dueDateOffset>-80</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Subscription_Renewal_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Check &quot;Häcki Sheet&quot; for Critical Data</subject>
    </tasks>
    <tasks>
        <fullName>Check_H_cki_Sheet_for_Critical_Data2</fullName>
        <assignedToType>owner</assignedToType>
        <description>This Opportunity has become critical - check out the Critical Data section on the Häcki Sheet.

https://docs.google.com/spreadsheets/d/16hHPvYA9zQJ_3Pn5ZI9bwQL3lKDQ6dUsP4NsVAgdKS0/edit#gid=1168513045

Talk to your data analyst in case of questions.</description>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Critical: Check &quot;Häcki Sheet&quot; for Critical Data</subject>
    </tasks>
    <tasks>
        <fullName>Feedback_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>You marked this customer&apos;s onboarding as complete 3 months ago - please schedule and hold a feedback call with this customer.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Feedback Call</subject>
    </tasks>
</Workflow>
