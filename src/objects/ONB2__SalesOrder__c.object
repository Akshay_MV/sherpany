<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>ONB2__Account__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The account that is associated with the sales order.</description>
        <externalId>false</externalId>
        <inlineHelpText>The account that is associated with the sales order.</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Sales Orders</relationshipLabel>
        <relationshipName>SalesOrders</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ONB2__BillingCity__c</fullName>
        <deprecated>false</deprecated>
        <description>The copied Billing City. Will be used for the Recipient Address field.</description>
        <externalId>false</externalId>
        <inlineHelpText>The copied Billing City. Will be used for the Recipient Address field.</inlineHelpText>
        <label>Billing City</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__BillingCountry__c</fullName>
        <deprecated>false</deprecated>
        <description>The copied Billing Country. Will be used for the Recipient Address field.</description>
        <externalId>false</externalId>
        <inlineHelpText>The copied Billing Country. Will be used for the Recipient Address field.</inlineHelpText>
        <label>Billing Country</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__BillingPostalCode__c</fullName>
        <deprecated>false</deprecated>
        <description>The copied Billing Postal Code. Will be used for the Recipient Address field.</description>
        <externalId>false</externalId>
        <inlineHelpText>The copied Billing Postal Code. Will be used for the Recipient Address field.</inlineHelpText>
        <label>Billing Postal Code</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__BillingState__c</fullName>
        <deprecated>false</deprecated>
        <description>The copied Billing State. Will be used for the Recipient Address field.</description>
        <externalId>false</externalId>
        <inlineHelpText>The copied Billing State. Will be used for the Recipient Address field.</inlineHelpText>
        <label>Billing State</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__BillingStreet__c</fullName>
        <deprecated>false</deprecated>
        <description>The copied Billing Street. Will be used for the Recipient Address field.</description>
        <externalId>false</externalId>
        <inlineHelpText>The copied Billing Street. Will be used for the Recipient Address field.</inlineHelpText>
        <label>Billing Street</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The contact that is associated with the sales order.</description>
        <externalId>false</externalId>
        <inlineHelpText>The contact that is associated with the sales order.</inlineHelpText>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Sales Orders</relationshipLabel>
        <relationshipName>SalesOrders</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ONB2__ShippingCity__c</fullName>
        <deprecated>false</deprecated>
        <description>The city of the shipping address of the related account.</description>
        <externalId>false</externalId>
        <inlineHelpText>The city of the shipping address of the related account.</inlineHelpText>
        <label>Shipping City</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__ShippingCountry__c</fullName>
        <deprecated>false</deprecated>
        <description>The country of the shipping address of the related account.</description>
        <externalId>false</externalId>
        <inlineHelpText>The country of the shipping address of the related account.</inlineHelpText>
        <label>Shipping Country</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__ShippingPostalCode__c</fullName>
        <deprecated>false</deprecated>
        <description>The postal code of the shipping address of the related account.</description>
        <externalId>false</externalId>
        <inlineHelpText>The postal code of the shipping address of the related account.</inlineHelpText>
        <label>Shipping Postal Code</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__ShippingState__c</fullName>
        <deprecated>false</deprecated>
        <description>The state of the shipping address of the related account.</description>
        <externalId>false</externalId>
        <inlineHelpText>The state of the shipping address of the related account.</inlineHelpText>
        <label>Shipping State</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__ShippingStreet__c</fullName>
        <deprecated>false</deprecated>
        <description>The street of the shipping address of the related account.</description>
        <externalId>false</externalId>
        <inlineHelpText>The street of the shipping address of the related account.</inlineHelpText>
        <label>Shipping Street</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Sales Order</label>
    <listViews>
        <fullName>ONB2__All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>{00000}</displayFormat>
        <label>Sales Order No.</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Sales Orders</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>ONB2__DeliverAndInvoice</fullName>
        <availability>online</availability>
        <description>Set the status to &apos;invoiced&apos; and create an invoice directly from the order in status &apos;draft&apos;. Such orders will not be considered by the job that creates transactions for the automatic billing.</description>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Deliver and Invoice</masterLabel>
        <openType>sidebar</openType>
        <page>ONB2__InvoiceNewFromSalesOrder</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
