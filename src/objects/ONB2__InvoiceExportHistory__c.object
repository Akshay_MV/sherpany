<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>ONB2__Highlight</compactLayoutAssignment>
    <compactLayouts>
        <fullName>ONB2__Highlight</fullName>
        <fields>Name</fields>
        <fields>ONB2__Invoice__c</fields>
        <fields>ONB2__ExportSetting__c</fields>
        <fields>ONB2__ExportedAt__c</fields>
        <fields>ONB2__FileUrl__c</fields>
        <label>Highlight</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>ONB2__BalanceCreated__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This is checked, when a balance has been created during export.</description>
        <externalId>false</externalId>
        <inlineHelpText>This is checked, when a balance has been created during export.</inlineHelpText>
        <label>Balance Created</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ONB2__ExportFormat__c</fullName>
        <deprecated>false</deprecated>
        <description>The export format as defined at the export setting.</description>
        <externalId>true</externalId>
        <inlineHelpText>The export format as defined at the export setting.</inlineHelpText>
        <label>Export Format</label>
        <length>255</length>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__ExportSetting__c</fullName>
        <deprecated>false</deprecated>
        <description>The name of the export setting.</description>
        <externalId>true</externalId>
        <inlineHelpText>The name of the export setting.</inlineHelpText>
        <label>Export Setting</label>
        <length>255</length>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__ExportedAt__c</fullName>
        <deprecated>false</deprecated>
        <description>The export timestamp.</description>
        <externalId>false</externalId>
        <inlineHelpText>The export timestamp.</inlineHelpText>
        <label>Exported at</label>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>ONB2__FileName__c</fullName>
        <deprecated>false</deprecated>
        <description>The name of the export file.</description>
        <externalId>true</externalId>
        <inlineHelpText>The name of the export file.</inlineHelpText>
        <label>File Name</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__FileUrl__c</fullName>
        <deprecated>false</deprecated>
        <description>The link to the export file.</description>
        <externalId>false</externalId>
        <inlineHelpText>The link to the export file.</inlineHelpText>
        <label>File URL</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>ONB2__Invoice__c</fullName>
        <deprecated>false</deprecated>
        <description>The related invoice for this export.</description>
        <externalId>false</externalId>
        <inlineHelpText>The related invoice for this export.</inlineHelpText>
        <label>Invoice</label>
        <referenceTo>ONB2__Invoice__c</referenceTo>
        <relationshipLabel>Invoice Export History</relationshipLabel>
        <relationshipName>InvoiceExportHistories</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ONB2__Locked__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>A record is marked as locked, if the export setting allows only one export (mark as exported).</description>
        <externalId>false</externalId>
        <inlineHelpText>A record is marked as locked, if the export setting allows only one export (mark as exported).</inlineHelpText>
        <label>Locked</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ONB2__NextPaymentDueDateChanged__c</fullName>
        <deprecated>false</deprecated>
        <description>This is true for linked installment invoices where the next payment due date has advanced and the next installment may be due.</description>
        <externalId>false</externalId>
        <formula>AND(
ISPICKVAL(ONB2__Invoice__r.ONB2__Type__c, &apos;Installment&apos;),
NOT(ISBLANK(ONB2__NextPaymentDueDate__c)),
NOT(ISBLANK(ONB2__Invoice__r.ONB2__NextPaymentDueDate__c)),
ONB2__NextPaymentDueDate__c &lt; ONB2__Invoice__r.ONB2__NextPaymentDueDate__c
)</formula>
        <inlineHelpText>This is true for linked installment invoices where the next payment due date has advanced and the next installment may be due.</inlineHelpText>
        <label>Next Payment Due Date Changed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ONB2__NextPaymentDueDate__c</fullName>
        <deprecated>false</deprecated>
        <description>The effective payment due date of the invoice during the export.</description>
        <externalId>false</externalId>
        <inlineHelpText>The effective payment due date of the invoice during the export.</inlineHelpText>
        <label>Next Payment Due Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Invoice Export History</label>
    <nameField>
        <displayFormat>{YYYY}{MM}{DD}-{00000000}</displayFormat>
        <label>#</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Invoice Export Histories</pluralLabel>
    <searchLayouts>
        <excludedStandardButtons>New</excludedStandardButtons>
        <excludedStandardButtons>OpenListInQuip</excludedStandardButtons>
        <excludedStandardButtons>MassChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>Accept</excludedStandardButtons>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <visibility>Public</visibility>
</CustomObject>
