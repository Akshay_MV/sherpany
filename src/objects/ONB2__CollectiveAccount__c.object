<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>List</customSettingsType>
    <description>Define collective accounts for booking data (e.g. deferred revenue, tax, payments)</description>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>ONB2__Account__c</fullName>
        <deprecated>false</deprecated>
        <description>The number of the collective account in the accounting system.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of the collective account in the accounting system.</inlineHelpText>
        <label>Account</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__BankAccountId__c</fullName>
        <deprecated>false</deprecated>
        <description>The bank account id of the target booking detail. Multiple values as comma separated list. (max 255 characters)</description>
        <externalId>false</externalId>
        <inlineHelpText>The bank account id of the target booking detail. Multiple values as comma separated list. (max 255 characters)</inlineHelpText>
        <label>Bank Account Id</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__BillingPractice__c</fullName>
        <deprecated>false</deprecated>
        <description>The billing practice of the target booking detail (e.g. Advance, Arrears). Multiple values as comma separated list. (max 255 characters)</description>
        <externalId>false</externalId>
        <inlineHelpText>The billing practice of the target booking detail (e.g. Advance, Arrears). Multiple values as comma separated list. (max 255 characters)</inlineHelpText>
        <label>Billing Practice</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__BpAccount__c</fullName>
        <deprecated>false</deprecated>
        <description>The number of the business partner account in the accounting system.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of the business partner account in the accounting system.</inlineHelpText>
        <label>Business Partner Account</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__PaymentProvider__c</fullName>
        <deprecated>false</deprecated>
        <description>The payment provider of the target booking detail (e.g. figo). Multiple values as comma separated list. (max 255 characters)</description>
        <externalId>false</externalId>
        <inlineHelpText>The payment provider of the target booking detail (e.g. figo). Multiple values as comma separated list. (max 255 characters)</inlineHelpText>
        <label>Payment Provider</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__Region__c</fullName>
        <deprecated>false</deprecated>
        <description>The region of the target booking detail (e.g. EU, EMEA). Multiple values as comma separated list. (max 255 characters)</description>
        <externalId>false</externalId>
        <inlineHelpText>The region of the target booking detail (e.g. EU, EMEA). Multiple values as comma separated list. (max 255 characters)</inlineHelpText>
        <label>Region</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__TaxCode__c</fullName>
        <deprecated>false</deprecated>
        <description>The name of the applied tax code (type Tax only). Multiple values as comma separated list. (max 255 characters)</description>
        <externalId>false</externalId>
        <inlineHelpText>The name of the applied tax code (type Tax only).</inlineHelpText>
        <label>Tax Code</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__TaxRule__c</fullName>
        <deprecated>false</deprecated>
        <description>The name of the applied tax rule (type Tax only). Multiple values as comma separated list. (max 255 characters)</description>
        <externalId>false</externalId>
        <inlineHelpText>The name of the applied tax rule (type Tax only).</inlineHelpText>
        <label>Tax Rule</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__Tenant__c</fullName>
        <deprecated>false</deprecated>
        <description>The name of the business entity. Multiple values as comma separated list. (max 255 characters).</description>
        <externalId>false</externalId>
        <inlineHelpText>The name of the tenant. Multiple values as comma separated list. (max 255 characters).</inlineHelpText>
        <label>Business Entity</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__Type__c</fullName>
        <deprecated>false</deprecated>
        <description>The type of the target booking detail (e.g. Revenue, Tax, Deferred, Payment). Multiple values as comma separated list. (max 255 characters)</description>
        <externalId>false</externalId>
        <inlineHelpText>The type of the target booking detail (e.g. Revenue, Tax, Deferred, Payment).</inlineHelpText>
        <label>Type</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Collective Account</label>
    <visibility>Public</visibility>
</CustomObject>
