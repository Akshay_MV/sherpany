<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>List</customSettingsType>
    <description>Tax Rules to provide tax rate lookups for combinations of sender country, account country, account tax class and product tax class.</description>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>ONB2__AccountCountry__c</fullName>
        <deprecated>false</deprecated>
        <description>Corresponds to either ShippingCountry or BillingCountry of an Account in this order</description>
        <externalId>false</externalId>
        <inlineHelpText>Corresponds to either ShippingCountry or BillingCountry of an Account in this order</inlineHelpText>
        <label>Invoice Country</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__AccountRegion__c</fullName>
        <deprecated>false</deprecated>
        <description>Name of the Region, e.g. &apos;EU&apos; or &apos;Non-EU&apos;</description>
        <externalId>false</externalId>
        <inlineHelpText>Name of the Region, e.g. &apos;EU&apos; or &apos;Non-EU&apos;</inlineHelpText>
        <label>Invoice Region</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__AccountState__c</fullName>
        <deprecated>false</deprecated>
        <description>Name of the state</description>
        <externalId>false</externalId>
        <inlineHelpText>Name of the state</inlineHelpText>
        <label>Invoice State</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__AccountTaxClass__c</fullName>
        <deprecated>false</deprecated>
        <description>Corresponds to the field AccountTaxClass__c on the invoice that has to be set via Account.ON_AccountTaxClass.</description>
        <externalId>false</externalId>
        <inlineHelpText>Corresponds to the custom field TaxClass__c of an Account, e.g retail, businessSame values as Account.TaxClass__c</inlineHelpText>
        <label>Account Tax Class</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__ProductGroup__c</fullName>
        <deprecated>false</deprecated>
        <description>Name of the matched Invoice Line Item Product Group. Multiple values as comma separated list. (max 255 characters)</description>
        <externalId>false</externalId>
        <inlineHelpText>Name of the matched Invoice Line Item Product Group. Multiple values as comma separated list. (max 255 characters)</inlineHelpText>
        <label>Product Group</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__ProductTaxClass__c</fullName>
        <deprecated>false</deprecated>
        <description>Corresponds to the custom field TaxClass__c of a Product2, e.g. e.g. (tax)free / reduced / full / luxury</description>
        <externalId>false</externalId>
        <inlineHelpText>Corresponds to the custom field TaxClass__c of a Product2, e.g. e.g. (tax)free / reduced / full / luxury</inlineHelpText>
        <label>Product Tax Class</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__TaxCode__c</fullName>
        <deprecated>false</deprecated>
        <description>The tax code for this rule</description>
        <externalId>false</externalId>
        <inlineHelpText>The tax code for this rule</inlineHelpText>
        <label>Tax Code</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__TaxRate__c</fullName>
        <deprecated>false</deprecated>
        <description>The tax rate for this rule</description>
        <externalId>false</externalId>
        <inlineHelpText>The tax rate for this rule</inlineHelpText>
        <label>Tax Rate</label>
        <precision>18</precision>
        <required>true</required>
        <scale>3</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__Tenant__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Business Entity</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__Type__c</fullName>
        <deprecated>false</deprecated>
        <description>Tax rules can be partitioned by type, to support multiple tax rates (e.g. sales tax) per invoice line item. Tax rules with the same type are evaluated together and yield one tax rate.</description>
        <externalId>false</externalId>
        <inlineHelpText>Tax rules can be partitioned by type, to support multiple tax rates (e.g. sales tax) per invoice line item. Tax rules with the same type are evaluated together and yield one tax rate.</inlineHelpText>
        <label>Type</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Tax Rules</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>ONB2__Tenant__c</columns>
        <columns>ONB2__AccountCountry__c</columns>
        <columns>ONB2__AccountTaxClass__c</columns>
        <columns>ONB2__ProductTaxClass__c</columns>
        <columns>ONB2__TaxRate__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <visibility>Public</visibility>
</CustomObject>
