<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>List</customSettingsType>
    <description>Holds the configurations for different types of archive jobs.</description>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>ONB2__Condition__c</fullName>
        <deprecated>false</deprecated>
        <description>You can set conditions to archive objects more specific.</description>
        <externalId>false</externalId>
        <inlineHelpText>You can set conditions to archive objects more specific.</inlineHelpText>
        <label>Condition</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>ONB2__DateField__c</fullName>
        <deprecated>false</deprecated>
        <description>Archiving objects needs to be done in pieces. This field shows the system which field is the basis to get objects for a day, month, or specific time frame.</description>
        <externalId>false</externalId>
        <inlineHelpText>Archiving objects needs to be done in pieces. This field shows the system which field is the basis to get objects for a day, month, or specific time frame.</inlineHelpText>
        <label>Date Field</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__DeleteAfterArchive__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If set to true, all archived objects will be removed from your org.</description>
        <externalId>false</externalId>
        <inlineHelpText>If set to true, all archived objects will be removed from your org.</inlineHelpText>
        <label>Delete After Archive</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ONB2__SyncType__c</fullName>
        <deprecated>false</deprecated>
        <description>There are two possible values &apos;store&apos; and &apos;restore&apos;. Store archives objects into the object storage. Restore pulls the archived objects from the object storage and recreates them in the org.</description>
        <externalId>false</externalId>
        <inlineHelpText>There are two possible values &apos;store&apos; and &apos;restore&apos;. Store archives objects into the object storage. Restore pulls the archived objects from the object storage and recreates them in the org.</inlineHelpText>
        <label>Sync Type</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__Target__c</fullName>
        <deprecated>false</deprecated>
        <description>Class of objects to archive.</description>
        <externalId>false</externalId>
        <inlineHelpText>Class of objects to archive.</inlineHelpText>
        <label>Target</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Object Storage Configuration</label>
    <visibility>Public</visibility>
</CustomObject>
