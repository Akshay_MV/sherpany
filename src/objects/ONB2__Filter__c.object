<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>List</customSettingsType>
    <description>Filters containing SOQL WHERE clauses that allow the customization of certain processes in bill.ON e.g. the Invoice Run.</description>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>ONB2__ChildRelation__c</fullName>
        <deprecated>false</deprecated>
        <description>The name of child object. Used to query child records (subscription builder only).</description>
        <externalId>false</externalId>
        <inlineHelpText>The name of child relation, in oder to query child records. (Subscription Builder only)</inlineHelpText>
        <label>Child Relation</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__Condition__c</fullName>
        <deprecated>false</deprecated>
        <description>The condition describing the logic of this filter in the format of a SOQL WHERE clause, e.g. &quot;StartDate__c &lt;= 2013-12-11&quot;. If left empty, matches all records of the specified type.</description>
        <externalId>false</externalId>
        <inlineHelpText>The condition describing the logic of this filter in the format of a SOQL WHERE clause, e.g. &quot;StartDate__c &lt;= 2013-12-11&quot;. If left empty, matches all records of the specified type.</inlineHelpText>
        <label>Condition</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>ONB2__FilterGroup__c</fullName>
        <deprecated>false</deprecated>
        <description>Allows to restrict the filter selection by a common group. A common way to group filters is the use of the business entity. This option is UI only.</description>
        <externalId>false</externalId>
        <inlineHelpText>Allows to restrict the filter selection by a common group. A common way to group filters is the use of the tenant. This option is UI only.</inlineHelpText>
        <label>Filter Group</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__ParentRelationships__c</fullName>
        <deprecated>false</deprecated>
        <description>Describes parent relationships for target as JSON. For example: {&quot;AccountId&quot;: &quot;Account&quot;}. Each key is the API field name on the target SObject. The value of a key ist the SObject type.</description>
        <externalId>false</externalId>
        <inlineHelpText>Describes parent relationships for target as JSON. For example: {&quot;AccountId&quot;: &quot;Account&quot;}. Each key is the API field name on the target SObject. The value of a key ist the SObject type.</inlineHelpText>
        <label>Parent Relationships</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__Sequence__c</fullName>
        <deprecated>false</deprecated>
        <description>The position of this filter in Filter picklists.</description>
        <externalId>false</externalId>
        <inlineHelpText>The position of this filter in Filter picklists.</inlineHelpText>
        <label>Sequence</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__SubchildRelation__c</fullName>
        <deprecated>false</deprecated>
        <description>The name of the subchild object. Used to query subchild records for price tier information (subscription builder only).</description>
        <externalId>false</externalId>
        <inlineHelpText>The name of the subchild object. Used to query subchild records for price tier information (subscription builder only).</inlineHelpText>
        <label>Subchild Relation</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__Target__c</fullName>
        <deprecated>false</deprecated>
        <description>The API name of the sObject that is targeted by this filter.</description>
        <externalId>false</externalId>
        <inlineHelpText>The API name of the sObject that is targeted by this filter.</inlineHelpText>
        <label>Target</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__TransactionalSuffix__c</fullName>
        <deprecated>false</deprecated>
        <description>The suffix for control fields used when converting records multiple times for different invoice recipients with the transaction builder (use case: Continuous) or the generic invoice run (use case: Generic).</description>
        <externalId>false</externalId>
        <inlineHelpText>This is used to convert records multiple times for the transaction builder/continuous invoice run. Control fields must end with this suffix for this filter.</inlineHelpText>
        <label>Transactional Suffix</label>
        <length>5</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__UseCase__c</fullName>
        <deprecated>false</deprecated>
        <description>The filter use case: &quot;SubscriptionBuilder&quot;, &quot;New&quot;, &quot;Reorder&quot;, &quot;Upgrade&quot; (subscription builder), &quot;Continuous&quot;/&quot;Continuous Daily&quot; (transaction builder), &quot;Generic&quot; (generic invoice run), &quot;AccountStatement&quot; (account statement run), empty (regular invoice run)</description>
        <externalId>false</externalId>
        <inlineHelpText>To use this filter for the Subscription builder: valid values are &quot;New&quot;, &quot;Reorder&quot;, &quot;Upgrade&quot;. To use this filter for Transaction building: valid values are &quot;Transaction&quot;, &quot;Continuous&quot;.</inlineHelpText>
        <label>Use Case</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Filters</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>ONB2__UseCase__c</columns>
        <columns>ONB2__Condition__c</columns>
        <columns>ONB2__Target__c</columns>
        <columns>ONB2__Sequence__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <visibility>Public</visibility>
</CustomObject>
