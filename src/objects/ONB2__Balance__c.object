<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>ONB2__Highlight</compactLayoutAssignment>
    <compactLayouts>
        <fullName>ONB2__Highlight</fullName>
        <fields>Name</fields>
        <fields>ONB2__Account__c</fields>
        <fields>ONB2__Date__c</fields>
        <fields>ONB2__Amount__c</fields>
        <fields>ONB2__PaymentMethod__c</fields>
        <fields>ONB2__Type__c</fields>
        <fields>ONB2__Invoice__c</fields>
        <label>Highlight</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>The balance object is used to track invoices, credits, payments, refunds etc. for invoices and for accounts. The invoice balance holds information about the grand total amount of a particular invoice, whereas the account balance sums up all balances of an account.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fieldSets>
        <fullName>Currency</fullName>
        <description>Balance</description>
        <displayedFields>
            <field>ONB2__Date__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Currency</label>
    </fieldSets>
    <fields>
        <fullName>ONB2__Account__c</fullName>
        <deprecated>false</deprecated>
        <description>Specifies the account to which this balance is associated.</description>
        <externalId>false</externalId>
        <inlineHelpText>The account which received or provided this balance.</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Balances</relationshipLabel>
        <relationshipName>Balances</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ONB2__Amount__c</fullName>
        <deprecated>false</deprecated>
        <description>Shows the balance sum. A positive amount is considered a debt, and a negative amount is considered a credit.</description>
        <externalId>false</externalId>
        <inlineHelpText>Positive amounts are considered a debt. Negative amounts are considered a credit.</inlineHelpText>
        <label>Amount</label>
        <precision>18</precision>
        <required>true</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ONB2__BankAccountId__c</fullName>
        <deprecated>false</deprecated>
        <description>Shows the internal bank account id of the payment provider.</description>
        <externalId>false</externalId>
        <inlineHelpText>Shows the internal bank account id of the payment provider.</inlineHelpText>
        <label>Bank Account Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__ConversionRate__c</fullName>
        <deprecated>false</deprecated>
        <description>The currency conversion rate for the original amount of the payment entry.</description>
        <externalId>false</externalId>
        <inlineHelpText>The currency conversion rate for the original amount of the payment entry.</inlineHelpText>
        <label>Conversion Rate</label>
        <precision>18</precision>
        <required>false</required>
        <scale>6</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__Date__c</fullName>
        <defaultValue>Today()</defaultValue>
        <deprecated>false</deprecated>
        <description>Shows the effective date of the balance.</description>
        <externalId>false</externalId>
        <inlineHelpText>The effective date of the balance.</inlineHelpText>
        <label>Date</label>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ONB2__Draft__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Draft balances are ignored in all business processes. Use the &quot;Fix Balances&quot; API to register them. This field should always be false and only set during a process.</description>
        <externalId>false</externalId>
        <inlineHelpText>Draft balances are ignored in all business processes. Use the &quot;Fix Balances&quot; API to register them. This field should always be false and only set during a process.</inlineHelpText>
        <label>Draft</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ONB2__Dunning__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Shows the related dunning in case of a dunning fee balance.</description>
        <externalId>false</externalId>
        <inlineHelpText>The related dunning in case of a dunning fee balance.</inlineHelpText>
        <label>Dunning</label>
        <referenceTo>ONB2__Dunning__c</referenceTo>
        <relationshipLabel>Balances</relationshipLabel>
        <relationshipName>Balances</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ONB2__InvoiceIsFinalized__c</fullName>
        <deprecated>false</deprecated>
        <description>Signals wheter the linked invoice has been finalized.</description>
        <externalId>false</externalId>
        <formula>IF(
ISBLANK(ONB2__Invoice__c),
false,

IF(
OR(
ISPICKVAL(ONB2__Invoice__r.ONB2__Status__c, &apos;Open&apos;),
ISPICKVAL(ONB2__Invoice__r.ONB2__Status__c, &apos;Paid&apos;),
ISPICKVAL(ONB2__Invoice__r.ONB2__Status__c, &apos;Settled&apos;)
),
true,
false
))</formula>
        <inlineHelpText>Signals wheter the linked invoice has been finalized.</inlineHelpText>
        <label>Invoice Is Finalized</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ONB2__Invoice__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Shows the invoice to which this balance is assigned. An invoice is considered paid/settled if the sum of all assigned balances is 0.</description>
        <externalId>false</externalId>
        <inlineHelpText>The invoice, where this balance has been assigned to. An invoice is considered paid/settled, when the sum of all balances is zero.</inlineHelpText>
        <label>Invoice</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ONB2__Invoice__c.ONB2__Account__c</field>
                <operation>equals</operation>
                <valueField>$Source.ONB2__Account__c</valueField>
            </filterItems>
            <infoMessage>Invoices must link to the same account.</infoMessage>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>ONB2__Invoice__c</referenceTo>
        <relationshipLabel>Balances</relationshipLabel>
        <relationshipName>Balances</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ONB2__IsPrepaid__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Is set automatically if the balance was creating from PP-Fields during finalize</description>
        <externalId>false</externalId>
        <inlineHelpText>Is set automatically if the balance was creating from PP-Fields during finalize</inlineHelpText>
        <label>IsPrepaid</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ONB2__LockedReason__c</fullName>
        <deprecated>false</deprecated>
        <description>The reason why this balance has been locked.</description>
        <externalId>false</externalId>
        <inlineHelpText>The reason why this balance has been locked.</inlineHelpText>
        <label>Locked Reason</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Refund via payment provider</fullName>
                    <default>false</default>
                    <label>Refund via payment provider</label>
                </value>
                <value>
                    <fullName>SEPA Export</fullName>
                    <default>false</default>
                    <label>SEPA Export</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>ONB2__Locked__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Locked balances are excluded from certain business processes like assignment to invoices during the invoice run.</description>
        <externalId>false</externalId>
        <inlineHelpText>Locked balances are excluded from certain business processes like assignment to invoices during the invoice run.</inlineHelpText>
        <label>Locked</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ONB2__NoAutoAssignment__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, this balance is not automatically assigned to new invoices during the invoice run.</description>
        <externalId>false</externalId>
        <inlineHelpText>This balance should not be assigned automatically.</inlineHelpText>
        <label>No Auto Assignment</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ONB2__OriginalAmount__c</fullName>
        <deprecated>false</deprecated>
        <description>The original amount of the payment entry before the currency conversion.</description>
        <externalId>false</externalId>
        <inlineHelpText>The original amount of the payment entry before the currency conversion.</inlineHelpText>
        <label>Original Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__OriginalCurrencyCode__c</fullName>
        <deprecated>false</deprecated>
        <description>The original currency code of the payment entry before the currency conversion.</description>
        <externalId>false</externalId>
        <inlineHelpText>The original currency code of the payment entry before the currency conversion.</inlineHelpText>
        <label>Original Currency Code</label>
        <length>3</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__PaymentEntry__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The Payment Entry which was used to create this Balance.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Payment Entry which was used to create this Balance.</inlineHelpText>
        <label>Payment Entry</label>
        <referenceTo>ONB2__PaymentEntry__c</referenceTo>
        <relationshipLabel>Balances</relationshipLabel>
        <relationshipName>Balances</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ONB2__PaymentInstrument__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The payment instrument which has been used to capture this balance.</description>
        <externalId>false</externalId>
        <inlineHelpText>The payment instrument which has been used to capture this balance.</inlineHelpText>
        <label>Payment Instrument</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Only payment instruments of the same account can be linked to balances!</errorMessage>
            <filterItems>
                <field>ONB2__PaymentInstrument__c.ONB2__Account__c</field>
                <operation>equals</operation>
                <valueField>$Source.ONB2__Account__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>ONB2__PaymentInstrument__c</referenceTo>
        <relationshipLabel>Balances</relationshipLabel>
        <relationshipName>Balances</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ONB2__PaymentMethod__c</fullName>
        <deprecated>false</deprecated>
        <description>Specifies the payment method (like Credit Card, Direct Debit, Bank Transfer, Cash) for balance records that represent a payment or refund.</description>
        <externalId>false</externalId>
        <inlineHelpText>The payment method, e.g. Credit Card, Direct Debit, etc.</inlineHelpText>
        <label>Payment Method</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Credit Card</fullName>
                    <default>false</default>
                    <label>Credit Card</label>
                </value>
                <value>
                    <fullName>Direct Debit</fullName>
                    <default>false</default>
                    <label>Direct Debit</label>
                </value>
                <value>
                    <fullName>Bank Transfer</fullName>
                    <default>false</default>
                    <label>Bank Transfer</label>
                </value>
                <value>
                    <fullName>Cash</fullName>
                    <default>false</default>
                    <label>Cash</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>ONB2__PaymentProvider__c</fullName>
        <deprecated>false</deprecated>
        <description>Specifies the payment provider (like Paymill, Paypal, Wirecard) for balance records that represent a payment or refund.</description>
        <externalId>false</externalId>
        <inlineHelpText>The payment provider, e.g.Paymill, Paypal, Wirecard, etc.</inlineHelpText>
        <label>Payment Provider</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Paymill</fullName>
                    <default>false</default>
                    <label>Paymill</label>
                </value>
                <value>
                    <fullName>Paypal</fullName>
                    <default>false</default>
                    <label>Paypal</label>
                </value>
                <value>
                    <fullName>Wirecard</fullName>
                    <default>false</default>
                    <label>Wirecard</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>ONB2__ProviderFee__c</fullName>
        <deprecated>false</deprecated>
        <description>Charges incurred when paying through a payment service provider.</description>
        <externalId>false</externalId>
        <inlineHelpText>Charges incurred when paying through a payment service provider.</inlineHelpText>
        <label>Provider Fee</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ONB2__Reference__c</fullName>
        <deprecated>false</deprecated>
        <description>Specifies additional information related to the payment (like customer ID, transaction number) for a bank statement as well as for matching the payment entry with balances and invoices.</description>
        <externalId>false</externalId>
        <inlineHelpText>The reference may hold further information related to the payment (e.g. Customer No, Transaction No).</inlineHelpText>
        <label>Reference</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__RelatedBalance__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Shows the related balance of a matching balance pair, like the original balance after a balance split.</description>
        <externalId>false</externalId>
        <inlineHelpText>Shows the related balance of a matching balance pair, like the original balance after a balance split.</inlineHelpText>
        <label>Related Balance</label>
        <referenceTo>ONB2__Balance__c</referenceTo>
        <relationshipLabel>Balances</relationshipLabel>
        <relationshipName>Balances</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ONB2__RelatedInvoiceType__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>TEXT(ONB2__RelatedInvoice__r.ONB2__Type__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Related Invoice Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__RelatedInvoice__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Shows the related invoice for the settlement or the proforma invoice process.</description>
        <externalId>false</externalId>
        <inlineHelpText>The related invoice for the settlement process.</inlineHelpText>
        <label>Related Invoice</label>
        <referenceTo>ONB2__Invoice__c</referenceTo>
        <relationshipLabel>Balances (Related Invoice)</relationshipLabel>
        <relationshipName>RelatedBalances</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ONB2__SepaValidationError__c</fullName>
        <deprecated>false</deprecated>
        <description>Contains a list of SEPA-related fields that are not valid and prevent the balance from being exported as SEPA XML.</description>
        <externalId>false</externalId>
        <inlineHelpText>Contains a list of SEPA-related fields that are not valid and prevent the balance from being exported as SEPA XML.</inlineHelpText>
        <label>Sepa Validation Error</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__Subscription__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>If a subscription is set, the balance will only be assigned to invoices of this subscription.</description>
        <externalId>false</externalId>
        <inlineHelpText>If a subscription is set, the balance will only be assigned to invoices of the same subscription.</inlineHelpText>
        <label>Subscription</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Subscriptions must link to the same account.</errorMessage>
            <filterItems>
                <field>ONB2__Subscription__c.ONB2__Account__c</field>
                <operation>equals</operation>
                <valueField>$Source.ONB2__Account__c</valueField>
            </filterItems>
            <infoMessage>Subscriptions must link to the same account.</infoMessage>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>ONB2__Subscription__c</referenceTo>
        <relationshipLabel>Balances</relationshipLabel>
        <relationshipName>Balances</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ONB2__TransactionNo__c</fullName>
        <deprecated>false</deprecated>
        <description>The transaction number as retrieved from the payment provider.</description>
        <externalId>false</externalId>
        <inlineHelpText>The transaction number from the payment provider.</inlineHelpText>
        <label>Transaction No.</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__Type__c</fullName>
        <deprecated>false</deprecated>
        <description>Specifies the type of the balance.</description>
        <externalId>false</externalId>
        <inlineHelpText>The type of the balance.</inlineHelpText>
        <label>Type</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Payment</fullName>
                    <default>false</default>
                    <label>Payment</label>
                </value>
                <value>
                    <fullName>Refund</fullName>
                    <default>false</default>
                    <label>Refund</label>
                </value>
                <value>
                    <fullName>Prepayment</fullName>
                    <default>false</default>
                    <label>Prepayment</label>
                </value>
                <value>
                    <fullName>Payout</fullName>
                    <default>false</default>
                    <label>Payout</label>
                </value>
                <value>
                    <fullName>Clearing</fullName>
                    <default>false</default>
                    <label>Clearing</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Balance</label>
    <listViews>
        <fullName>ONB2__All</fullName>
        <columns>NAME</columns>
        <columns>ONB2__Account__c</columns>
        <columns>ONB2__Date__c</columns>
        <columns>ONB2__Amount__c</columns>
        <columns>ONB2__Type__c</columns>
        <columns>ONB2__Invoice__c</columns>
        <columns>ONB2__Locked__c</columns>
        <columns>ONB2__LockedReason__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>ONB2__Payments</fullName>
        <columns>NAME</columns>
        <columns>ONB2__Account__c</columns>
        <columns>ONB2__Date__c</columns>
        <columns>ONB2__Amount__c</columns>
        <columns>ONB2__Type__c</columns>
        <columns>ONB2__Invoice__c</columns>
        <columns>ONB2__Locked__c</columns>
        <columns>ONB2__LockedReason__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>ONB2__Type__c</field>
            <operation>equals</operation>
            <value>Payment,Prepayment</value>
        </filters>
        <label>Payments</label>
    </listViews>
    <listViews>
        <fullName>ONB2__Payouts</fullName>
        <columns>NAME</columns>
        <columns>ONB2__Account__c</columns>
        <columns>ONB2__Date__c</columns>
        <columns>ONB2__Amount__c</columns>
        <columns>ONB2__Type__c</columns>
        <columns>ONB2__Invoice__c</columns>
        <columns>ONB2__Locked__c</columns>
        <columns>ONB2__LockedReason__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>ONB2__Type__c</field>
            <operation>equals</operation>
            <value>Refund,Payout</value>
        </filters>
        <label>Payouts</label>
    </listViews>
    <listViews>
        <fullName>ONB2__SEPA_Validation_Error</fullName>
        <columns>NAME</columns>
        <columns>ONB2__Account__c</columns>
        <columns>ONB2__Date__c</columns>
        <columns>ONB2__Amount__c</columns>
        <columns>ONB2__Type__c</columns>
        <columns>ONB2__SepaValidationError__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>ONB2__SepaValidationError__c</field>
            <operation>notEqual</operation>
        </filters>
        <label>SEPA Validation Error</label>
    </listViews>
    <listViews>
        <fullName>ONB2__Unassigned</fullName>
        <columns>NAME</columns>
        <columns>ONB2__Account__c</columns>
        <columns>ONB2__Date__c</columns>
        <columns>ONB2__Amount__c</columns>
        <columns>ONB2__Type__c</columns>
        <columns>ONB2__Invoice__c</columns>
        <columns>ONB2__Locked__c</columns>
        <columns>ONB2__LockedReason__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>ONB2__Type__c</field>
            <operation>equals</operation>
            <value>Payment,Refund,Prepayment,Payout</value>
        </filters>
        <filters>
            <field>ONB2__Locked__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>ONB2__Invoice__c</field>
            <operation>equals</operation>
        </filters>
        <label>Unassigned</label>
    </listViews>
    <nameField>
        <displayFormat>B-{0000000000}</displayFormat>
        <label>#</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Balances</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>ONB2__CheckPayment</fullName>
        <active>true</active>
        <description>Check if balances of type Payment and Prepayment have an amount lower than zero.</description>
        <errorConditionFormula>(ISPICKVAL(ONB2__Type__c, &apos;Payment&apos;) || ISPICKVAL(ONB2__Type__c, &apos;Prepayment&apos;)) &amp;&amp; ONB2__Amount__c &gt; 0</errorConditionFormula>
        <errorDisplayField>ONB2__Amount__c</errorDisplayField>
        <errorMessage>A balance of type Payment or Prepayment needs to have an amount lower than zero.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ONB2__CheckRefund</fullName>
        <active>true</active>
        <description>Check if balances of type Refund and Payout have an amount greater than zero.</description>
        <errorConditionFormula>(ISPICKVAL(ONB2__Type__c, &apos;Refund&apos;) || ISPICKVAL(ONB2__Type__c, &apos;Payout&apos;)) &amp;&amp; ONB2__Amount__c &lt; 0</errorConditionFormula>
        <errorDisplayField>ONB2__Amount__c</errorDisplayField>
        <errorMessage>A balance of type Refund or Payout needs to have an amount greater than zero.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ONB2__RestrictLockedChange</fullName>
        <active>true</active>
        <description>It is not allowed to clear the Locked Checkbox or to change the Locked Reason.</description>
        <errorConditionFormula>OR (
    AND (
      ISCHANGED(ONB2__Locked__c),
      false = ONB2__Locked__c
    ),
    AND (
      ISCHANGED(ONB2__LockedReason__c),
      NOT(ISPICKVAL(PRIORVALUE(ONB2__LockedReason__c), &apos;&apos;))
    )
)</errorConditionFormula>
        <errorMessage>It is not allowed to clear the Locked Checkbox or to change the Locked Reason.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>ONB2__ExportBalances</fullName>
        <availability>online</availability>
        <description>Export balance information as a SEPA XML file.</description>
        <displayType>massActionButton</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Export</masterLabel>
        <openType>sidebar</openType>
        <page>ONB2__ExportBalancesPage</page>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
    </webLinks>
    <webLinks>
        <fullName>ONB2__Refund</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Refund</masterLabel>
        <openType>sidebar</openType>
        <page>ONB2__PaymentRefund</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>ONB2__UnregisterPayment</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Unregister Payment</masterLabel>
        <openType>sidebar</openType>
        <page>ONB2__UnregisterPayment</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
