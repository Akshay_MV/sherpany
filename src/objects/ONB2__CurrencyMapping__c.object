<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>List</customSettingsType>
    <description>Map an ISO currency strings to currency symbols, e.g. EUR - €.</description>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>ONB2__CurrencySign__c</fullName>
        <deprecated>false</deprecated>
        <description>The Currency Symbol this setting maps to.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Currency Symbol this setting maps to.</inlineHelpText>
        <label>Currency Sign</label>
        <length>5</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__RoundingDifference__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This field controls whether or not a rounding difference should be applied to the grand total of invoices in this currency.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field controls whether or not a rounding difference should be applied to the grand total of invoices in this currency.</inlineHelpText>
        <label>Activate Rounding</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ONB2__RoundingMethod__c</fullName>
        <deprecated>false</deprecated>
        <description>The method used for rounding the grand total of invoices. Available values: NONE, FLOOR, CEILING, DOWN, UP, HALF_DOWN, HALF_UP, HALF_DOWN_ZERO, HALF_UP_ZERO, HALF_EVEN.</description>
        <externalId>false</externalId>
        <inlineHelpText>The method used for rounding the grand total of invoices. Available values: NONE, FLOOR, CEILING, DOWN, UP, HALF_DOWN, HALF_UP, HALF_DOWN_ZERO, HALF_UP_ZERO, HALF_EVEN.</inlineHelpText>
        <label>Rounding Method</label>
        <length>128</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__RoundingPrecision__c</fullName>
        <deprecated>false</deprecated>
        <description>Controls the precision for the rounding difference. Example with dollars: 0.05 will round to 5 cents, 1.00 will round to whole dollars etc.</description>
        <externalId>false</externalId>
        <inlineHelpText>Controls the precision for the rounding difference. Example with dollars: 0.05 will round to 5 cents, 1.00 will round to whole dollars etc.</inlineHelpText>
        <label>Rounding Precision</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Currency Mappings</label>
    <visibility>Public</visibility>
</CustomObject>
