<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>An item to a sales order. Holds the information for an order position.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>ONB2__AppliedTaxRule__c</fullName>
        <deprecated>false</deprecated>
        <description>The tax rule identified as applicable for this detail.</description>
        <externalId>false</externalId>
        <inlineHelpText>The tax rule identified as applicable for this detail.</inlineHelpText>
        <label>Applied Tax Rule</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__Description__c</fullName>
        <deprecated>false</deprecated>
        <description>The long description for the sales order item.</description>
        <externalId>false</externalId>
        <inlineHelpText>The long description for the sales order item.</inlineHelpText>
        <label>Description</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>ONB2__Discount__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <description>It stores the actually applied discount as a percentage.</description>
        <externalId>false</externalId>
        <inlineHelpText>It stores the actually applied discount as a percentage.</inlineHelpText>
        <label>Discount</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>ONB2__Item__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The item that is ordered.</description>
        <externalId>false</externalId>
        <inlineHelpText>The item that is ordered.</inlineHelpText>
        <label>Item</label>
        <referenceTo>ONB2__Item__c</referenceTo>
        <relationshipLabel>Sales Order Line Items</relationshipLabel>
        <relationshipName>SalesOrderLineItems</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ONB2__ProductGroup__c</fullName>
        <deprecated>false</deprecated>
        <description>The product group of this sales order line item.</description>
        <externalId>false</externalId>
        <inlineHelpText>The product group of this sales order line item.</inlineHelpText>
        <label>Product Group</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__Quantity__c</fullName>
        <defaultValue>1</defaultValue>
        <deprecated>false</deprecated>
        <description>The quantity for this sales order line item.</description>
        <externalId>false</externalId>
        <inlineHelpText>The quantity for this sales order line item.</inlineHelpText>
        <label>Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__SalesOrder__c</fullName>
        <deprecated>false</deprecated>
        <description>The sales order to which this item belongs.</description>
        <externalId>false</externalId>
        <inlineHelpText>The sales order to which this item belongs.</inlineHelpText>
        <label>Sales Order</label>
        <referenceTo>ONB2__SalesOrder__c</referenceTo>
        <relationshipLabel>Sales Order Line Items</relationshipLabel>
        <relationshipName>SalesOrderLineItems</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ONB2__Status__c</fullName>
        <deprecated>false</deprecated>
        <description>The status of this order line item, e.g. New, Delivered, Invoiced.</description>
        <externalId>false</externalId>
        <inlineHelpText>The status of this order line item, e.g. New, Delivered, Invoiced.</inlineHelpText>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>New</fullName>
                    <default>true</default>
                    <label>New</label>
                </value>
                <value>
                    <fullName>Delivered</fullName>
                    <default>false</default>
                    <label>Delivered</label>
                </value>
                <value>
                    <fullName>Invoiced</fullName>
                    <default>false</default>
                    <label>Invoiced</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>ONB2__TaxRate__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <description>The tax rate that applies to this order detail as defined via tax rules. Can be manually overwritten.</description>
        <externalId>false</externalId>
        <inlineHelpText>The tax rate that applies to this order detail as defined via tax rules. Can be manually overwritten.</inlineHelpText>
        <label>Tax Rate</label>
        <precision>6</precision>
        <required>false</required>
        <scale>3</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>ONB2__Title__c</fullName>
        <deprecated>false</deprecated>
        <description>The title or name of a product.</description>
        <externalId>false</externalId>
        <inlineHelpText>The title or name of a product.</inlineHelpText>
        <label>Title</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ONB2__UnitPrice__c</fullName>
        <deprecated>false</deprecated>
        <description>The net or gross price (depending on the type of invoice) of one unit of this item.</description>
        <externalId>false</externalId>
        <inlineHelpText>The net or gross price (depending on the type of invoice) of one unit of this item.</inlineHelpText>
        <label>Unit Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ONB2__Unit__c</fullName>
        <deprecated>false</deprecated>
        <description>The unit associated with this Item.</description>
        <externalId>false</externalId>
        <inlineHelpText>The unit associated with this Item.</inlineHelpText>
        <label>Unit</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>pc</fullName>
                    <default>false</default>
                    <label>pc</label>
                </value>
                <value>
                    <fullName>h</fullName>
                    <default>false</default>
                    <label>h</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Sales Order Line Item</label>
    <nameField>
        <displayFormat>{00000}</displayFormat>
        <label>No.</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Sales Order Line Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>ONB2__StatusIsNotEmpty</fullName>
        <active>true</active>
        <description>Status is mandatory.</description>
        <errorConditionFormula>ISPICKVAL(ONB2__Status__c, &apos;&apos;)</errorConditionFormula>
        <errorDisplayField>ONB2__Status__c</errorDisplayField>
        <errorMessage>Please enter a status.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
