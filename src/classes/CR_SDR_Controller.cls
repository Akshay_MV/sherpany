public class CR_SDR_Controller{
    
    // @AuraEnabled
    // public static String getUserData(){
    //     return [SELECT Id, Name, Users__c, Report_Name__c FROM SetUp__c WHERE Report_Name__c =: 'Conversion Rate SDR' ].Users__c;
    // }
    @AuraEnabled
    public static List<String> getUserListData(){
        return [SELECT Id, Name, Users__c, Report_Name__c FROM SetUp__c WHERE Report_Name__c =: 'Conversion Rate SDR'].Users__c.split(',');
    }
    
    @AuraEnabled
    public static conversionSDRTotal getDefault(Integer year, Integer month, String ls, List<String> userNames){
        
        String UN = '';
        UN += CR_SDR_Controller.getUserNameString();
        List<Lead> leadList = new List<Lead>();
        // System.debug('UN'+UN);
        // String UName = '';
        // UName = CR_SDR_Controller.getUserData();
        List<String> uid = CR_SDR_Controller.getUserListData();
        // System.debug('uid'+uid);
        // List<String> uId = CR_SDR_Controller.getUserData();
        String s = ls == 'All Inbound' ? '%Inbound%' : '%Outbound%';
        String query = ' SELECT Id, Status, CurrencyIsoCode, CreatedDate, Booked_Qualification_Call__c, Responsible_SDR__c, Country, Is_ASL__c, Refusal_in_ASL__c, Lead_Created_Date__c, Was_In_ASL__c, Was_In_MQL__c '+
                       +' FROM Lead '
                       // +' WHERE Was_In_MQL__c = true '
                       +' WHERE CALENDAR_YEAR(Lead_Created_Date__c) =: year '
            			// Changed from CreatedDate to LeadCreatedDate
                       +' AND CALENDAR_MONTH(Lead_Created_Date__c) =: month ';
                      if(ls == 'All Inbound' || ls == 'All Outbound'){
                            query += ' AND LeadSource LIKE : s ';
                      }else if(ls == 'Not Defined'){
                            String blankSource = '';
                            query += ' AND LeadSource LIKE : blankSource ';
                      }else if(ls != 'All'){
                            query += ' AND LeadSource =: ls ';
                      }
                      if(!uid.isEmpty()){
                            query += UN;
                            
                            // query += ' AND Responsible_SDR__c =: UName ';
                      }
                      system.debug('leadLis>>>>>' + query);
        leadList = Database.query(query);
        
        system.debug('leadLis>>>>>' + leadList);
        
        List<Opportunity> oppList = new List<Opportunity>();
        String oppQurty = ' SELECT Id, StageName, CurrencyIsoCode, CreatedDate, SQL__c, Account.BillingCountry, Country__c, Responsible_SDR__c, Booked_Qualification_Call__c, LeadSource, Lead_Created_Date__c '+
                         +' FROM Opportunity '
                         +' WHERE CALENDAR_YEAR(Lead_Created_Date__c) =: year '
                         +' AND Is_converted__c = true '
                         // +' AND ( StageName =: quali OR SQL__c = true ) '
	            		 // Changed from CreatedDate to LeadCreatedDate
                         +' AND CALENDAR_MONTH(Lead_Created_Date__c) =: month ';
                         if(ls == 'All Inbound' || ls == 'All Outbound'){
                             oppQurty += ' AND LeadSource LIKE : s';
                         }else if(ls == 'Not Defined'){
                             String blankSource = ''; 
                             oppQurty += ' AND LeadSource LIKE : blankSource';
                         }else if(ls != 'All'){
                             oppQurty += ' AND LeadSource =: ls';
                         }
        
        
        
        oppList = Database.query(oppQurty);
        
        
        conversionSDRTotal CR = new conversionSDRTotal();
        
        Map<String, conversionSDR> conversionMap = new Map<String, conversionSDR>();
        for(lead l : leadList){
            if(l.Was_In_ASL__c || l.status == 'Converted'){
                if(!conversionMap.containsKey(l.Responsible_SDR__c)){
                    conversionSDR c = new conversionSDR();
                    
                    c.Name = l.Responsible_SDR__c;
                    if(l.Was_In_ASL__c || l.status == 'Converted')  c.NumberOfASL       += 1; 
                    if(l.Booked_Qualification_Call__c != null)      c.NumberOfQCBooked  += 1;
                    
                    conversionMap.put(l.Responsible_SDR__c, c);
                }else{
                    conversionSDR c = conversionMap.get(l.Responsible_SDR__c);
                    c.Name = l.Responsible_SDR__c;
                    if(l.Was_In_ASL__c || l.status == 'Converted')  c.NumberOfASL       += 1; 
                    if(l.Booked_Qualification_Call__c != null)      c.NumberOfQCBooked  += 1;
                    
                    conversionMap.put(l.Responsible_SDR__c, c);
                }
                if(l.Was_In_ASL__c || l.status == 'Converted')  CR.NumberOfASL      += 1;
                if(l.Booked_Qualification_Call__c != null)      CR.NumberOfQCBooked += 1;
            }
        }
        
        for(Opportunity op : oppList){
            if(conversionMap.containsKey(op.Responsible_SDR__c)){
                conversionSDR c = conversionMap.get(op.Responsible_SDR__c);
                if(op.SQL__c) c.NumberOfSQL += 1;
                
                conversionMap.put(op.Responsible_SDR__c, c);
            }
            if(op.SQL__c)  CR.NumberOfSQL      += 1;
        }
        
        
        for(String a : conversionMap.keySet()){
            conversionSDR c = conversionMap.get(a);
            if(c.NumberOfASL != 0) c.CR_ASLtoQCBooked =         ( (c.NumberOfQCBooked * 100) / c.NumberOfASL );
            if(c.NumberOfASL != 0) c.CR_ASLtoSQL =         ( (c.NumberOfSQL * 100) / c.NumberOfASL );
            if(c.NumberOfQCBooked != 0) c.CR_QCBookedtoSQL =    ( (c.NumberOfSQL * 100) / c.NumberOfQCBooked );
            conversionMap.put(a, c);
        }
        
        if(CR.NumberOfASL != 0) CR.CR_ASLtoQCBooked         =   ( (CR.NumberOfQCBooked * 100) / CR.NumberOfASL );
        if(CR.NumberOfASL != 0) CR.CR_ASLtoSQL              =   ( (CR.NumberOfSQL * 100) / CR.NumberOfASL );
        if(CR.NumberOfQCBooked != 0) CR.CR_QCBookedtoSQL    =   ( (CR.NumberOfSQL * 100) / CR.NumberOfQCBooked );
        
        CR.conversionSDRList = conversionMap.values();
        
        system.debug('conversionMap.values()'+conversionMap.values());
        
        return CR;
    }
     @AuraEnabled
     public static String getUserNameString(){
        // String UName = '';
        // UName = CR_SDR_Controller.getUserData();
        List<String> uid = CR_SDR_Controller.getUserListData();
        String UN = '';
        for(Integer i = 0 ; i < uid.size() ; i++ ){
            if(uid[i] == null || uid[i] == ''){
                uid.remove(i);
            }
        }
        for(Integer i = 0 ; i < uid.size() ; i++ ){
            if(i == 0){
                UN += 'AND ( Responsible_SDR__c = \'';
                
                    UN += uid[i];
                
                UN += '\'';
            }else if(i-2 != uid.size() ){
                UN += ' OR Responsible_SDR__c = \'';
                    UN += uid[i];
                
                UN += '\'';
            }
        }
        UN += ' )';
        return UN;
    }
    
    
    // GET USER LIST
     @AuraEnabled
    public static List<String> UserName(Integer Year){
        
        Set<String> userName = new Set<String>();         
        for(Opportunity op : [ SELECT Responsible_SDR__c FROM Opportunity ]) userName.add(op.Responsible_SDR__c);
        
        List<String> check = new List<String>();
        check.addAll(userName);
        check.add('No Responsible Sales');
        return check;
    }
    
    
     // METHOD FOR GETTING PICK-LIST VALUES FROM BACKEND
    @AuraEnabled
    public static List<String> getPickListValues(String ObjName,String FieldName){
        String[] values = new String[]{};
            String[] types = new String[]{ObjName};
                Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(FieldName).getDescribe().getPicklistValues()) {
                if (entry.isActive()) {values.add(entry.getValue());}
            }
        }
        if(ObjName == 'Lead'){
            values.add('All Outbound');
            values.add('All Inbound');
            values.add('Not Defined');
            values.add('All');
        }
        return values;
    }
    
    public class conversionSDRTotal{
        @AuraEnabled public String   Name = '';
        @AuraEnabled public Decimal  NumberOfASL = 0.00;
        @AuraEnabled public Decimal  NumberOfQCBooked = 0.00;
        @AuraEnabled public Decimal  NumberOfSQL = 0.00;
        
        @AuraEnabled public Decimal  CR_ASLtoQCBooked = 0.00;
        @AuraEnabled public Decimal  CR_ASLtoSQL = 0.00;
        @AuraEnabled public Decimal  CR_QCBookedtoSQL = 0.00;
        @AuraEnabled public List<conversionSDR> conversionSDRList = new List<conversionSDR>();
    }
    
    public class conversionSDR{
        @AuraEnabled public String   Name = '';
        @AuraEnabled public Decimal  NumberOfASL = 0.00;
        @AuraEnabled public Decimal  NumberOfQCBooked = 0.00;
        @AuraEnabled public Decimal  NumberOfSQL = 0.00;
        
        @AuraEnabled public Decimal  CR_ASLtoQCBooked = 0.00;
        @AuraEnabled public Decimal  CR_ASLtoSQL = 0.00;
        @AuraEnabled public Decimal  CR_QCBookedtoSQL = 0.00;
    }
    
    public static void test(){
        Integer a,b,c;
        a=1;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
    }
}