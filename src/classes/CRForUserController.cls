public class CRForUserController {
    
    @AuraEnabled
    public static List<String> UserName(Integer Year){
        List<Opportunity> OppList = new List<Opportunity>();
        OppList = [ SELECT Id, Name, StageName, Country__c, CreatedDate, Account.BillingCountry, Account.Name, Responsible_Sales__c, Responsible_Sales__r.Name, Responsible_SDR__c
                      FROM Opportunity 
                     WHERE CALENDAR_YEAR(CreatedDate) =: Year 
                    //   AND ( Responsible_Sales__r.Name = 'Dharmik Shah' 
                    //     OR   Responsible_Sales__r.Name = 'Luca Haag' )
                     ];
        Set<String> userName = new Set<String>();         
        for(Opportunity op : OppList){
            userName.add(op.Responsible_SDR__c);
        }
        
        List<String> check = new List<String>();
        check.addAll(userName);
        check.add('No Responsible Sales');
        System.debug(check);
        System.debug(OppList);
        return check;
    }

    @AuraEnabled
    public static OppTotalWrapper getDefault(Integer year, Integer month, String ls, List<String> userNames){

        System.debug('U name'+userNames);
        String UN = '';
        for(Integer i = 0 ; i < userNames.size() ; i++ ){
            if(userNames[i] == null || userNames[i] == ''){
                userNames.remove(i);
            }
        }
        for(Integer i = 0 ; i < userNames.size() ; i++ ){
            if(i == 0){
                UN += 'AND ( Responsible_SDR__c = \'';
                if(userNames[i] == 'No Responsible Sales'){
                    UN += '';
                }else{
                    UN += userNames[i];
                }
                UN += '\'';
            }else if(i-2 != userNames.size() ){
                UN += ' OR Responsible_SDR__c = \'';
                if(userNames[i] == 'No Responsible Sales'){
                    UN += '';
                }else{
                    UN += userNames[i];
                }
                UN += '\'';
            }
        }
        UN += ' )';
        System.debug(UN);

        OppTotalWrapper otw = new OppTotalWrapper();
        List<Opportunity>   OppList     = new List<Opportunity>();
        List<Lead>          LeadList    = new List<Lead>();
        String s = ls == 'All Inbound' ? '%Inbound%' : '%Outbound%';
        
        String query  = ' SELECT Id, Name, Was_In_ASL__c, Was_In_MQL__c, Responsible_Sales__r.Name, '+
            ' Responsible_Sales__c, StageName, Country__c, LeadSource, Responsible_SDR__c '+
            ' FROM Opportunity '+
            ' WHERE CALENDAR_YEAR(Lead_Created_Date__c) =: year '+
            ' AND CALENDAR_MONTH(Lead_Created_Date__c) =: month ';
        if(ls == 'All Inbound' || ls == 'All Outbound'){
            query += ' AND LeadSource LIKE : s ';
        }
        else if(ls == 'Not Defined'){
            String blankSource = '';
            query += ' AND LeadSource LIKE : blankSource ';
        }
        else if(ls != 'All'){
            query += ' AND LeadSource =: ls ';
        }
        if(!userNames.isEmpty()){
            query += UN;
        }
        OppList = Database.query(query);
        System.debug('query===>>'+query);
        
        String LeadQuery = ' SELECT Id, Name, LeadSource, Country, OwnerId, Owner.Name, Was_In_ASL__c '+
            ' FROM Lead '+
            ' WHERE CALENDAR_YEAR(Lead_Created_Date__c) =: year '+
            ' AND CALENDAR_MONTH(Lead_Created_Date__c) =: month ';
        if(ls == 'All Inbound' || ls == 'All Outbound'){
            LeadQuery += ' AND LeadSource LIKE : s ';
        }
        else if(ls == 'Not Defined'){
            String blankSource = '';
            LeadQuery += ' AND LeadSource LIKE : blankSource ';
        }
        else if(ls != 'All'){
            LeadQuery += ' AND LeadSource =: ls ';
        }
        LeadList = Database.query(LeadQuery);
        // System.debug(LeadList+LeadList);
        
        Map<String, OpportunityDataWrapper> personMap = new Map<String, OpportunityDataWrapper>();
        
        List<OpportunityDataWrapper> oppdwList = new List<OpportunityDataWrapper>();
        System.debug('OppList ====>>>'+OppList);
        for(Opportunity op : OppList){
            System.debug('op.Responsible_SDR__c ==>'+op.Responsible_SDR__c);
            if(! personMap.containsKey(op.Responsible_SDR__c)){
                
                OpportunityDataWrapper odw = new OpportunityDataWrapper();
                
                odw.Name        = op.Responsible_SDR__c == null ? 'No responsible sales' : op.Responsible_SDR__c;
                odw.Country     = op.Country__c;
                
                for(Lead ld : LeadList) if(ld.Owner.Name == op.Responsible_SDR__c && ld.Was_In_ASL__c == true) odw.LeadInASL += 1;
                
                if( op.StageName == 'A Qualification' && op.Was_In_ASL__c ){
                    odw.Qualification       += 1;
                    otw.TotalAQualification += 1;
                }
                if( op.StageName == 'B Discovery' && op.Was_In_ASL__c ){
                    odw.Qualification       += 1;
                    odw.ABStage             += 1;
                    odw.Discovery           += 1;
                    otw.TotalBDiscovery     += 1;
                    otw.TotalABStage        += 1;
                }
                otw.TotalASL += 1;
                personMap.put(op.Responsible_SDR__c,odw);
            }else{
                OpportunityDataWrapper odw = personMap.get(op.Responsible_SDR__c);
                for(Lead ld : LeadList) if(ld.Owner.Name == op.Responsible_SDR__c && ld.Was_In_ASL__c == true) odw.LeadInASL += 1;
                if( op.StageName == 'A Qualification' && op.Was_In_ASL__c ){
                    odw.Qualification       += 1;
                    otw.TotalAQualification += 1;
                }
                if( op.StageName == 'B Discovery' && op.Was_In_ASL__c ){
                    odw.Qualification       += 1;
                    odw.ABStage             += 1;
                    odw.Discovery           += 1;
                    otw.TotalBDiscovery     += 1;
                    otw.TotalABStage        += 1;
                }
                otw.TotalASL += 1;
                personMap.put(op.Responsible_SDR__c,odw);
            }
            oppdwList = personMap.values();
        }
        
        for(Opportunity op : OppList){
            if(personMap.containsKey(op.Responsible_SDR__c)){
                OpportunityDataWrapper odw  = personMap.get(op.Responsible_SDR__c);
                odw.ASLQualification        = odw.LeadInASL     == 0 ? 0 : ( ( odw.Qualification         * 100 ) / odw.LeadInASL     );
                odw.ASLDiscovery            = odw.LeadInASL     == 0 ? 0 : ( ( odw.Discovery             * 100 ) / odw.LeadInASL     );
                odw.QualificationDiscovery  = odw.Qualification == 0 ? 0 : ( ( odw.Discovery             * 100 ) / odw.Qualification );
                otw.TotalASLQualification   = otw.TotalASL      == 0 ? 0 : ( ( otw.TotalAQualification   * 100 ) / otw.TotalASL      );
                otw.TotalASLDiscovery       = otw.TotalASL      == 0 ? 0 : ( ( otw.TotalBDiscovery       * 100 ) / otw.TotalASL      );
            }
            oppdwList = personMap.values();
        }
        
        otw.oppDataList = oppdwList;
        return otw;
    }
    
    
    @AuraEnabled
    public static String getOpportunityJSON(Integer year, Integer month, List<String> userNames){
        
        System.debug('U name'+userNames);
        String UN = '';
        for(Integer i = 0 ; i < userNames.size() ; i++ ){
            if(userNames[i] == null || userNames[i] == ''){
                userNames.remove(i);
            }
        }
        for(Integer i = 0 ; i < userNames.size() ; i++ ){
            if(i == 0){
                UN += 'AND ( Responsible_SDR__c = \'';
                if(userNames[i] == 'No Responsible Sales'){
                    UN += '';
                }else{
                    UN += userNames[i];
                }
                UN += '\'';
            }else if(i-2 != userNames.size() ){
                UN += ' OR Responsible_SDR__c = \'';
                if(userNames[i] == 'No Responsible Sales'){
                    UN += '';
                }else{
                    UN += userNames[i];
                }
                UN += '\'';
            }
        }
        UN += ' )';
        System.debug(UN);

        List<opportunity> lstopp = new List<opportunity>();
        
        String query  = ' SELECT Id, Name, Was_In_ASL__c, Was_In_MQL__c, Responsible_Sales__r.Name, CreatedDate, Lead_Created_Date__c, Date_of_Demo__c, '+
            ' Responsible_Sales__c, StageName, Country__c, LeadSource '+
            ' FROM Opportunity '+
            ' WHERE CALENDAR_YEAR(Lead_Created_Date__c) =: year ';
            // ' AND CALENDAR_MONTH(Lead_Created_Date__c) =: month AND Responsible_Sales__c =: UserId ';
        
        if(!userNames.isEmpty()){
            query += UN;
        }
        lstopp = Database.query(query);
        System.debug(lstopp);

        Map<String,Integer> mapLeadSource = new Map<String,Integer>();
        
        // Map<Integer, Integer> oppMap = new Map<Integer, Integer>();
        Map<String, Map<Integer, Integer>> userMap = new Map<String, Map<Integer, Integer>>();

        Map<Integer, Integer> oppMap = new Map<Integer, Integer>();
        for(Integer i = 1 ; i <= 12 ; i ++){
            oppMap.put(i, 0);
        }
        userMap.put('',oppMap);
        // OppTotalWrapper ow = CRForUserController.getDefault(2019, 12, '');

        Integer dateString = 0;
        userWrapper uw = new userWrapper();

        Map<String, List<RadarDataWrapper>> radarDataMap = new Map<String, List<RadarDataWrapper>>();
        for(opportunity l : lstopp){
            
            if(l.Lead_Created_Date__c != null){
                if(l.StageName == 'B Discovery' && l.Was_In_ASL__c ){
                    if( oppMap.containsKey(l.Lead_Created_Date__c.month())){
                        Integer check = 0;
                        check = oppMap.get(l.Lead_Created_Date__c.month()) + 1;
                        oppMap.put(l.Lead_Created_Date__c.month(),check);
                        System.debug(oppMap);
                    }
                }
            }
        }
        
        list<RadarDataWrapper> radarData = new list<RadarDataWrapper>();
 
        for(Integer key : oppMap.keySet())
        {
           RadarDataWrapper rdw = new RadarDataWrapper();
            // rdw.UserName = 'Akshay';
            rdw.name=key;
            rdw.y=oppMap.get(key);
            radarData.add(rdw);
        }
        // system.debug('rdw---'+radarData);
        return System.json.serialize(radarData);
        // return userMap;
    }
 
    /**
     * Wrapper class to serialize as JSON as return Value
     * */

    public class userWrapper{
        @AuraEnabled public String UserName;
        @AuraEnabled public List<RadarDataWrapper> radarDataList;
    }

    class RadarDataWrapper
    {
       @AuraEnabled
       public String UserName; 
       @AuraEnabled
       public Integer name;
       @AuraEnabled
       public Integer y;
 
    }
    
    
    
    // METHOD FOR GETTING PICK-LIST VALUES FROM BACKEND
    @AuraEnabled
    public static List<String> getPickListValues(String ObjName,String FieldName){
        String[] values = new String[]{};
            String[] types = new String[]{ObjName};
                Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(FieldName).getDescribe().getPicklistValues()) {
                if (entry.isActive()) {values.add(entry.getValue());}
            }
        }
        if(ObjName == 'Lead'){
            values.add('All Outbound');
            values.add('All Inbound');
            values.add('Not Defined');
            values.add('All');
        }
        return values;
    }
    
    public class OppTotalWrapper{
        @AuraEnabled public String Name;
        
        // ASL Lead count total.
        @AuraEnabled public Decimal TotalLeadInASL   = 0;
        // Lead Stage count total.
        @AuraEnabled public Decimal TotalASL = 0;
        @AuraEnabled public Decimal TotalAQualification = 0;
        @AuraEnabled public Decimal TotalBDiscovery = 0;
        @AuraEnabled public Decimal TotalABStage    = 0;
        
        // Conversion Rate total.
        @AuraEnabled public Decimal TotalASLQualification   = 0;
        @AuraEnabled public Decimal TotalASLDiscovery       = 0;
        @AuraEnabled public Decimal TotalABStagePer    = 0;
        
        @AuraEnabled public List<OpportunityDataWrapper> oppDataList;
    }
    
    public class OpportunityDataWrapper{
        @AuraEnabled public String Name;
        @AuraEnabled public String StageName;
        @AuraEnabled public String Country;
        // ASL Lead count
        @AuraEnabled public Decimal LeadInASL   = 0;
        // Lead Stage count
        @AuraEnabled public Decimal Qualification   = 0;
        @AuraEnabled public Decimal Discovery       = 0;
        @AuraEnabled public Decimal ABStage         = 0;
        // Conversion Rate
        @AuraEnabled public Decimal ASLQualification   = 0;
        @AuraEnabled public Decimal ASLDiscovery       = 0;
        @AuraEnabled public Decimal QualificationDiscovery       = 0;
        
        // Depricating...
        @AuraEnabled public Decimal QD = 0.00;
        @AuraEnabled public Decimal DP = 0.00;
        @AuraEnabled public Decimal PC = 0.00;
        @AuraEnabled public Decimal CH = 0.00;
        @AuraEnabled public Decimal HW = 0.00;
        @AuraEnabled public Decimal QW = 0.00;
    }
}