public class userSetUpController {
   public static List<SetUp__c>setupLIst {get;set;}
   
   
   @AuraEnabled
   public static List<WrapUser> getDefault(String rptName){
       System.debug(rptName);
      List<WrapUser> WrapUserList = new List<WrapUser> ();
      Set<Id> UserIDSet = new Set<Id>();
      Set<String>UserNameSet = new Set<String>();
      setupLIst =[SELECT Id, Name,Users__c,Report_Name__c FROM SetUp__c WHERE Report_Name__c =:rptName];
      if(setupLIst.size()>0){
          for(Integer i = 0; i< setupLIst.size() ;i++){
              if(setupLIst[i].Users__c!=''){
                  if(rptName == 'Conversion Rate SDR'){
                      String userString = setupLIst[i].Users__c;
                      List<String> strList = userString.split(',');
                      UserNameSet.addAll(strList);
                    
                  }else{
                      String userString = setupLIst[i].Users__c;
                      List<Id> strList = userString.split(',');
                      UserIDSet.addAll(strList);
                  }
              }
          }
      }
      for(User u : [SELECT Id, Name FROM User ORDER BY Name]){
          WrapUserList.add(new WrapUser(u.Id, u.Name, UserIDSet.contains(u.Id) ? true : false,UserNameSet.contains(u.Name) ? true : false));
      }
      
       return WrapUserList;
   }
   
   @AuraEnabled
   public static String saveSetup(String[] lstUserId, String[] lstUserName, String rptName){
       String msg='';
       List<User>usList = new List<User>([SELECT Id,Name FROM User WHERE Id =:lstUserId OR Name =: lstUserName]);
       setupLIst = new List<SetUp__c>([SELECT Id, Name,Users__c,Report_Name__c FROM SetUp__c WHERE Report_Name__c =:rptName]);
       String[] tmp1 = New String[]{};
       String[] tmp2 = New String[]{};
       String Idstring;
       String NameString;
        for(User u : usList){
           tmp1.add(u.Id);
           tmp2.add(u.Name);
        }
        Idstring = string.join(tmp1,',');
        NameString = string.join(tmp2,',');
        SetUp__c setup = new SetUp__c();
        if(setupLIst.size()>0){
            setup.Id =setupLIst[0].Id;
        }
        if(rptName == 'Opportunity stage conversion rate'){
            setup.Name = 'CR Opportunity Stage';
            setup.Users__c=IdString;
            setup.Report_Name__c ='Opportunity stage conversion rate';
            upsert setup;
        }
        else{
            setup.Name = 'CR SDR';
            setup.Users__c=NameString;
            setup.Report_Name__c ='Conversion Rate SDR';
            upsert setup;
        }
        return msg;
    }
    
    public class WrapUser{
        @AuraEnabled public boolean isSelected{get;set;}
        @AuraEnabled public boolean ischecked{get;set;}
        @AuraEnabled public String UserName{get;set;}
        @AuraEnabled public String UserId{get;set;}
        
        
        public WrapUser(String Id, String UserName, Boolean isSelected, Boolean ischecked){
            this.UserId = Id;
            this.UserName = UserName;
            this.isSelected = isSelected;
            this.ischecked = ischecked;
        }
    }
}