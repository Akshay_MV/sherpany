@isTest
public class Test_CRPCController {

    @isTest
	public static void test() {
	    
	    List<Opportunity> oppList = new List<Opportunity>();
	    List<Account> accList = new List<Account>();
	    
	    for(Integer i = 0 ; i < 16 ; i++){
	        Account acc = new Account();
    	    acc.name = 'Test'+i;
    	    if(i == 0 || i == 8){
                acc.BillingCountry = 'India';
            }else if(i == 1 || i == 9){
                acc.BillingCountry = 'Canada';
            }else if(i == 2 || i == 10){
                acc.BillingCountry = 'Belgium';
            }else if(i == 3 || i == 11){
                acc.BillingCountry = 'Bhutan';
            }else if(i == 4 || i == 12){
                acc.BillingCountry = 'Germany';
            }else if(i == 5 || i == 13){
                acc.BillingCountry = 'Indonesia';
            }else if(i == 6 || i == 14){
                acc.BillingCountry = 'Mexico';
            }else if(i == 7 || i == 15){
                acc.BillingCountry = 'Qatar';
            }
    	    accList.add(acc);
	    }
	    
	    insert accList;
	    
	    for(Integer i = 0 ; i < 16 ; i++){
            Opportunity opp = new Opportunity();
            opp.Name = 'Test';
            opp.AccountId = accList[i].Id;
            
            if(i == 0 || i == 8){
                opp.StageName = 'A Qualification';
            }else if(i == 1 || i == 9){
                opp.StageName = 'B Discovery';
            }else if(i == 2 || i == 10){
                opp.StageName = 'C POV';
            }else if(i == 3 || i == 11){
                opp.StageName = 'D Closing';
            }else if(i == 4 || i == 12){
                opp.StageName = 'E Closed Won';
            }else if(i == 5 || i == 13){
                opp.StageName = 'Contract Replaced';
            }else if(i == 6 || i == 14){
                opp.StageName = 'Closed Dead';
                opp.Termination_Reason__c = 'Termination for Testing';
            }else if(i == 7 || i == 15){
                opp.StageName = 'Terminated';
                opp.Termination_Reason__c = 'Termination for Testing';
                opp.Termination_Date__c = System.today();
            }
            opp.CloseDate = System.today();
            oppList.add(opp);
	    }
        insert oppList;
        List<String> userNames = new List<String>();
        userNames.add('No Responsible Sales');
        userNames.add(null);
        
        CRPCController.main(2019, userNames);
        CRPCController.mainPer(2019, userNames);
        CRPCController.countryData(2019, userNames);
        
        userNames = new List<String>();
        CRPCController.main(2019, userNames);
        CRPCController.mainPer(2019, userNames);
        CRPCController.countryData(2019, userNames);
        
        CRPCController.UserName(2019);
        CRPCController.getPickListValues('Opportunity','Migrate_demo_room_in_productive_room__c');
        
	}
}