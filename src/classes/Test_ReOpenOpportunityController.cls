@isTest(SeeAllData = true)
public class Test_ReOpenOpportunityController {
    
    @isTest
    public static void Method1(){
        
        List<Opportunity> op = new List<Opportunity>();
        List<Event> ev = new List<Event>();
        List<Task> ts = new List<Task>();
        
        Account acc = new Account();
        acc.Name = 'test MV';
        acc.BillingCountry = 'India';
        acc.OwnerId = [SELECT Id FROM User WHERE isActive = true LIMIT 1].Id;
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.Name = 'Test';
        opp.StageName = 'Prospecting';
        opp.CloseDate = System.today()+5;
        opp.Ownerid = UserInfo.getUserId();
        Insert opp; 
        
        Attachment att = new Attachment();
        att.Name = 'test';
        att.body = blob.valueOf('test');
        att.ParentId = opp.Id;
        Insert att;
        
        Event event = new Event();
        event.subject = 'demo';
        event.StartDateTime = System.today();
        event.EndDateTime = System.Today()+5;
        event.type = 'Call';
        event.CurrencyIsoCode = 'EUR';
        event.Ownerid = UserInfo.getUserId();
        event.WhatId = opp.Id;
        
        Insert event;
        
        Task task = new Task();
        task.subject = 'Cold Call';
        task.Priority = 'High';
        task.CurrencyIsoCode = 'EUR';
        task.Ownerid = UserInfo.getUserId();
        task.status = 'Open';
        task.WhatId = opp.Id;
        Insert task;
        
        ReOpenOpportunityController.createNewOpp(opp);
        ReOpenOpportunityController.getDefault(opp.Id);
        ReOpenOpportunityController.updateOldOpp(opp);
        ReOpenOpportunityController.getPickListValues('Opportunity','StageName');
        ReOpenOpportunityController.getPickListValues('Lead','Status');
        Util.getParentRecordFields('Account');
        Util.getPickListValues('task','status');
    }
}