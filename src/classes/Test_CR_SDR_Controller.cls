@isTest
private class Test_CR_SDR_Controller {
    @isTest
	private static void doTest() {
        
        List<String> usernames = new List<String>();
        usernames.add('Mathias Brenner');
        usernames.add('Dharmik Shah');
        usernames.add('Luca Haag');
        usernames.add('No Responsible Sales');
        usernames.add('All');
        
        List<User> userList  =new List<User>();
        userList = [ SELECT Id FROM User ];
        
        List<SetUp__c>setupList = new List<SetUp__c>();
        for(Integer i=0;i<1;i++){
          SetUp__c setup = new SetUp__c();
            setup.Name='Test';
            setup.Users__c = UserInfo.getUserId();
            setup.Report_Name__c= 'Conversion Rate SDR';
            setupList.add(setup);  
        }
        upsert setupList;
        
        // String userString = setupList[0].Users__c;
        //   List<String> strList = userString.split(',');
        // CR_SDR_Controller.getUserData();
        CR_SDR_Controller.getUserListData();
        CR_SDR_Controller.getDefault(2020, 1, 'All Inbound', usernames);
        
        CR_SDR_Controller.UserName(2020);
        CR_SDR_Controller.getPickListValues('Lead', 'Industries__c');
	
	    CR_SDR_Controller.getDefault(2020, 1, 'Not Defined', usernames);
	    CR_SDR_Controller.getDefault(2020, 1, 'Al', usernames);
	    CR_SDR_Controller.test();
	}

}