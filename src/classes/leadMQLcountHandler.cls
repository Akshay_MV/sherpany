public class leadMQLcountHandler {
    
    List<Lead> recordNewList = new List<Lead>();
    List<Lead> recordOldList = new List<Lead>();
    Map<Id, Lead> recordNewMap = new Map<Id, Lead>();
    Map<Id, Lead> recordOldMap = new Map<Id, Lead>();
    Boolean isInsert, isUpdate, isDelete, isUndelete = false;
    Private Boolean run = true;

    public leadMQLcountHandler(List<Lead> newList, List<Lead> oldList, Map<Id, Lead> newMap, Map<Id, Lead> oldMap, boolean isInsert, boolean isUpdate, Boolean isDelete, Boolean isUndelete) {
        this.recordNewList = newList;
        this.recordOldList = oldList;
        this.recordNewMap = newMap;
        this.recordOldMap = oldMap;
        this.isInsert = isInsert;
        this.isUpdate = isUpdate;
        this.isDelete = isDelete;
        this.isUndelete = isUndelete;
    }
    
    public void BeforeInsertEvent(){ }
    
    public void BeforeUpdateEvent(){
        for(Integer i = 0 ; i < recordNewList.size() ; i++ ){
            if(recordNewList[i].Status == 'Qualified' && recordOldList[i].Status != 'Qualified') recordNewList[i].Was_In_MQL__c = true;
            if(recordNewList[i].Status == 'In progress' && recordOldList[i].Status != 'In progress') recordNewList[i].Was_In_ASL__c = true;
        }
    }
    
    public void BeforeDeleteEvent(){ }
    
    public void AfterInsertEvent(){ }
    
    public void AfterUpdateEvent(){ }
    
    public void AfterDeleteEvent(){ }
    
    public void AfterUndeleteEvent(){ }

    public static void test(){
        Integer a = 1;
        Integer b;
        Integer c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
    }
}