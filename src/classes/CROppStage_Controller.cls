public class CROppStage_Controller {

    private static final string A_QULIFICATION   = 'A Qualification';
    private static final string B_Discovery      = 'B Discovery';
    private static final string C_POV            = 'C POV';
    private static final string D_Closing        = 'D Closing';
    private static final string E_Closed_Won     = 'E Closed Won';
    private static final string Request_Handover = 'Request Handover';
    
    @AuraEnabled
    public static oppUser getDefault(Integer year, Integer month, String selectedUser, String selectedCountry){
        List<OpportunityFieldHistory> oppHistoryList = new List<OpportunityFieldHistory>();
        String StageName = 'StageName';
        String created  = 'created';
        String query =  ' SELECT Id, Field, NewValue, OldValue, IsDeleted, OpportunityId, '+
                        ' Opportunity.Responsible_Sales__r.Name, Opportunity.Responsible_Sales__c, '+
                        ' Opportunity.Country__c, Opportunity.StageName '+
                        ' FROM OpportunityFieldHistory '+
                        ' WHERE CALENDAR_YEAR(CreatedDate) =: year '+
                        ' AND CALENDAR_MONTH(CreatedDate) =: month '+
                        ' AND ( Field =: StageName OR Field =: created ) '+
                        ' AND IsDeleted = false ';
        oppHistoryList = Database.query(query);

        oppUser oppUser = new oppUser();
        Map<String, oppUserData> userMap = new Map<String, oppUserData>();
        Decimal AQ = 0;
        for(OpportunityFieldHistory opp : oppHistoryList){
            System.debug('opp.Opportunity.Responsible_Sales__c'+opp.Opportunity.Responsible_Sales__c);
            if(! userMap.containsKey(opp.Opportunity.Responsible_Sales__c)){
                oppUserData ud  = new oppUserData();
                ud.Name         = opp.Opportunity.Responsible_Sales__r.Name == null ? 'No responsible sale' : opp.Opportunity.Responsible_Sales__r.Name;

                // calculating individual value
                if(opp.OldValue == A_QULIFICATION   && opp.NewValue == B_Discovery )        ud.QualificationDiscovery   += 1;
                if(opp.OldValue == B_Discovery      && opp.NewValue == C_POV )              ud.DiscoveryPOV             += 1;
                if(opp.OldValue == C_POV            && opp.NewValue == D_Closing )          ud.POVClosing               += 1;
                if(opp.OldValue == D_Closing        && opp.NewValue == Request_Handover )   ud.ClosingHandover          += 1;
                if(opp.OldValue == Request_Handover && opp.NewValue == E_Closed_Won )       ud.HandoverWon              += 1;
                if(opp.OldValue == A_QULIFICATION   && opp.NewValue == E_Closed_Won )       ud.QualificationWon         += 1;

                // Calculating total
                if(opp.OldValue == A_QULIFICATION   && opp.NewValue == B_Discovery )        oppUser.TotalQualificationDiscovery   += 1;
                if(opp.OldValue == B_Discovery      && opp.NewValue == C_POV )              oppUser.TotalDiscoveryPOV             += 1;
                if(opp.OldValue == C_POV            && opp.NewValue == D_Closing )          oppUser.TotalPOVClosing               += 1;
                if(opp.OldValue == D_Closing        && opp.NewValue == Request_Handover )   oppUser.TotalClosingHandover          += 1;
                if(opp.OldValue == Request_Handover && opp.NewValue == E_Closed_Won )       oppUser.TotalHandoverWon              += 1;
                if(opp.OldValue == A_QULIFICATION   && opp.NewValue == E_Closed_Won )       oppUser.TotalQualificationWon         += 1;
                
                // For Total of each stage
                if(opp.OldValue == A_QULIFICATION   || opp.Opportunity.StageName == A_QULIFICATION)     ud.TotalQualification   += 1;
                if(opp.OldValue == B_Discovery      || opp.Opportunity.StageName == B_Discovery)        ud.TotalDiscovery       += 1;
                if(opp.OldValue == C_POV            || opp.Opportunity.StageName == C_POV)              ud.TotalPOV             += 1;
                if(opp.OldValue == D_Closing        || opp.Opportunity.StageName == D_Closing)          ud.TotalClosing         += 1;
                if(opp.OldValue == Request_Handover || opp.Opportunity.StageName == Request_Handover)   ud.TotalHandover        += 1;
                if(opp.OldValue == E_Closed_Won     || opp.Opportunity.StageName == E_Closed_Won)       ud.TotalWon             += 1;

                userMap.put(opp.Opportunity.Responsible_Sales__c, ud);
            } else {
                oppUserData ud  = userMap.get(opp.Opportunity.Responsible_Sales__c);

                // calculating individual value
                if(opp.OldValue == A_QULIFICATION   && opp.NewValue == B_Discovery )      ud.QualificationDiscovery   += 1;
                if(opp.OldValue == B_Discovery      && opp.NewValue == C_POV )            ud.DiscoveryPOV             += 1;
                if(opp.OldValue == C_POV            && opp.NewValue == D_Closing )        ud.POVClosing               += 1;
                if(opp.OldValue == D_Closing        && opp.NewValue == Request_Handover ) ud.ClosingHandover          += 1;
                if(opp.OldValue == Request_Handover && opp.NewValue == E_Closed_Won )     ud.HandoverWon              += 1;
                if(opp.OldValue == A_QULIFICATION   && opp.NewValue == E_Closed_Won )     ud.QualificationWon         += 1;

                // Calculating total
                if(opp.OldValue == A_QULIFICATION   && opp.NewValue == B_Discovery )        oppUser.TotalQualificationDiscovery   += 1;
                if(opp.OldValue == B_Discovery      && opp.NewValue == C_POV )              oppUser.TotalDiscoveryPOV             += 1;
                if(opp.OldValue == C_POV            && opp.NewValue == D_Closing )          oppUser.TotalPOVClosing               += 1;
                if(opp.OldValue == D_Closing        && opp.NewValue == Request_Handover )   oppUser.TotalClosingHandover          += 1;
                if(opp.OldValue == Request_Handover && opp.NewValue == E_Closed_Won )       oppUser.TotalHandoverWon              += 1;
                if(opp.OldValue == A_QULIFICATION   && opp.NewValue == E_Closed_Won )       oppUser.TotalQualificationWon         += 1;

                // For Total of each stage                                
                if(opp.OldValue == A_QULIFICATION   || opp.Opportunity.StageName == A_QULIFICATION)     ud.TotalQualification   += 1;
                if(opp.OldValue == B_Discovery      || opp.Opportunity.StageName == B_Discovery)        ud.TotalDiscovery       += 1;
                if(opp.OldValue == C_POV            || opp.Opportunity.StageName == C_POV)              ud.TotalPOV             += 1;
                if(opp.OldValue == D_Closing        || opp.Opportunity.StageName == D_Closing)          ud.TotalClosing         += 1;
                if(opp.OldValue == Request_Handover || opp.Opportunity.StageName == Request_Handover)   ud.TotalHandover        += 1;
                if(opp.OldValue == E_Closed_Won     || opp.Opportunity.StageName == E_Closed_Won)       ud.TotalWon             += 1;

                userMap.put(opp.Opportunity.Responsible_Sales__c, ud);
            }
        }

        // User Percentage:
        for(OpportunityFieldHistory opp : oppHistoryList){
            if(userMap.containsKey(opp.Opportunity.Responsible_Sales__c)){
                oppUserData ud  = userMap.get(opp.Opportunity.Responsible_Sales__c);
                
                Decimal A = ud.TotalQualification        == 0 ? 0 : ( ( ud.QualificationDiscovery * 100) / ud.TotalQualification     );
                Decimal B = ud.QualificationDiscovery    == 0 ? 0 : ( ( ud.DiscoveryPOV           * 100) / ud.QualificationDiscovery );
                Decimal C = ud.DiscoveryPOV              == 0 ? 0 : ( ( ud.POVClosing             * 100) / ud.DiscoveryPOV           );
                Decimal D = ud.POVClosing                == 0 ? 0 : ( ( ud.ClosingHandover        * 100) / ud.POVClosing             );
                Decimal E = ud.ClosingHandover           == 0 ? 0 : ( ( ud.HandoverWon            * 100) / ud.ClosingHandover        );
                Decimal F = ud.TotalQualification        == 0 ? 0 : ( ( ud.HandoverWon            * 100) / ud.TotalQualification     );

                ud.QualificationDiscoveryPer    =  A.setScale(2);
                ud.DiscoveryPOVPer              =  B.setScale(2);
                ud.POVClosingPer                =  C.setScale(2);
                ud.ClosingHandoverPer           =  D.setScale(2);
                ud.HandoverWonPer               =  E.setScale(2);
                ud.QualificationWonPer          =  F.setScale(2);
             }
        }

        // User total Percentage
        oppUser.TotalQualificationDiscoveryPer  = oppUser.TotalQualificationDiscovery == 0 ? 0 : ( ( oppUser.TotalQualificationDiscovery * 100 ) / oppUser.TotalQualificationDiscovery );
        oppUser.TotalDiscoveryPOVPer            = oppUser.TotalQualificationDiscovery == 0 ? 0 : ( ( oppUser.TotalDiscoveryPOV * 100 ) / oppUser.TotalQualificationDiscovery );
        oppUser.TotalPOVClosingPer              = oppUser.TotalDiscoveryPOV == 0 ? 0 : ( ( oppUser.TotalPOVClosing * 100 ) / oppUser.TotalDiscoveryPOV );
        oppUser.TotalClosingHandoverPer         = oppUser.TotalPOVClosing == 0 ? 0 : ( ( oppUser.TotalClosingHandover * 100 ) / oppUser.TotalPOVClosing );
        oppUser.TotalHandoverWonPer             = oppUser.TotalClosingHandover == 0 ? 0 : ( ( oppUser.TotalHandoverWon * 100 ) / oppUser.TotalClosingHandover );
        oppUser.TotalQualificationWonPer        = oppUser.TotalHandoverWon == 0 ? 0 : ( ( oppUser.TotalQualificationWon * 100 ) / oppUser.TotalHandoverWon );

        // Country Map
        Map<String, oppCountryData> countryMap = new Map<String, oppCountryData>();
        for(OpportunityFieldHistory opp : oppHistoryList){
            if(! countryMap.containsKey(opp.Opportunity.Country__c)){
                oppCountryData ud  = new oppCountryData();
                ud.Name         = opp.Opportunity.Country__c == null ? 'No country added' : opp.Opportunity.Country__c;
                if(opp.OldValue == A_QULIFICATION   && opp.NewValue == B_Discovery )        ud.QualificationDiscovery   += 1;
                if(opp.OldValue == B_Discovery      && opp.NewValue == C_POV )              ud.DiscoveryPOV             += 1;
                if(opp.OldValue == C_POV            && opp.NewValue == D_Closing )          ud.POVClosing               += 1;
                if(opp.OldValue == D_Closing        && opp.NewValue == Request_Handover )   ud.ClosingHandover          += 1;
                if(opp.OldValue == Request_Handover && opp.NewValue == E_Closed_Won )       ud.HandoverWon              += 1;
                if(opp.OldValue == A_QULIFICATION   && opp.NewValue == E_Closed_Won )       ud.QualificationWon         += 1;
                                
                if(opp.OldValue == A_QULIFICATION   || opp.Opportunity.StageName == A_QULIFICATION)     ud.TotalQualification   += 1;
                if(opp.OldValue == B_Discovery      || opp.Opportunity.StageName == B_Discovery)        ud.TotalDiscovery       += 1;
                if(opp.OldValue == C_POV            || opp.Opportunity.StageName == C_POV)              ud.TotalPOV             += 1;
                if(opp.OldValue == D_Closing        || opp.Opportunity.StageName == D_Closing)          ud.TotalClosing         += 1;
                if(opp.OldValue == Request_Handover || opp.Opportunity.StageName == Request_Handover)   ud.TotalHandover        += 1;
                if(opp.OldValue == E_Closed_Won     || opp.Opportunity.StageName == E_Closed_Won)       ud.TotalWon             += 1;


                countryMap.put(opp.Opportunity.Country__c, ud);
            }else {
                oppCountryData ud  = countryMap.get(opp.Opportunity.Country__c);
                if(opp.OldValue == A_QULIFICATION   && opp.NewValue == B_Discovery )                    ud.QualificationDiscovery   += 1;
                if(opp.OldValue == B_Discovery      && opp.NewValue == C_POV )                          ud.DiscoveryPOV             += 1;
                if(opp.OldValue == C_POV            && opp.NewValue == D_Closing )                      ud.POVClosing               += 1;
                if(opp.OldValue == D_Closing        && opp.NewValue == Request_Handover )               ud.ClosingHandover          += 1;
                if(opp.OldValue == Request_Handover && opp.NewValue == E_Closed_Won )                   ud.HandoverWon              += 1;
                if(opp.OldValue == A_QULIFICATION   && opp.NewValue == E_Closed_Won )                   ud.QualificationWon         += 1;

                if(opp.OldValue == A_QULIFICATION   || opp.Opportunity.StageName == A_QULIFICATION)     ud.TotalQualification       += 1;
                if(opp.OldValue == B_Discovery      || opp.Opportunity.StageName == B_Discovery)        ud.TotalDiscovery           += 1;
                if(opp.OldValue == C_POV            || opp.Opportunity.StageName == C_POV)              ud.TotalPOV                 += 1;
                if(opp.OldValue == D_Closing        || opp.Opportunity.StageName == D_Closing)          ud.TotalClosing             += 1;
                if(opp.OldValue == Request_Handover || opp.Opportunity.StageName == Request_Handover)   ud.TotalHandover            += 1;
                if(opp.OldValue == E_Closed_Won     || opp.Opportunity.StageName == E_Closed_Won)       ud.TotalWon                 += 1;

                countryMap.put(opp.Opportunity.Country__c, ud);
            }
        }
        
        // Country Total
        for(OpportunityFieldHistory opp : oppHistoryList){
            if(countryMap.containsKey(opp.Opportunity.Country__c)){
                oppCountryData ud  = countryMap.get(opp.Opportunity.Country__c);
                
                Decimal A = ud.TotalQualification        == 0 ? 0 : ( ( ud.QualificationDiscovery * 100) / ud.TotalQualification     );
                Decimal B = ud.QualificationDiscovery    == 0 ? 0 : ( ( ud.DiscoveryPOV           * 100) / ud.QualificationDiscovery );
                Decimal C = ud.DiscoveryPOV              == 0 ? 0 : ( ( ud.POVClosing             * 100) / ud.DiscoveryPOV           );
                Decimal D = ud.POVClosing                == 0 ? 0 : ( ( ud.ClosingHandover        * 100) / ud.POVClosing             );
                Decimal E = ud.ClosingHandover           == 0 ? 0 : ( ( ud.HandoverWon            * 100) / ud.ClosingHandover        );
                Decimal F = ud.TotalQualification        == 0 ? 0 : ( ( ud.HandoverWon            * 100) / ud.TotalQualification     );

                ud.QualificationDiscoveryPer    =  A.setScale(2);
                ud.DiscoveryPOVPer              =  B.setScale(2);
                ud.POVClosingPer                =  C.setScale(2);
                ud.ClosingHandoverPer           =  D.setScale(2);
                ud.HandoverWonPer               =  E.setScale(2);
                ud.QualificationWonPer          =  F.setScale(2);
             }
        }

        oppUser.userDataList = userMap.values();
        oppUser.countryDataList = countryMap.values();

        return oppUser;
    }

    public class oppUser{
        // Total Number
        @AuraEnabled public Decimal TotalQualificationDiscovery     = 0.00;
        @AuraEnabled public Decimal TotalDiscoveryPOV               = 0.00;
        @AuraEnabled public Decimal TotalPOVClosing                 = 0.00;
        @AuraEnabled public Decimal TotalClosingHandover            = 0.00;
        @AuraEnabled public Decimal TotalHandoverWon                = 0.00;
        @AuraEnabled public Decimal TotalQualificationWon           = 0.00;
        
        // Total Percentage
        @AuraEnabled public Decimal TotalQualificationDiscoveryPer  = 0.00;
        @AuraEnabled public Decimal TotalDiscoveryPOVPer            = 0.00;
        @AuraEnabled public Decimal TotalPOVClosingPer              = 0.00;
        @AuraEnabled public Decimal TotalClosingHandoverPer         = 0.00;
        @AuraEnabled public Decimal TotalHandoverWonPer             = 0.00;
        @AuraEnabled public Decimal TotalQualificationWonPer        = 0.00;

        @AuraEnabled public List<oppUserData> userDataList;
        @AuraEnabled public List<oppCountryData> countryDataList;
    }

    public class oppUserData{
        @AuraEnabled public string Name;
        
        @AuraEnabled public Decimal TotalQualification              = 0.00;
        @AuraEnabled public Decimal TotalDiscovery                  = 0.00;
        @AuraEnabled public Decimal TotalPOV                        = 0.00;
        @AuraEnabled public Decimal TotalClosing                    = 0.00;
        @AuraEnabled public Decimal TotalHandover                   = 0.00;
        @AuraEnabled public Decimal TotalWon                        = 0.00;

        // Number
        @AuraEnabled public Decimal QualificationDiscovery          = 0.00;
        @AuraEnabled public Decimal DiscoveryPOV                    = 0.00;
        @AuraEnabled public Decimal POVClosing                      = 0.00;
        @AuraEnabled public Decimal ClosingHandover                 = 0.00;
        @AuraEnabled public Decimal HandoverWon                     = 0.00;
        @AuraEnabled public Decimal QualificationWon                = 0.00;

        // Percentage
        @AuraEnabled public Decimal QualificationDiscoveryPer       = 0.00;
        @AuraEnabled public Decimal DiscoveryPOVPer                 = 0.00;
        @AuraEnabled public Decimal POVClosingPer                   = 0.00;
        @AuraEnabled public Decimal ClosingHandoverPer              = 0.00;
        @AuraEnabled public Decimal HandoverWonPer                  = 0.00;
        @AuraEnabled public Decimal QualificationWonPer             = 0.00;
    }

    public class oppCountry{
        // Total Number
        @AuraEnabled public Decimal TotalQualificationDiscovery;
        @AuraEnabled public Decimal TotalDiscoveryPOV;
        @AuraEnabled public Decimal TotalPOVClosing;
        @AuraEnabled public Decimal TotalClosingHandover;
        @AuraEnabled public Decimal TotalHandoverWon;
        @AuraEnabled public Decimal TotalQualificationWon;

        // Total Percentage
        @AuraEnabled public Decimal TotalQualificationDiscoveryPer;
        @AuraEnabled public Decimal TotalDiscoveryPOVPer;
        @AuraEnabled public Decimal TotalPOVClosingPer;
        @AuraEnabled public Decimal TotalClosingHandoverPer;
        @AuraEnabled public Decimal TotalHandoverWonPer;
        @AuraEnabled public Decimal TotalQualificationWonPer;

        @AuraEnabled public List<oppCountryData> countryDataList;
    }

    public class oppCountryData{
        @AuraEnabled public string Name;

        @AuraEnabled public Decimal TotalQualification              = 0.00;
        @AuraEnabled public Decimal TotalDiscovery                  = 0.00;
        @AuraEnabled public Decimal TotalPOV                        = 0.00;
        @AuraEnabled public Decimal TotalClosing                    = 0.00;
        @AuraEnabled public Decimal TotalHandover                   = 0.00;
        @AuraEnabled public Decimal TotalWon                        = 0.00;

        // Number
        @AuraEnabled public Decimal QualificationDiscovery= 0.00;
        @AuraEnabled public Decimal DiscoveryPOV= 0.00;
        @AuraEnabled public Decimal POVClosing= 0.00;
        @AuraEnabled public Decimal ClosingHandover= 0.00;
        @AuraEnabled public Decimal HandoverWon= 0.00;
        @AuraEnabled public Decimal QualificationWon= 0.00;

        // Percentage
        @AuraEnabled public Decimal QualificationDiscoveryPer= 0.00;
        @AuraEnabled public Decimal DiscoveryPOVPer= 0.00;
        @AuraEnabled public Decimal POVClosingPer= 0.00;
        @AuraEnabled public Decimal ClosingHandoverPer= 0.00;
        @AuraEnabled public Decimal HandoverWonPer= 0.00;
        @AuraEnabled public Decimal QualificationWonPer= 0.00;
    }
}