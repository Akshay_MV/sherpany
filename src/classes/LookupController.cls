public with sharing class LookupController {
    @AuraEnabled
    public static List<sObject> searchRecord(String objectAPIName, String fieldAPIName, 
                                             String andConditionField, String andConditionValue, 
                                             List<String> moreFields, String searchText,
                                             Integer recordLimit,Boolean isEqual)
    {
        
        List<sObject> objectList =  new List<sObject>();
        
        searchText='\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';
        
        String soqlQuery = 'SELECT Id, Name';
        if(!moreFields.isEmpty()){
            soqlQuery = soqlQuery + ',' + String.join(moreFields, ',') ;
        }
        
        soqlQuery = soqlQuery + ' FROM ' + objectAPIName 
            + ' WHERE ' + fieldAPIName
            + ' LIKE ' + searchText;
        
        if(String.isNotEmpty(andConditionField) && String.isNotBlank(andConditionField) 
           && String.isNotEmpty(andConditionValue) && String.isNotBlank(andConditionValue)){
               if(isEqual == true) soqlQuery = soqlQuery + ' AND ' + andConditionField + ' =: andConditionValue';
               else soqlQuery = soqlQuery + ' AND ' + andConditionField + ' !=: andConditionValue';
           }            				  
        soqlQuery = soqlQuery + ' LIMIT ' + recordLimit;
        
        objectList = Database.query(soqlQuery);
        return objectList;
    }
}