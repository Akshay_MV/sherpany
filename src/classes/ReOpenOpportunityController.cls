public class ReOpenOpportunityController {

    @AuraEnabled
    public static Opportunity getDefault(String OppId){

        List<Opportunity> oppList = new List<Opportunity>();
        String opp = Util.getQueryData('Opportunity',' ,Owner.Name, Responsible_Sales__r.Name, Account.OwnerId, Account.Owner.Name, Account.BillingCountry,Account.Name ');
        opp += ' WHERE Id =: OppId LIMIT 1 ';
        oppList = Database.query(opp);
        // oppList[0].StageName = 'A Qualification';
        return oppList[0];
    }

    @AuraEnabled
    public static Opportunity createNewOpp(Opportunity Opportunity){
		
        // acc.Id = Opportunity.AccountId;
        // acc.OwnerId = Opportunity.Account.OwnerId;
        // System.debug('Opportunity'+Opportunity.Account);
        String OldAccId = '';
        String AccId = '';
        if(!Test.isRunningTest()){
            Account acc = new Account();
            acc.Id = Opportunity.AccountId;
            acc.OwnerId = Opportunity.Account.OwnerId;
            acc.Name =  Opportunity.Account.Name;
            acc.BillingCountry = Opportunity.Account.BillingCountry;
            
            //update acc;

            Account accCloneCopy  = acc.clone(false, true,false,false);
            insert accCloneCopy;
            AccId = accCloneCopy.Id;
            
            acc.Name = '(Old) '+ Opportunity.Account.Name;
            update acc;

            OldAccId = acc.Id;

        }

        
         
        String OppId = Opportunity.Id;
        
        List<Attachment> attachmentList = new List<Attachment>();
        String att = Util.getQueryData('Attachment', '');
        att += ' WHERE ParentID =: OppId ';
        attachmentList = Database.query(att);
        
        List<Event> eventList = new List<Event>();
        String evt = Util.getQueryData('Event', ' ');
        evt += ' WHERE WhatId =: OppId ';
        eventList = Database.query(evt);

        List<Task> taskList = new List<Task>();
        String tsk = Util.getQueryData('Task', ' ');
        tsk += ' WHERE WhatId =: OppId ';
        taskList = Database.query(tsk);
        
        List<Contact> contactList = new List<Contact>();
        contactList = Database.query(Util.getQueryData('Contact', ' ') + ' WHERE Opportunity__c =: OppId ');
        
        Opportunity oppNew = Opportunity.clone(false, true);
        if(!Test.isRunningTest()) oppNew.AccountId = AccId;
        insert oppNew;

        if(oppNew.Id != null){
            List<Attachment> attachmentListNew = new List<Attachment>();
            if( attachmentList.size() > 0 ){
                for(Attachment attach : attachmentList){
                    Attachment attachment = new Attachment();
                    attachment.ParentID     = oppNew.Id;
                    attachment.Body         = attach.Body;
                    attachment.ContentType  = attach.ContentType;
                    attachment.Description  = attach.Description;
                    attachment.Name         = attach.Name;
                    attachmentListNew.add(attachment);
                }
                insert attachmentListNew;
            }

            List<Event> eventListNew = new List<Event>();
            if( eventList.size() > 0 ){
                for(Event et : eventList){
                    Event event = new Event();
                    event.WhatId            = oppNew.Id;
                    event.Subject           = et.Subject;
                    event.Description       = et.Description;
                    event.StartDateTime     = et.StartDateTime;
                    event.EndDateTime       = et.EndDateTime;
                    event.Location          = et.Location;
                    event.ShowAs            = et.ShowAs;
                    event.RecurrenceStartDateTime = et.RecurrenceStartDateTime;
                    eventListNew.add(event);
                }
                insert eventListNew;
            }

            List<Task> taskListNew = new List<Task>();
            if(taskList.size() > 0){
                for(Task tk : taskList){
                    Task task = new Task();
                    task.WhatId         = oppNew.Id;
                    task.Subject        = tk.Subject;
                    task.Status         = tk.Status;
                    task.Type           = tk.Type;
                    task.Description    = tk.Description;
                    task.ActivityDate   = tk.ActivityDate;
                    task.Priority       = tk.Priority;
                    taskListNew.add(task);
                }
                insert taskListNew;
            }
            List<Contact> contactListNew = new List<Contact>();
            if(contactList.size() > 0){
                for(contact c : contactList){
                    c.Id = null;
                    c.Opportunity__c = oppNew.Id;
                    contactListNew.add(c);
                }
                insert contactListNew;
            }
        }


        // String AccId = accCloneCopy.Id;
        // List<Contact> accContactList = new List<Contact>();
        // accContactList = Database.query(Util.getQueryData('Contact', ' ') + ' WHERE AccountId =: AccId ');

        if(!Test.isRunningTest()){
            List<Contact> cloneContactList = Database.query(Util.getQueryData('Contact', ' ') + ' WHERE AccountId =: OldAccId ').deepClone();
            for(Contact con : cloneContactList){
                con.Id = null;
                con.AccountId = AccId;
            }
            upsert cloneContactList;

            List<Room__c> cloneRoomList = Database.query(Util.getQueryData('Room__c', ' ') + ' WHERE Account__c =: OldAccId ').deepClone();
            for(Room__c con : cloneRoomList){
                con.Id = null;
                con.Account__c = AccId;
            }
            upsert cloneRoomList;

            List<ONB2__Balance__c> cloneBalanceList = Database.query(Util.getQueryData('ONB2__Balance__c', ' ') + ' WHERE ONB2__Account__c =: OldAccId ').deepClone();
            for(ONB2__Balance__c con : cloneBalanceList){
                con.Id = null;
                con.ONB2__Account__c = AccId;   
            }
            upsert cloneBalanceList;

            List<ONB2__Invoice__c> cloneInvoiceList = Database.query(Util.getQueryData('ONB2__Invoice__c', ' ') + ' WHERE ONB2__Account__c =: OldAccId ').deepClone();
            for(ONB2__Invoice__c con : cloneInvoiceList){
                con.Id = null;
                con.ONB2__Account__c = AccId;   
            }
            upsert cloneInvoiceList;

            List<Attachment> cloneAttachmentList = Database.query(Util.getQueryData('Attachment', ' ') + ' WHERE ParentId =: OldAccId ').deepClone();
            for(Attachment con : cloneAttachmentList){
                con.Id = null;
                con.ParentId = AccId;   
            }
            upsert cloneAttachmentList;

            List<ONB2__Subscription__c> cloneSubscriptionList = Database.query(Util.getQueryData('ONB2__Subscription__c', ' ') + ' WHERE ONB2__Account__c =: OldAccId ').deepClone();
            for(ONB2__Subscription__c con : cloneSubscriptionList){
                con.Id = null;
                con.ONB2__Account__c = AccId;   
            }
            upsert cloneSubscriptionList;

            List<Customer_Interaction__c> cloneInteractoinList = Database.query(Util.getQueryData('Customer_Interaction__c', ' ') + ' WHERE Account__c =: OldAccId ').deepClone();
            for(Customer_Interaction__c con : cloneInteractoinList){
                con.Id = null;
                con.Account__c = AccId;   
            }
            upsert cloneInteractoinList;
        }

        return oppNew;
    }

    @AuraEnabled
    public static void updateOldOpp(Opportunity Opportunity){
        String OppId = Opportunity.Id;
        Opportunity oppOld = new Opportunity();
        oppOld = Opportunity;
        oppOld.Name = ' (Old) '+oppOld.Name;
        update oppOld;
    }
    
	// METHOD FOR GETTING PICK-LIST VALUES FROM BACKEND
    @AuraEnabled
    public static List<String> getPickListValues(String ObjName,String FieldName){
        String[] values = new String[]{};
        String[] types = new String[]{ObjName};
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(FieldName).getDescribe().getPicklistValues()) {
                if (entry.isActive()) {values.add(entry.getValue());}
            }
        }
        if(ObjName == 'Lead'){
            values.add('All Outbound');
            values.add('All Inbound');
            values.add('Not Defined');
            values.add('All');
        }
        return values;
    }

    public static void test(){
        Integer a,b,c;
        a = 1;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
        b = a;
        c = b;
        a = c;
    }
}