global class Util {
    
    /*public static List<String> getListtoSet(List<String> listOfData){
        Set<String> setOfData = new Set<String>();
        for(Integer i = 0 ; i < listOfData.size() ; i++){
            setOfData.add(listOfData[i]);
        }
        listOfData = new List<String>();
        listOfData.addAll(setOfData);
        return listOfData == null ? listOfData = new List<String>() : listOfData;
    }*/

     
    // To query from any object.
    public static String getQueryData(String ObjectName, String relatedFields){
        
        String query = 'SELECT ' + util.getDynamicFields(ObjectName,'');
        // If we have any parent fields to query pass in string format with comma seperated.
        if(relatedFields != '') query += relatedFields;
        query += ' FROM ' + ObjectName;
        
        return query;
    }
    
    public static string getParentRecordFields(String ObjectName){
        return ' , '+Util.getDynamicFields(ObjectName, ObjectName.removeEnd('c') + (ObjectName.endsWith('c') ? 'r.' : '.'));
    }
    
    // returns object's field names without parent fields.
    public static String getDynamicFields(String ObjectName, String ParentObj){
        String fieldNames ='';
        Schema.SObjectType convertType = Schema.getGlobalDescribe().get(ObjectName);
        Map<String, Schema.SObjectField> fieldsMap = convertType.getDescribe().fields.getMap();
        Boolean isFirst = true;
        for(String key : fieldsMap.keySet()) {
            fieldNames  = isFirst   ? fieldNames + ' '+ParentObj+ fieldsMap.get(key)     : fieldNames + ', '+ParentObj+ fieldsMap.get(key) + ' ';
            isFirst     = false;
        }
        return fieldNames;
    }
    
    // Returns list of picklist values.
    // @AuraEnabled
    public static List<String> getPickListValues(String ObjName,String FieldName){
        String[] values = new String[]{};
        String[] types = new String[]{ObjName};
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        
        // values.add('--None--');
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(FieldName).getDescribe().getPicklistValues()) {
                if (entry.isActive()) {values.add(entry.getValue());}
            }
        }
        return values;
    }
    
    // For Exception
    /*public static void checkExceptions( Database.SaveResult[] result ){
        decimal totalContactsUpdated=0, totalError=0;
        for (Database.SaveResult cmm : result) {
            if (cmm.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully upserted Campaign Member ID: ' + cmm.getId());
                totalContactsUpdated++;
            }else {
                // Operation failed, so get all errors                
                for(Database.Error err : cmm.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Campaign Member fields that affected this error: ' + err.getFields());
                    totalError++;
                }
            }
        }
        System.debug('Total Users Successfully Updated :'+totalContactsUpdated);
        System.debug('Total Users Errored :'+totalError);
    } */
}