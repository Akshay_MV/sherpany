@isTest
public class Test_RecordPageLookupController {

	@isTest
	public static  void test() {
	    
	    Industry__c inds= new Industry__c();
	    inds.Name = 'test';
	    inds.Industry_Score__c=4;
	    insert inds;
	    
	    Lead ld= new Lead();
	    ld.Industry__c=inds.Id;
	    ld.Email='test@123.com';
	    ld.LastName='test';
	    ld.Company = 'test';
	   // insert ld;
	   	
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        
	    Test.startTest();
	    RecordPageLookupController.getRecordDetails(ld.Id);
	    RecordPageLookupController.saveRecordDetails(ld.Id,inds.Id);
        RecordPageLookupController.getRecordDetail(opp.Id);
		RecordPageLookupController.saveRecordDetail(opp.Id,inds.Id);
	    Test.stopTest();

	}

}