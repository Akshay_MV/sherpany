@isTest
public class Test_CR_OppStageController {
    @isTest
	public static void test() {
    List<Opportunity> oppList = new List<Opportunity>();
        // List<User> userList  =new List<User>();
        // userList = [ SELECT Id FROM User LIMIT 1];
		Profile prof = [select id from profile where name ='Standard User' LIMIT 1];
        User user = new User(firstname = 'fName',
        lastName = 'lName',
        email = 'test@test.org',
        Username = 'test122@test.org',
        EmailEncodingKey = 'ISO-8859-1',
        Alias = 'testtest',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US',
        ProfileId = prof.Id);
        insert user;
        String UserId = user.Id;
        SetUp__c setupdata = new SetUp__c();
        setupdata.Name = 'Default';
		setupdata.Users__c = UserId;
		setupdata.Report_Name__c = 'Opportunity stage conversion rate';
        
        insert setupdata;
        
        List<Account> accList = new List<Account>();
	    
	    for(Integer i = 0 ; i < 16 ; i++){
	        Account acc = new Account();
    	    acc.name = 'Test'+i;
    	    if(i == 0 || i == 8){
                acc.BillingCountry = 'India';
            }
            else if(i == 1 || i == 9){
                acc.BillingCountry = 'Canada';
            }else if(i == 2 || i == 10){
                acc.BillingCountry = 'Belgium';
            }else if(i == 3 || i == 11){
                acc.BillingCountry = 'Bhutan';
            }else if(i == 4 || i == 12){
                acc.BillingCountry = 'Germany';
            }else if(i == 5 || i == 13){
                acc.BillingCountry = 'Indonesia';
            }else if(i == 6 || i == 14){
                acc.BillingCountry = 'Mexico';
            }else if(i == 7 || i == 15){
                acc.BillingCountry = 'Qatar';
            }else{
                acc.BillingCountry = 'India';
            }
    	    accList.add(acc);
	    }
	    
	    insert accList;

        for(Integer i = 0 ; i < 16 ; i++){
            Opportunity opp = new Opportunity();
            opp.Name = 'Test';
            opp.Responsible_Sales__c = user.Id;
            opp.AccountId = accList[i].Id;
            opp.Lead_Created_date__c = System.today();
            opp.Pain_Points__c = 'Too many meetings';
            // opp.Country__c= 'Belize';
            
            if(i == 0 || i == 8){
                opp.StageName = 'A Qualification';
            }else if(i == 1 || i == 9){
                opp.StageName = 'B Discovery';
            }else if(i == 2 || i == 10){
                opp.StageName = 'C POV';
            }else if(i == 3 || i == 11){
                opp.StageName = 'D Closing';
            }else if(i == 4 || i == 12){
                opp.StageName = 'E Closed Won';
            }else if(i == 5 || i == 13){
                opp.StageName = 'Contract Replaced';
            }else if(i == 6 || i == 14){
                opp.StageName = 'Closed Dead';
                opp.Termination_Reason__c = 'Termination for Testing';
            }else if(i == 7 || i == 15){
                opp.StageName = 'Terminated';
                opp.Termination_Reason__c = 'Termination for Testing';
                opp.Termination_Date__c = System.today();
            }else{
                opp.StageName = 'Terminated';
                opp.Termination_Reason__c = 'Termination for Testing';
                opp.Termination_Date__c = System.today();
            }
            opp.Did_they_have_a_demo_room__c = 'Yes';
            opp.CloseDate = System.today();
            oppList.add(opp);
        }
        insert oppList;
        
        Integer i = 0;
        for(Opportunity op : oppList){
            
            if(i == 0 || i == 8){
                op.StageName = 'B Discovery';
            }else if(i == 1 || i == 9){
                op.StageName = 'C POV';
            }else if(i == 2 || i == 10){
                op.StageName = 'D Closing';
            }else if(i == 3 || i == 11){
                op.StageName = 'E Closed Won';
            }else if(i == 4 || i == 12){
                op.StageName = 'Contract Replaced';
            }else if(i == 5 || i == 13){
                op.StageName = 'Closed Dead';
            }else if(i == 6 || i == 14){
                op.StageName = 'Terminated';
                op.Termination_Reason__c = 'Termination for Testing';
            }else if(i == 7 || i == 15){
                op.StageName = 'A Qualification';
                op.Termination_Reason__c = 'Termination for Testing';
                op.Termination_Date__c = System.today();
            }
            
           //op.StageName = 'D Closing';
            op.Industries__c = 'Energy';
            i++;
            //op.Responsible_Sales__c = userList[0].Id;
        }
        //update oppList;
        
        List<Integer> month = new List<Integer>();
        month.add(1);
        CR_OppStageController.getUserData();
        CR_OppStageController.getDefault(System.today(),System.today().addDays(5),'','');
        // CR_OppStageController.getUserData();
        CR_OppStageController.getDefault(System.today(),System.today().addDays(5),'','');
        CR_OppStageController.dummy();
        delete oppList;
    }

}