@isTest
private class Test_GrowthHackerController {

	@isTest static void getLeadData(){
        Integer myYear = Date.today().year();
	     
        List<Lead> leadList1 = new List<Lead>();
        for(Integer i = 0 ; i < 20 ; i++){
            Lead lead = new Lead();
            lead.Status =   'MQL';
            lead.CurrencyIsoCode = 'CHF';
            lead.Email = 'test'+i*i+'@test.com';
            lead.LastName = 'test'+i;
            lead.Company = 'testCompany'+i;
            lead.saleswings__Validation_Skip__c = date.newinstance(myYear+1,1,1);
            lead.saleswings__Last_Visit_Datetime__c = date.newinstance(myYear+1,1,1);
            lead.Resumption_at__c = date.newinstance(myYear+1,1,1);
            // lead.LastTransferDate = date.newinstance(myYear+1,1,1);
            leadList1.add(lead);
        }
        // insert leadList1;
        for(Lead ld : leadList1){
            //Test.setCreatedDate(ld.Id, Date.newInstance(2019, 1, 10));
        }
        
        // upsert leadList1;
        
        List<Lead> leadList2 = new List<Lead>();
        for(Integer i = 21 ; i < 40 ; i++){
            Lead lead = new Lead();
            lead.Status =   'MQL';
            lead.CurrencyIsoCode = 'CHF';
            lead.Email = 'test'+i*i+'@test.com';
            lead.LastName = 'test'+i;
            lead.Company = 'testCompany'+i;
            leadList2.add(lead);
        }
        
        //insert leadList2;
        
        for(Lead ld : leadList2){
            //Test.setCreatedDate(ld.Id, Date.newInstance(2019, 2, 10));
        }
    
        // upsert leadList2;
        
        
        List<Lead> leadList3 = new List<Lead>();
        for(Integer i = 41 ; i < 60 ; i++){
            Lead lead = new Lead();
            lead.Status =   'MQL';
            lead.CurrencyIsoCode = 'CHF';
            lead.Email = 'test'+i*i+'@test.com';
            lead.LastName = 'test'+i;
            lead.Company = 'testCompany'+i;
            leadList3.add(lead);
        }
        //insert leadList3;
        
        for(Lead ld : leadList3){
            //Test.setCreatedDate(ld.Id, Date.newInstance(2019, 3, 10));
        }
        // upsert leadList3;
        
        //Opportunity
        List<Opportunity> oppList1 = new List<Opportunity>();
        for(Integer i = 0 ; i < 20 ; i++){
            Opportunity opp = new Opportunity();
            opp.Name = 'test'+i;
            opp.StageName = 'A Qualification';
            opp.CurrencyIsoCode = 'EUR';
            opp.CloseDate = date.newinstance(myYear+1,1,1);
            oppList1.add(opp);
        }
        insert oppList1;
        for(Opportunity op : oppList1){
            Test.setCreatedDate(op.Id, Date.newInstance(2019, 1, 10));
        }
        upsert oppList1;
        
        List<Opportunity> oppList2 = new List<Opportunity>();
        for(Integer i = 21 ; i < 40 ; i++){
            Opportunity opp = new Opportunity();
            opp.Name = 'test'+i;
            opp.StageName = 'A Qualification';
            opp.CurrencyIsoCode = 'EUR';
            opp.CloseDate = date.newinstance(myYear+1,1,1);
            oppList2.add(opp);
        }
        insert oppList2;
        for(Opportunity op : oppList2){
            Test.setCreatedDate(op.Id, Date.newInstance(2019, 2, 10));
        }
        upsert oppList2;
        
        List<Opportunity> oppList3 = new List<Opportunity>();
        for(Integer i = 41 ; i < 60 ; i++){
            Opportunity opp = new Opportunity();
            opp.Name = 'test'+i;
            opp.StageName = 'A Qualification';
            opp.CurrencyIsoCode = 'EUR';
            opp.CloseDate = date.newinstance(myYear+1,1,1);
            oppList3.add(opp);
        }
        insert oppList3;
        for(Opportunity op : oppList3){
            Test.setCreatedDate(op.Id, Date.newInstance(2019, 3, 10));
        }
        upsert oppList3;
        
        
        
        GrowthHackerController.getLeadData(2019,1,'All Inbound');
        GrowthHackerController.getPickListValues('Lead','Status');
        //GrowthHackerController.getYearData(2019,'All Inbound');
        GrowthHackerController.dummy();
	}
	
	@isTest static void getLeadData1(){
	    Integer myYear = Date.today().year();
       /* List<Lead> leadList1 = new List<Lead>();
        for(Integer i = 0 ; i < 20 ; i++){
            Lead lead = new Lead();
            lead.Status =   'MQL';
            lead.CurrencyIsoCode = 'CHF';
            lead.Email = 'test'+i+'@test.com';
            lead.LastName = 'test'+i;
            lead.Company = 'testCompany'+i;
            leadList1.add(lead);
        }
        insert leadList1;
        
        for(Lead ld : leadList1){
            Test.setCreatedDate(ld.Id, Date.newInstance(2018, 1, 10));
        }
        
        // upsert leadList1;
        
        List<Lead> leadList2 = new List<Lead>();
        for(Integer i = 21 ; i < 40 ; i++){
            Lead lead = new Lead();
            lead.Status =   'MQL';
            lead.CurrencyIsoCode = 'CHF';
            lead.Email = 'test'+i+'@test.com';
            lead.LastName = 'test'+i;
            lead.Company = 'testCompany'+i;
            leadList2.add(lead);
        }
        insert leadList2;
        
        for(Lead ld : leadList2){
            Test.setCreatedDate(ld.Id, Date.newInstance(2018, 2, 10));
        }
        // upsert leadList2;
        
        List<Lead> leadList3 = new List<Lead>();
        for(Integer i = 41 ; i < 60 ; i++){
            Lead lead = new Lead();
            lead.Status =   'MQL';
            lead.CurrencyIsoCode = 'CHF';
            lead.Email = 'test'+i+'@test.com';
            lead.LastName = 'test'+i;
            lead.Company = 'testCompany'+i;
            leadList3.add(lead);
        }
        insert leadList3;
        
        for(Lead ld : leadList3){
            Test.setCreatedDate(ld.Id, Date.newInstance(2018, 3, 10));
        }*/
        // upsert leadList3;
        
        
        //Opportunity
        List<Opportunity> oppList1 = new List<Opportunity>();
        for(Integer i = 0 ; i < 20 ; i++){
            Opportunity opp = new Opportunity();
            opp.Name = 'test'+i;
            opp.StageName = 'A Qualification';
            opp.CurrencyIsoCode = 'EUR';
            opp.CloseDate = date.newinstance(myYear+1,1,1);
            oppList1.add(opp);
        }
        insert oppList1;
        for(Opportunity op : oppList1){
            Test.setCreatedDate(op.Id, Date.newInstance(2018, 1, 10));
        }
        // upsert oppList1;
        
        List<Opportunity> oppList2 = new List<Opportunity>();
        for(Integer i = 21 ; i < 40 ; i++){
            Opportunity opp = new Opportunity();
            opp.Name = 'test'+i;
            opp.StageName = 'A Qualification';
            opp.CurrencyIsoCode = 'EUR';
            opp.CloseDate = date.newinstance(myYear+1,1,1);
            oppList2.add(opp);
        }
        insert oppList2;
        for(Opportunity op : oppList2){
            Test.setCreatedDate(op.Id, Date.newInstance(2018, 2, 10));
        }
        // upsert oppList2;
        
        List<Opportunity> oppList3 = new List<Opportunity>();
        for(Integer i = 41 ; i < 60 ; i++){
            Opportunity opp = new Opportunity();
            opp.Name = 'test'+i;
            opp.StageName = 'A Qualification';
            opp.CurrencyIsoCode = 'EUR';
            opp.CloseDate = date.newinstance(myYear+1,1,1);
            oppList3.add(opp);
        }
        insert oppList3;
        for(Opportunity op : oppList3){
            Test.setCreatedDate(op.Id, Date.newInstance(2018, 3, 10));
        }
        // upsert oppList3;
        
        GrowthHackerController.getLeadData(2019,1,'All Inbound');
        GrowthHackerController.getPickListValues('Lead','Status');
        GrowthHackerController.getYearData(2019,'All Inbound');
	}

}