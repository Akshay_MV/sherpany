@isTest(SeeAllData = true)
public class Test_CRForUserController {
	@isTest
    public static void testCRFUCM(){
       
        List<Opportunity>  OppList  = new List<Opportunity>();
        List<Account> accList = new List<Account>();  

        List<User> userList  =new List<User>();
        userList = [ SELECT Id, Name FROM User LIMIT 3];
       

        Integer myYear = Date.today().year();
        
	    for(Integer i = 0 ; i < 16 ; i++){
	        Account acc = new Account();
    	    acc.name = 'Test'+i;
    	    if(i == 0 || i == 8){
                acc.BillingCountry = 'India';
            }else if(i == 1 || i == 9){
                acc.BillingCountry = 'Canada';
            }else if(i == 2 || i == 10){
                acc.BillingCountry = 'Belgium';
            }else if(i == 3 || i == 11){
                acc.BillingCountry = 'Bhutan';
            }else if(i == 4 || i == 12){
                acc.BillingCountry = 'Germany';
            }else if(i == 5 || i == 13){
                acc.BillingCountry = 'Indonesia';
            }else if(i == 6 || i == 14){
                acc.BillingCountry = 'Mexico';
            }else if(i == 7 || i == 15){
                acc.BillingCountry = 'Qatar';
            }
    	    accList.add(acc);
	    }
	    
	    insert accList;
	    
	    for(Integer i = 0 ; i < 16 ; i++){
            Opportunity opp = new Opportunity();
            opp.Name = 'Test';
            opp.Responsible_Sales__c = userList[0].Id;
            opp.AccountId = accList[i].Id;
            opp.Lead_Created_Date__c = System.today();
            
            if(i == 0 || i == 8){
                opp.StageName = 'A Qualification';
            }else if(i == 1 || i == 9){
                opp.StageName = 'B Discovery';
            }else if(i == 2 || i == 10){
                opp.StageName = 'C POV';
            }else if(i == 3 || i == 11){
                opp.StageName = 'D Closing';
            }else if(i == 4 || i == 12){
                opp.StageName = 'E Closed Won';
            }else if(i == 5 || i == 13){
                opp.StageName = 'Contract Replaced';
            }else if(i == 6 || i == 14){
                opp.StageName = 'Closed Dead';
                opp.Termination_Reason__c = 'Termination for Testing';
            }else if(i == 7 || i == 15){
                opp.StageName = 'Terminated';
                opp.Termination_Reason__c = 'Termination for Testing';
                opp.Termination_Date__c = System.today();
            }
            opp.CloseDate = System.today();
            oppList.add(opp);
	    }
        insert oppList;

        List<Opportunity> oppList1 = new List<Opportunity>();
        for(Opportunity op : oppList1){
            Test.setCreatedDate(op.Id, Date.newInstance(2019, 1, 10));
        }
        upsert oppList1;
        
        List<Opportunity> oppList2 = new List<Opportunity>();
        for(Integer i = 21 ; i < 40 ; i++){
            Opportunity opp = new Opportunity();
            opp.Lead_Created_Date__c = System.today();
            opp.Name = 'test'+i;
            opp.StageName = 'A Qualification';
            opp.Was_In_ASL__c = true;
            opp.CurrencyIsoCode = 'EUR';
            opp.Responsible_Sales__c = userList[0].Id;
            opp.CloseDate = date.newinstance(myYear+1,1,1);
            oppList2.add(opp);
        }
        insert oppList2;
        for(Opportunity op : oppList2){
            //Test.setCreatedDate(op.Id, Date.newInstance(2019, 2, 10));
        }
        upsert oppList2;
        
        List<Opportunity> oppList3 = new List<Opportunity>();
        for(Integer i = 41 ; i < 60 ; i++){
            Opportunity opp = new Opportunity();
            opp.Name = 'test'+i;
            opp.StageName = 'B Discovery';
            opp.CurrencyIsoCode = 'EUR';
            opp.Lead_Created_Date__c = System.today();
            opp.Was_In_ASL__c = true;
            opp.Was_In_MQL__c = true;
            opp.Responsible_Sales__c = userList[0].Id;
            opp.CloseDate = date.newinstance(myYear+1,1,1);
            oppList3.add(opp);
        }
        insert oppList3;
        for(Opportunity op : oppList3){
            // Test.setCreatedDate(op.Id, Date.newInstance(2019, 3, 10));
        }
        upsert oppList3;
     
		
        List<String> lsString = new List<String>();
        // userList[0].Name= '';
        lsString.add('No Responsible Sales');
        lsString.add('');
        lsString.add(userList[0].Name);
        lsString.add('No Responsible Sales');
         CRForUserController.UserName(2020);
        CRForUserController.getDefault(2020,01,'All Inbound',lsString); 
        List<String>lsString1 = new List<String>();
        lsString1.add('test');
        CRForUserController.getDefault(2020,01,'All Outbound',lsString1); 
        CRForUserController.getDefault(2020,01,'Not Defined',lsString);
        CRForUserController.getDefault(2020,01,'Inbound',lsString); 
        CRForUserController.getOpportunityJSON(2020,01,lsString);
        // CRForUserController.getOpportunityJSON(2020,01,lsString);
        // CRForUserController.getOpportunityJSON(2020,01,lsString);
       
        CRForUserController.getPickListValues('Lead','Status');

    }
}