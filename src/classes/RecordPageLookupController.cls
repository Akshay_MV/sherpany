public class RecordPageLookupController {
	@AuraEnabled
    public static RecordPageLookupController.dataWrapper getRecordDetails(String recordId){
        List<Lead> leadList = new List<Lead>();
        leadList = [SELECT Id, Industry__c, Industry__r.Name FROM Lead WHERE Id =: recordId LIMIT 1];
        
        RecordPageLookupController.dataWrapper dw = new RecordPageLookupController.dataWrapper();
        
        if(!leadList.isEmpty()){
            dw.recordId = leadList[0].Industry__c != null ? leadList[0].Industry__c : '';
            dw.recordName = leadList[0].Industry__c != null ? leadList[0].Industry__r.Name : '';
        }
        return dw;
    }
    
    @AuraEnabled
    public static String saveRecordDetails(String recordId, String industryId){
        Lead leadObj = new Lead();
        leadObj.Id = recordId;
        leadObj.Industry__c = String.isNotBlank(industryId) && String.isNotEmpty(industryId) ? industryId : null;
        
        try{
            update leadObj;
            return 'SUCCESS';
        }catch(exception e){
            return 'FAIL';
        }
    }
    
    //Opportunity
    @AuraEnabled
    public static RecordPageLookupController.dataWrapper getRecordDetail(String recordId){
        List<Opportunity> oppList = new List<Opportunity>();
        oppList = [SELECT Id, Industry_lookup__c, Industry_lookup__r.Name FROM Opportunity WHERE Id =: recordId LIMIT 1];
        
        RecordPageLookupController.dataWrapper dw = new RecordPageLookupController.dataWrapper();
        
        if(!oppList.isEmpty()){
            dw.recordId = oppList[0].Industry_lookup__c != null ? oppList[0].Industry_lookup__c : '';
            dw.recordName = oppList[0].Industry_lookup__c != null ? oppList[0].Industry_lookup__r.Name : '';
        }
        return dw;
    }
    
    //Opportunity
    @AuraEnabled
    public static String saveRecordDetail(String recordId, String industryId){
        Opportunity oppObj = new Opportunity();
        oppObj.Id = recordId;
        oppObj.Industry_lookup__c = String.isNotBlank(industryId) && String.isNotEmpty(industryId) ? industryId : null;
        
        try{
            update oppObj;
            return 'SUCCESS';
        }catch(exception e){
            return 'FAIL';
        }
    }
    
    public class dataWrapper{
        @AuraEnabled public String recordId;
        @AuraEnabled public String recordName;
        
        public dataWrapper(){
        	recordId = '';
            recordName = '';
        }
    }
}