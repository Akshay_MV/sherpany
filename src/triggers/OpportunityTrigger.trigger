/*-----------------------------------
Author: Akshay Chauhan
Date: 01 December 2019    

Description: To count SQL numbers
THIS Trigger can be used for all the tasks done by Opportunity.
Handler: 
-----------------------------------*/

trigger OpportunityTrigger on Opportunity (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    OpportunityTriggerHandler handler = new OpportunityTriggerHandler(trigger.new, trigger.old, trigger.newMap, trigger.oldMap, trigger.isInsert,trigger.isUpdate, trigger.isDelete, trigger.isUndelete);
    
    if(trigger.isBefore){
        if(trigger.isInsert) handler.BeforeInsertEvent();
        else if(trigger.isUpdate) handler.BeforeUpdateEvent();
        else if(trigger.isDelete) handler.BeforeDeleteEvent();
    }else if(trigger.isAfter){
        if(trigger.isInsert) handler.AfterInsertEvent();
        else if(trigger.isUpdate) handler.AfterUpdateEvent();
        else if(trigger.isDelete) handler.AfterDeleteEvent();
        else if(trigger.isUndelete) handler.AfterUndeleteEvent();
    }
}