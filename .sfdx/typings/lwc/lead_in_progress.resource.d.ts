declare module "@salesforce/resourceUrl/lead_in_progress" {
    var lead_in_progress: string;
    export default lead_in_progress;
}