declare module "@salesforce/apex/userSetUpController.getDefault" {
  export default function getDefault(param: {rptName: any}): Promise<any>;
}
declare module "@salesforce/apex/userSetUpController.saveSetup" {
  export default function saveSetup(param: {lstUserId: any, lstUserName: any, rptName: any}): Promise<any>;
}
