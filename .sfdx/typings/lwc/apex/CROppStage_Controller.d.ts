declare module "@salesforce/apex/CROppStage_Controller.getDefault" {
  export default function getDefault(param: {year: any, month: any, selectedUser: any, selectedCountry: any}): Promise<any>;
}
