declare module "@salesforce/apex/CR_OppStageController.getUserData" {
  export default function getUserData(): Promise<any>;
}
declare module "@salesforce/apex/CR_OppStageController.getDefault" {
  export default function getDefault(param: {StartDate: any, EndDate: any, selectedUser: any, selectedCountry: any}): Promise<any>;
}
