declare module "@salesforce/apex/GrowthHackerDBLeadLevelController.LeadLevelData" {
  export default function LeadLevelData(param: {year: any, Quarter: any, ls: any, DataType: any}): Promise<any>;
}
declare module "@salesforce/apex/GrowthHackerDBLeadLevelController.getLeadData" {
  export default function getLeadData(param: {year: any, Quarter: any, ls: any, DataType: any, LeadLevel: any}): Promise<any>;
}
declare module "@salesforce/apex/GrowthHackerDBLeadLevelController.LeadLevelGetYearData" {
  export default function LeadLevelGetYearData(param: {year: any, ls: any, DataType: any}): Promise<any>;
}
declare module "@salesforce/apex/GrowthHackerDBLeadLevelController.getYearData" {
  export default function getYearData(param: {year: any, ls: any, DataType: any, LeadLevel: any}): Promise<any>;
}
declare module "@salesforce/apex/GrowthHackerDBLeadLevelController.getPickListValues" {
  export default function getPickListValues(param: {ObjName: any, FieldName: any}): Promise<any>;
}
