declare module "@salesforce/apex/CRForUserController.UserName" {
  export default function UserName(param: {Year: any}): Promise<any>;
}
declare module "@salesforce/apex/CRForUserController.getDefault" {
  export default function getDefault(param: {year: any, month: any, ls: any, userNames: any}): Promise<any>;
}
declare module "@salesforce/apex/CRForUserController.getOpportunityJSON" {
  export default function getOpportunityJSON(param: {year: any, month: any, userNames: any}): Promise<any>;
}
declare module "@salesforce/apex/CRForUserController.getPickListValues" {
  export default function getPickListValues(param: {ObjName: any, FieldName: any}): Promise<any>;
}
