declare module "@salesforce/apex/CR_SDR_Controller.getUserListData" {
  export default function getUserListData(): Promise<any>;
}
declare module "@salesforce/apex/CR_SDR_Controller.getDefault" {
  export default function getDefault(param: {year: any, month: any, ls: any, userNames: any}): Promise<any>;
}
declare module "@salesforce/apex/CR_SDR_Controller.getUserNameString" {
  export default function getUserNameString(): Promise<any>;
}
declare module "@salesforce/apex/CR_SDR_Controller.UserName" {
  export default function UserName(param: {Year: any}): Promise<any>;
}
declare module "@salesforce/apex/CR_SDR_Controller.getPickListValues" {
  export default function getPickListValues(param: {ObjName: any, FieldName: any}): Promise<any>;
}
