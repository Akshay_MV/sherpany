declare module "@salesforce/apex/ReOpenOpportunityController.getDefault" {
  export default function getDefault(param: {OppId: any}): Promise<any>;
}
declare module "@salesforce/apex/ReOpenOpportunityController.createNewOpp" {
  export default function createNewOpp(param: {Opportunity: any}): Promise<any>;
}
declare module "@salesforce/apex/ReOpenOpportunityController.createNewAccount" {
  export default function createNewAccount(param: {AccId: any}): Promise<any>;
}
declare module "@salesforce/apex/ReOpenOpportunityController.updateOldOpp" {
  export default function updateOldOpp(param: {Opportunity: any}): Promise<any>;
}
declare module "@salesforce/apex/ReOpenOpportunityController.getPickListValues" {
  export default function getPickListValues(param: {ObjName: any, FieldName: any}): Promise<any>;
}
