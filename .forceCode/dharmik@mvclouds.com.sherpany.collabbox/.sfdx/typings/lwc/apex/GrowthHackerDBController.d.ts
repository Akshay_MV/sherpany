declare module "@salesforce/apex/GrowthHackerDBController.getLeadData" {
  export default function getLeadData(param: {year: any, Quarter: any, ls: any, DataType: any}): Promise<any>;
}
declare module "@salesforce/apex/GrowthHackerDBController.getYearData" {
  export default function getYearData(param: {year: any, ls: any, DataType: any}): Promise<any>;
}
declare module "@salesforce/apex/GrowthHackerDBController.getPickListValues" {
  export default function getPickListValues(param: {ObjName: any, FieldName: any}): Promise<any>;
}
