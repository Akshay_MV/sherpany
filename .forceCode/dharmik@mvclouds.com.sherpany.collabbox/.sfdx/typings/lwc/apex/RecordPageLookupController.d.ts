declare module "@salesforce/apex/RecordPageLookupController.getRecordDetails" {
  export default function getRecordDetails(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/RecordPageLookupController.saveRecordDetails" {
  export default function saveRecordDetails(param: {recordId: any, industryId: any}): Promise<any>;
}
declare module "@salesforce/apex/RecordPageLookupController.getRecordDetail" {
  export default function getRecordDetail(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/RecordPageLookupController.saveRecordDetail" {
  export default function saveRecordDetail(param: {recordId: any, industryId: any}): Promise<any>;
}
