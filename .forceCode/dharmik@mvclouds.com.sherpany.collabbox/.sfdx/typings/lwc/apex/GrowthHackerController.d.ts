declare module "@salesforce/apex/GrowthHackerController.getLeadData" {
  export default function getLeadData(param: {year: any, Quarter: any, ls: any}): Promise<any>;
}
declare module "@salesforce/apex/GrowthHackerController.getYearData" {
  export default function getYearData(param: {year: any, ls: any}): Promise<any>;
}
declare module "@salesforce/apex/GrowthHackerController.getPickListValues" {
  export default function getPickListValues(param: {ObjName: any, FieldName: any}): Promise<any>;
}
