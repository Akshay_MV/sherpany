declare module "@salesforce/apex/LookupController.searchRecord" {
  export default function searchRecord(param: {objectAPIName: any, fieldAPIName: any, andConditionField: any, andConditionValue: any, moreFields: any, searchText: any, recordLimit: any, isEqual: any}): Promise<any>;
}
