declare module "@salesforce/apex/CRPCController.UserName" {
  export default function UserName(param: {Year: any}): Promise<any>;
}
declare module "@salesforce/apex/CRPCController.countryData" {
  export default function countryData(param: {Year: any, userNames: any}): Promise<any>;
}
declare module "@salesforce/apex/CRPCController.mainPer" {
  export default function mainPer(param: {Year: any, userNames: any}): Promise<any>;
}
declare module "@salesforce/apex/CRPCController.main" {
  export default function main(param: {Year: any, userNames: any}): Promise<any>;
}
declare module "@salesforce/apex/CRPCController.getPickListValues" {
  export default function getPickListValues(param: {ObjName: any, FieldName: any}): Promise<any>;
}
